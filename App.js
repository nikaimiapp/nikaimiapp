import 'react-native-gesture-handler';
import React, { StrictMode, useEffect, useRef } from 'react'
import { StyleSheet, Text, Alert, SafeAreaView } from 'react-native'
import Routes from './src/navigation/Routes';
import messaging from '@react-native-firebase/messaging';
import notifee from '@notifee/react-native';
import { AuthProvider } from './src/Context/AuthContext';
import OpenFile from './src/screens/Chat/a';
import Map from './src/screens/Map';
import Insights from './src/screens/Insides';
import SplashScreen from 'react-native-splash-screen'
import AdminOrderReview from './src/screens/AdminOrderReview';
import SMTourPlanDetails from './src/screens/SMTourPlanDetails';

const App = () => {

  React.useEffect(() => {
    setTimeout(() => {
      //hiding splash screen in 500 ms seconds
      SplashScreen.hide();

    }, 500);

  }, [])

  const getAcessToken = async () => {
    await messaging().registerDeviceForRemoteMessages();
    const token = await messaging().getToken();
    console.log("token", token)

  }

  // async function onDisplayNotification() {
  //   // Request permissions (required for iOS)
  //   await notifee.requestPermission()

  //   // Create a channel (required for Android)
  //   const channelId = await notifee.createChannel({
  //     id: 'default',
  //     name: 'Default Channel',
  //   });

  //   // Display a notification
  //   await notifee.displayNotification({
  //     title: 'Test notification',
  //     body: 'message from nisanth',
  //     android: {
  //       channelId,
  //       //smallIcon: 'name-of-a-small-icon', // optional, defaults to 'ic_launcher'.
  //       // pressAction is needed if you want the notification to open the app when pressed
  //       pressAction: {
  //         id: 'default'
  //       },
  //     },
  //   });
  // }


  useEffect(() => {


    // //foreground notification
    // const unsubscribe = messaging().onMessage(async remoteMessage => {
    //   Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    //   onDisplayNotification();
    // });
    getAcessToken();
    //when app is background notification
    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      //navigation.navigate(remoteMessage.data.type);
    });
    //when app is quite state notification
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
          //setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
        }
        //setLoading(false);
      });
    //return unsubscribe;
  }, []);
  // let ws = useRef(null);
  // useEffect(() => {
  //   //console.log("web socket initialisation");
  //   ws.current = new WebSocket('wss://dev.ordo.primesophic.com:8090');
  //   //open connection
  //   ws.current.onopen = (e) => {
  //     console.log('ordo WebSocket connection opened');
  //     ws.current.send('app side test message');
  //     //sending message
  //     data = {
  //       avatar: "",
  //       id: "1878e60a-fe1c-0d00-e8b6-63119c1882aa",
  //       message: "test message from app side 3",
  //       name: "Sales messager",

  //     }
  //     //ws.current.send(JSON.stringify({ type: 'chat_sent',data: data, convo_id: '', by: '1878e60a-fe1c-0d00-e8b6-63119c1882aa', user_ids: '1878e60a-fe1c-0d00-e8b6-63119c1882aa,619370d5-c7a0-f000-8a4f-633fd47942da', device_type:'mobile' }))
  //     //ws.current.send(JSON.stringify({ type:'past_conversation',by:'1878e60a-fe1c-0d00-e8b6-63119c1882aa',device_type:'mobile' }))
  //     ws.current.send(JSON.stringify({ type: 'get_users', by: 'c5dffe5a-e0bc-a2a1-c5f3-63103c0dde89', device_type: 'mobile' }))


  //     console.log(" request  sent")
  //   }
  //   ws.current.onerror = (e) => {
  //     // an error occurred
  //     console.log("ordo web socket connection failed ", e.message);
  //   };

  //   ws.current.onmessage = (e) => {
  //     // a message was received
  //     console.log("new msg \n", e.data);

  //   };

  //   ws.current.onclose = () => {
  //     console.log('ordo WebSocket connection closed')
  //   }

  //   return () => ws.current.close();
  // }, [])


  // const send = () => {
  //   ws.current.send('app side test message');
  //   //sending message
  //   data = {
  //     avatar: "",
  //     id: "1878e60a-fe1c-0d00-e8b6-63119c1882aa",
  //     message: "test message 1",
  //     name: "Sales messager",

  //   }
  //   ws.current.send(JSON.stringify({ type: 'chat_sent', data: data, convo_id: '', by: '1878e60a-fe1c-0d00-e8b6-63119c1882aa', user_ids: '1878e60a-fe1c-0d00-e8b6-63119c1882aa,619370d5-c7a0-f000-8a4f-633fd47942da', device_type: 'mobile' }))

  // }


  return (
    <AuthProvider>
      <SafeAreaView style={{ flex: 1 }}>
        <Routes />
        {/* <AdminOrderReview/> */}
        {/* <AdminInvoice/> */}
        {/* <SMTourPlanDetails/> */}
      </SafeAreaView>
    </AuthProvider>


  )
}

export default App

const styles = StyleSheet.create({

})

