import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Image } from 'react-native';
import Visits from '../screens/Visits';
import UserList from '../screens/UsersList';
import Attendacnce from '../screens/Attendance';
import Insides from '../screens/Insides';
import Colors from '../constants/Colors';
import Inventory from '../screens/Inventory';
import CustomerFinance from '../screens/CustomerFinance';
import AdminHome from '../screens/AdminHome';
import { AuthContext } from '../Context/AuthContext';
import { useContext } from 'react';
const Tab = createBottomTabNavigator();

export default function BottomTab() {

    const { admin } = useContext(AuthContext);

    return (

        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarStyle: { height: 50 },
                headerShown: false,
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if (route.name === 'Attendance') {
                        iconName = focused ? 'person-circle' : 'person-circle-outline';
                        size = 23;
                        return <Ionicons name={iconName} size={size} color={color} />;
                    }
                    else if (route.name === 'AdminHome') {
                        iconName = focused ? 'home' : 'home-outline';
                        size = 23;
                        return <Ionicons name={iconName} size={size} color={color} />;
                    }
                    else if (route.name === 'Inventory') {
                        iconName = 'inventory';
                        size = 20;
                        return <MaterialIcons name={iconName} size={size} color={color} />;
                    } else if (route.name === 'Customer') {
                        iconName = 'finance';
                        size = 20;
                        return <MaterialCommunityIcons name={iconName} size={size} color={color} />;
                    }
                    else if (route.name === 'Insights') {
                        iconName = 'insights';
                        size = 20;
                        return <MaterialIcons name={iconName} size={size} color={color} />;
                    }
                    else if (route.name === 'Visits') {
                        return <Image source={require('../assets/images/tourplan.png')}
                            style={{ height: 20, width: 20, tintColor: focused ? Colors.primary : 'grey' }}
                        />;
                    }



                    // Default icon rendering if no matching route name is found
                    return null;
                },
                tabBarActiveTintColor: Colors.primary,
                tabBarInactiveTintColor: 'grey',
                tabBarLabelStyle: { fontFamily: 'Poppins-Regular' }
                // Other options...
            })}
        >
            {/* admin user will have admin dashboard */}
            {admin ? (<Tab.Screen options={{ title: 'Home' }} name="AdminHome" component={AdminHome} />) :
                (<Tab.Screen options={{ title: 'Tour Plan' }} name="Visits" component={Visits} />)}
            <Tab.Screen name="Insights" component={Insides} />
            <Tab.Screen name="Inventory" component={Inventory} />
            <Tab.Screen name="Attendance" component={Attendacnce} />
            <Tab.Screen name="Customer" component={CustomerFinance} />
        </Tab.Navigator >

    );
}