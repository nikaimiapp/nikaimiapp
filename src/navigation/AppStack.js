import React, { useContext } from 'react';
import { NavigationContainer } from '@react-navigation/native';

import { ActivityIndicator, View } from 'react-native';
import UserList from '../screens/UsersList';
import Chat from '../screens/Chat';
import { createStackNavigator } from '@react-navigation/stack';
import Search from '../screens/Search';
import Login from '../screens/Login';
import BottomTab from './BottomTab';
import MIList from '../screens/MIList';
import MIListDetail from '../screens/MIListDetail';
import CreatePlan from '../screens/CreatePlan';
import OrderwiseReport from '../screens/OrderwiseReport';
import PlanDetails from '../screens/PlanDetails';
import Map from '../screens/Map';
import CheckIn from '../screens/CheckIn';
import ReturnsCart from '../screens/ReturnsCart';
import ProductDetails from '../screens/ProductDetails';
import CheckInInventory from '../screens/CheckInventory';
import CompetitorIntelligence from '../screens/CompetitorIntelligence';
import Invoice from '../screens/Invoice';
import ReturnOrderDetails from '../screens/ReturnOrderDetails';
import PreviousVisits from '../screens/PreviousVisits';
import MyPerformance from '../screens/MyPerformance';

import AdminPromotions from '../screens/AdminPromotions';
import AdminHome from '../screens/AdminHome';
import AdminOrders from '../screens/AdminOrders';
import AdminStores from '../screens/AdminStores';
import AdminOrderCart from '../screens/AdminOrderCart';
import AdminCreateOrder from '../screens/AdminCreateOrder';
// import Scanner from '../screens/AdminOrderCart/scanner';
import AdminOrderReview from '../screens/AdminOrderReview';
import SalesManagerHome from '../screens/SalesManagerHome';
import { AuthContext } from '../Context/AuthContext';
import MerchHome from '../screens/MerchHome';
import SMTourPlanDetails from '../screens/SMTourPlanDetails';
import SMApprovedPlanDetails from '../screens/SMAprrovedPlanDetails';
import SMCreatePlan from '../screens/SMCreatePlan';

import MerchAddProduct from '../screens/MerchAddProduct';
import MerchAttendance from '../screens/MerchAttendance';
import MerchInventory from '../screens/MerchInventory';
import Report from '../screens/PlanDetails/report';
import SKUHistoryList from '../screens/SKUHistoryList';
import SKUHistoryDetails from '../screens/SKUHistoryDetails';
import EditPlan from '../screens/EditPlan';
import PDFViewer from '../screens/SMAprrovedPlanDetails/pdfViewer';
export default function AppStack() {

  const { merch } = useContext(AuthContext);

  const Stack = createStackNavigator();


  return (

    <Stack.Navigator screenOptions={{ headerShown: false }} >
      {merch && <Stack.Screen name="MerchHome" component={MerchHome} />}
      <Stack.Screen name="BottomTab" component={BottomTab} />
      <Stack.Screen name="Chat" component={Chat} />
      <Stack.Screen name="Search" component={Search} />
      <Stack.Screen name="MIList" component={MIList} />
      <Stack.Screen name="MIListDetail" component={MIListDetail} />
      <Stack.Screen name="CreatePlan" component={CreatePlan} />
      <Stack.Screen name="OrderwiseReport" component={OrderwiseReport} />
      <Stack.Screen name="PlanDetails" component={PlanDetails} />
      <Stack.Screen name="Map" component={Map} />
      <Stack.Screen name="CheckIn" component={CheckIn} />

      <Stack.Screen name="ProductDetails" component={ProductDetails} />
      <Stack.Screen name="UserList" component={UserList} />
      <Stack.Screen name="CheckInInventory" component={CheckInInventory} />
      <Stack.Screen name="CompetitorIntelligence" component={CompetitorIntelligence} />
      <Stack.Screen name="Invoice" component={Invoice} />
      <Stack.Screen name="ReturnsCart" component={ReturnsCart} />
      <Stack.Screen name="ReturnOrderDetails" component={ReturnOrderDetails} />
      <Stack.Screen name="PreviousVisits" component={PreviousVisits} />
      <Stack.Screen name="MyPerformance" component={MyPerformance} />
      <Stack.Screen name="AdminOrders" component={AdminOrders} />
      <Stack.Screen name="AdminPromotions" component={AdminPromotions} />
      <Stack.Screen name="AdminHome" component={AdminHome} />
      <Stack.Screen name="AdminStores" component={AdminStores} />
      <Stack.Screen name="AdminOrderCart" component={AdminOrderCart} />
      <Stack.Screen name="AdminCreateOrder" component={AdminCreateOrder} />
      {/* <Stack.Screen name="Scanner" component={Scanner} /> */}
      <Stack.Screen name="AdminOrderReview" component={AdminOrderReview} />
      <Stack.Screen name="SMTourPlanDetails" component={SMTourPlanDetails} />
      <Stack.Screen name="SMApprovedPlanDetails" component={SMApprovedPlanDetails} />
      <Stack.Screen name="SalesManagerHome" component={SalesManagerHome} />
      <Stack.Screen name="SMCreatePlan" component={SMCreatePlan} />
      <Stack.Screen name="MerchInventory" component={MerchInventory} />
      <Stack.Screen name="MerchAddProduct" component={MerchAddProduct} />
      <Stack.Screen name="MerchAttendance" component={MerchAttendance} />
      <Stack.Screen name="Report" component={Report} />
      <Stack.Screen name="SKUHistoryList" component={SKUHistoryList} />
      <Stack.Screen name="SKUHistoryDetails" component={SKUHistoryDetails} />
      <Stack.Screen name="EditPlan" component={EditPlan} />
      <Stack.Screen name="PDFViewer" component={PDFViewer} />



























    </Stack.Navigator>

  );
}