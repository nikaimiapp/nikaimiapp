import React, { useContext,useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import AuthStack from './AuthStack';
import AppStack from './AppStack';
import { AuthContext } from '../Context/AuthContext';
import { ActivityIndicator, View } from 'react-native';
import messaging from '@react-native-firebase/messaging';
export default function Routes() {
    //context value
    const { isLoading, token } = useContext(AuthContext);


    useEffect(() => {
        // Assume a message-notification contains a "type" property in the data payload of the screen to open

        messaging().onNotificationOpenedApp(remoteMessage => {
            console.log(
                'Notification caused app to open from background state:',
                remoteMessage.notification,
            );
            //navigation.navigate(remoteMessage.data.type);
        });

        // Check whether an initial notification is available
        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                if (remoteMessage) {
                    console.log(
                        'Notification caused app to open from quit state:',
                        remoteMessage.notification,
                    );
                    //setInitialRoute(remoteMessage.data.type); // e.g. "Settings"
                }
               // setLoading(false);
            });
    }, []);

    //activity loader scrren method
    if (isLoading) {
        return (
            <View style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }} >
                <ActivityIndicator size={'large'} />
            </View>
        );

    }
    return (
        <NavigationContainer >
            {token == null ? <AuthStack /> : <AppStack />}
        </NavigationContainer>
    );
}