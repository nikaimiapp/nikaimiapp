// import React, { useEffect, useState, useContext } from 'react';
// import {
//     SafeAreaView,
//     ScrollView,
//     StatusBar,
//     StyleSheet,
//     Text,
//     useColorScheme,
//     View,
//     TextInput,
//     TouchableOpacity,
//     Alert,
//     Image
// } from 'react-native';
// import { AuthContext } from '../../Context/AuthContext';
// import messaging from '@react-native-firebase/messaging';
// const Login = ({ navigation }) => {
//     useEffect(() => {
//         getAcessToken();
//     }, [])

//     const { token, changeToken } = useContext(AuthContext)

//     const onPressLogin = () => {
//         // Do something about login operation
//         var myHeaders = new Headers();
//         myHeaders.append("Content-Type", "text/plain");
//         var raw = "{\n    \"__module_code__\":\"PO_12\",\n    \"__uname__\":\"" + state.username + "\",      \n    \"__pass__\":\"" + state.password + "\",      \n    \"__token__\":\"" + state.token + "\"\n}";
//         var requestOptions = {
//             method: 'POST',
//             headers: myHeaders,
//             body: raw,
//             redirect: 'follow'
//         };
//         Alert
//         fetch("https://dev.ordo.primesophic.com/log_in_ordo.php", requestOptions)
//             .then(response => response.json())
//             .then(result => {
//                 console.log(result);
//                 if (result.ErrorCode == "200") {
//                     console.log("login sccess");
//                     console.log("token", result?.id);
//                     changeToken(result?.id)
//                     navigation.navigate('BottomTab')

//                 } else {
//                     Alert.alert("Warning", "Invalid Credentials")
//                 }
//             })
//             .catch(error => console.log("login error", error));

//     };

//     const [state, setState] = useState({
//         username: '',
//         password: '',
//         token: ''
//     })
//     const updateusername = text => {
//         setState({ ...state, username: text });
//     };
//     const updatePassword = text => {
//         setState({ ...state, password: text });
//     };
//     const getAcessToken = async () => {
//         await messaging().registerDeviceForRemoteMessages();
//         const token = await messaging().getToken();
//         console.log("token", token)
//         setState({ ...state, token: token });


//     }

//     return (
//         <View style={styles.container}>

//             <View style={styles.titleView}>
//             <View style={styles.subimageView}>
//                 <Image style={styles.subimage}  source={require('../../assets/images/NikaLogo.png')} />
//                 <Text style={styles.title}>OrdoApp</Text>
//             </View>
//                 {/* <Text style={styles.title}>OrdoApp</Text> */}
//             </View>

//             <View>
//                 <Image style={styles.image} source={require('../../assets/images/SubWhiteBackground.jpg')} />
//             </View>
//             <View style={styles.loginContent}>
//                 <View style={styles.inputView}>
//                     <TextInput
//                         style={styles.inputText}
//                         placeholder="Username"
//                         autoCapitalize='none'
//                         placeholderTextColor="#003f5c"
//                         onChangeText={text => updateusername(text)} />
//                 </View>
//                 <View style={styles.inputView}>
//                     <TextInput
//                         style={styles.inputText}
//                         secureTextEntry
//                         placeholder="Password"
//                         placeholderTextColor="#003f5c"
//                         onChangeText={text => updatePassword(text)} />
//                 </View>
//                 <TouchableOpacity
//                     onPress={onPressLogin}
//                     style={styles.loginBtn}>
//                     <Text style={styles.loginText}>LOGIN </Text>
//                 </TouchableOpacity>
//             </View>


//         </View>
//     );
// }
// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#011A90',
//         alignItems: 'center',
//         justifyContent: 'center',
//     },
//     titleView: {
//         marginTop: 200,
//     },
//     title: {
//         marginTop:20,
//         fontWeight: "bold",
//         fontSize: 24,
//         color: "white",
//     },
//     inputView: {
//         width: "80%",
//         backgroundColor: "#d5d5d5",
//         borderRadius: 25,
//         height: 50,
//         marginBottom: 20,
//         justifyContent: "center",
//         padding: 20,
//         marginLeft: 40
//     },
//     inputText: {
//         width: 350,
//         height: 50,
//         color: "black"
//     },
//     forgotAndSignUpText: {
//         color: "black",
//         fontSize: 11
//     },
//     loginText: {
//         color: 'white'
//     },
//     loginBtn: {
//         width: "50%",
//         backgroundColor: "#011A90",
//         borderRadius: 25,
//         height: 50,
//         alignItems: "center",
//         justifyContent: "center",
//         marginTop: 40,
//         marginBottom: 10,
//         marginLeft: 100
//     },
//     image: {
//         width: 400,
//         height: 750,
//         marginLeft: 1,
//         borderRadius: 60,
//         marginTop: 50
//     },
//     loginContent: {
//         position: 'absolute',
//     },
//     subimageView: {
//         marginTop:10,
//         marginLeft: 1,
//         justifyContent:'center',
//         alignItems:'center'
//         // position: 'absolute',
//     },
//     subimage: {
//         width: 80,
//         height: 80,
//         resizeMode:'contain',
//         tintColor:'#fff'
//     },
// });
// export default Login;

import React, { useEffect, useState, useContext } from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    TextInput,
    TouchableOpacity,
    Alert,
    Image
} from 'react-native';
import { AuthContext } from '../../Context/AuthContext';
import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-async-storage/async-storage';
const Login = ({ navigation }) => {
    const { changeDealerData } = useContext(AuthContext)
    useEffect(() => {
        getAcessToken();
    }, [])

    const { token, changeToken, admin, changeAdmin, salesManager,
        setSalesManager, userData,
        setUserData, merch,
        setMerch } = useContext(AuthContext)

    const onPressLogin = () => {
        // Do something about login operation
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "text/plain");
        var raw = "{\n    \"__module_code__\":\"PO_12\",\n    \"__uname__\":\"" + state.username + "\",      \n    \"__pass__\":\"" + state.password + "\",      \n    \"__token__\":\"" + state.token + "\"\n}";
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/log_in_ordo.php", requestOptions)
            .then(response => response.json())
            .then(async result => {
                console.log(result);
                if (result.ErrorCode == "200") {
                    console.log("login sccess");
                    //admin
                    if (result?.type == "99") {
                        changeAdmin(true);
                        console.log("admin is ", admin);
                        await AsyncStorage.setItem("isAdmin", result?.id);
                    }
                    //salesmanager
                    else if (result?.type == "5") {
                        setSalesManager(true);
                        console.log("sales manager is ", salesManager);
                        await AsyncStorage.setItem("isSalesManager", result?.id);
                    }
                    //merchandiser
                    else if (result?.type == "6") {
                        setMerch(true);
                        console.log("merchandiser is ", merch);
                        await AsyncStorage.setItem("isMerch", result?.id);
                        changeDealerData({ id: result?.account_id })
                    }
                    //salesman
                    else {
                        await AsyncStorage.setItem("token", result?.id);
                        console.log("token", result?.id);
                    }
                    changeToken(result?.id);
                    //saving user data
                    await AsyncStorage.setItem("userData", JSON.stringify(result));
                    setUserData(result)

                    //changeName(result?.name)
                    //navigation.navigate('BottomTab')

                } else {
                    Alert.alert("Warning", "Invalid Credentials")
                }
            })
            .catch(error => console.log("login error", error));

    };

    const [state, setState] = useState({
        username: '',
        password: '',
        token: ''
    })
    const updateusername = text => {
        setState({ ...state, username: text });
    };
    const updatePassword = text => {
        setState({ ...state, password: text });
    };
    const getAcessToken = async () => {
        await messaging().registerDeviceForRemoteMessages();
        const token = await messaging().getToken();
        console.log("token", token)
        setState({ ...state, token: token });


    }

    return (
        <View style={styles.container}>
            <View style={styles.titleView}>
                {/* <Text style={styles.title}>OrdoApp</Text> */}
                <Image style={styles.subimage} source={require('../../assets/images/ordotext.png')} />
            </View>
            <Text style={styles.subTitle}>Join OrdoMI App</Text>
            <View style={styles.contentContainer}>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.inputText}
                        placeholder="Username"
                        autoCapitalize='none'
                        placeholderTextColor="#003f5c"
                        onChangeText={text => updateusername(text)}
                    />
                </View>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.inputText}
                        secureTextEntry
                        placeholder="Password"
                        placeholderTextColor="#003f5c"
                        onChangeText={text => updatePassword(text)}
                        autoCapitalize='none'
                    />
                </View>

            </View>
            <View style={{ marginLeft: 41, marginRight: 40 }}>
                <Text style={{
                    fontFamily: 'Poppins-Regular',
                    fontSize: 12,
                    color: 'grey'
                }}>
                    By clicking Login You Agree to the OrdoMI
                </Text>
                <Text style={styles.agreementColor}>
                    User Agreement, Privacy Policy and  Cookie Policy
                </Text>
            </View>
            <TouchableOpacity
                onPress={onPressLogin}
                style={styles.loginBtn}>
                <Text style={styles.loginText}>LOGIN </Text>
            </TouchableOpacity>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFFFF',
        // backgroundColor: 'yellow',
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    contentContainer: {
        // backgroundColor: 'green',
        backgroundColor: '#FFFFFF',
        alignItems: 'center',
        marginTop: 10,
        // justifyContent: 'center',
    },
    titleView: {
        justifyContent: "center",
        marginLeft: 32,
    },
    title: {
        fontWeight: "bold",
        fontSize: 20,
        color: "#011A90",
        marginBottom: 40,
        marginLeft: 20
    },
    subTitle: {
        //fontWeight: "400",
        fontFamily: 'Poppins-SemiBold',
        fontSize: 24,
        color: "black",
        marginBottom: 10,
        marginLeft: 38
    },
    inputView: {
        width: "80%",
        borderWidth: 1,
        backgroundColor: "white",
        borderRadius: 1,
        height: 50,
        marginBottom: 20,
        justifyContent: "center",
        padding: 20,
        paddingLeft: 5,

    },
    inputText: {
        height: 50,
        color: "black",
        fontFamily: 'Poppins-Regular',
    },
    forgotAndSignUpText: {
        color: "black",
        fontSize: 14,
        fontFamily: 'Poppins-Regular',
    },
    loginText: {
        color: 'white',
        fontFamily: 'Poppins-Bold',

    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#011A90",
        borderRadius: 30,
        height: 60,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 20,
        marginBottom: 10,
        marginLeft: 41
    },
    agreementColor: {
        color: "grey",
        fontFamily: 'Poppins-Regular',
        fontSize: 12
    },
    subimage: {
        width: 100,
        height: 80
    },

});
export default Login;


