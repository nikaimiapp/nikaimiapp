import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import Colors from '../../constants/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { ScrollView } from 'react-native-gesture-handler';
import moment from 'moment';
const ProductDetails = ({ navigation, route }) => {
    const [text, setText] = useState('');
    const { item } = route.params;
    const [desc, setDesc] = useState('');
    console.log("item", item)

    useEffect(() => {
        //repalcing ' and " values in description
        let sQuote = item.ldescription.replaceAll('&#039;', `'`)
        let dQuote = sQuote.replaceAll('&quot;', `"`);
        setDesc(dQuote)

    }, [])


    const handleInputChange = (inputText) => {
        setText(inputText);
    };

    return (
        <View style={styles.container}>
            <View style={{ ...styles.headercontainer }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <AntDesign name='arrowleft' size={25} color={Colors.black} />
                </TouchableOpacity>
                <Text style={styles.headerTitle}>Item Details</Text>
            </View>

            <View style={styles.imageView}>
                <Image
                    source={{ uri: item.imgsrc }}
                    style={{ width: 200, height: 200, marginTop: 40, marginBottom: 20, resizeMode: 'contain' }}
                />
                {/* <View style={styles.quantityView}>
                    <TouchableOpacity style={styles.quantityButtonView}>
                        <Text style={styles.buttonText}>-</Text>
                    </TouchableOpacity>
                    <View style={styles.quantityCountView}>
                        <TextInput
                            style={styles.inputText}
                            onChangeText={handleInputChange}
                            value={text}
                            placeholder="0"
                        />
                    </View>
                    <TouchableOpacity style={styles.quantityButtonView}>
                        <Text style={styles.buttonText}>+</Text>
                    </TouchableOpacity>
                </View> */}
            </View>

            <View style={styles.itemDescriptionView}>
                <ScrollView>
                    <Text style={styles.title}>{item.description}</Text>
                    <Text style={styles.rupees}>AED {Number(item.price)}</Text>
                    <Text style={styles.items}>Manufactured Date: {moment(item.manufactured_date).format('DD-MM-YYYY')}</Text>
                    <Text style={styles.description}>Description</Text>
                    <Text style={styles.content}>{desc}</Text>
                    <Text style={styles.items}>Item Code: {item.itemid}</Text>
                    <Text style={styles.items}>HSN: {item.hsn}</Text>
                    {/* <Text style={styles.items}>MOQ: 12</Text> */}
                    <Text style={styles.stock}>On hand Stock: {item.stock}</Text>
                </ScrollView>
            </View>

        </View>
    )
}

export default ProductDetails

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    imageView: {
        flex: 0.4,
        backgroundColor: 'white',
        // justifyContent: 'center', 
        alignItems: 'center',
    },
    quantityButtonView: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        // paddingVertical: 5,
        // paddingHorizontal: 15,
        height: 45,
        width: 50,
        marginRight: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    quantityCountView: {
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        height: 45,
        width: 100,
        marginRight: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText: {
        color: 'black',
        fontFamily: 'Poppins-Regular',
        fontSize: 20,
        alignSelf: 'center'
    },
    quantityView: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 50,
    },
    title: {
        marginTop: 5,
        marginLeft: 10,
        fontSize: 23,
        fontFamily: 'Poppins-SemiBold',
        color: 'black'
    },
    rupees: {
        marginLeft: 10,
        fontSize: 18,
        fontFamily: 'Poppins-Regular',
        color: 'red',
    },
    description: {
        marginTop: 5,
        marginLeft: 10,
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'black',
    },
    content: {
        marginLeft: 10,
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'black',
        marginBottom: 20
    },
    items: {
        marginLeft: 10,
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'black',
    },
    stock: {
        marginLeft: 10,
        fontSize: 15,
        fontFamily: 'Poppins-SemiBold',
        color: 'black',
    },
    itemDescriptionView: {
        flex: 0.6,
        backgroundColor: '#f1f1f1',
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25,
        paddingHorizontal: 10,
        paddingVertical: 10,
        // backgroundColor:'red'

    },
    headercontainer: {
        padding: 10,
        //backgroundColor:'red',
        flexDirection: 'row',
        alignItems: 'center',

    },
    headerTitle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: Colors.black,
        marginLeft: 10,
        marginTop: 3
    },
    value: {
        fontFamily: 'Poppins-Regular',
        color: 'grey'

    }
})