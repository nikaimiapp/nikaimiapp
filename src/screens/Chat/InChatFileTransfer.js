import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const InChatFileTransfer = ({ filePath }) => {
  var fileType = '';
  var name = '';
  const [img, setImg] = useState('');
  if (filePath !== undefined) {
    name = filePath.split('/').pop();
    fileType = filePath.split('.').pop();
  }


  useEffect(() => {
    fileType = filePath.split('.').pop();
    //pdf icon
    if (fileType == 'pdf') {
      setImg(require('../../assets/images/pdf.png'))
    }
    //gogole docs icon
    else if (fileType == 'docx' || fileType == 'doc') {
      setImg(require('../../assets/images/docs.png'))
    }
    //excel sheet icon
    else if (fileType == 'xlsx' || fileType == 'csv' || fileType == 'xls') {
      setImg(require('../../assets/images/sheets.png'))
    }
    //ppt icon
    else if (fileType == 'ppt' || fileType == 'pptx') {
      setImg(require('../../assets/images/ppt.png'))
    }
    else if (fileType == 'mp3') {
      setImg(require('../../assets/images/music.png'))
    }
  }, [])
  return (
    <View style={styles.container}>
      <View
        style={styles.frame}
      >
        <Image
          source={img ? img : require('../../assets/images/unknownfile.png')}
          style={{ height: 60, width: 60 }}
        />
        <View style={{ flex: 1 }}>
          <Text style={styles.text}>
            {name.replace('%20', '').replace(' ', '')}
          </Text>
          <Text style={styles.textType}>{fileType.toUpperCase()}</Text>
        </View>
      </View>
    </View>
  );
};
export default InChatFileTransfer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 5,
    borderRadius: 15,
    padding: 5,
  },
  text: {
    color: 'black',
    marginTop: 10,
    fontSize: 16,
    lineHeight: 20,
    marginLeft: 5,
    marginRight: 5,
  },
  textType: {
    color: 'black',
    //marginTop: 5,
    fontSize: 14,
    fontWeight: 'bold',
    marginLeft: 10,
    marginLeft: 5,
    marginRight: 5,

  },
  frame: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    borderRadius: 10,
    padding: 5,
    marginTop: -4,
  },
});