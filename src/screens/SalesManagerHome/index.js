import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList, Alert } from 'react-native'
import React, { useContext, useEffect } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../constants/Colors';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { AuthContext } from '../../Context/AuthContext';
import { useFocusEffect } from '@react-navigation/native';
import moment from 'moment';


function ApprovedPlans({ navigation }) {
    const {
        approvedArray,
    } = useContext(AuthContext)
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <FlatList
                // data={dealerArray}
                data={approvedArray}
                renderItem={({ item }) => {
                    let color = item.status == 'Approved' ? 'green' : item.status == 'PendingApproval' ? 'orange' : 'red';
                    return (

                        <TouchableOpacity
                            style={styles.itemContainer}
                            onPress={() => navigation.navigate('SMApprovedPlanDetails', { item: item })}
                            // key={item.id}
                            activeOpacity={0.5}
                        >
                            <View style={{ paddingHorizontal: 15, }}>
                                <View style={{ marginVertical: 3 }}>
                                    {/* <Text style={styles.heading}>Plan Name : {item.name} </Text>
                            <Text style={styles.value}>Approved By : {item.approved_by}</Text>
                            <Text style={styles.heading}>No. of Visit : {item?.dealer_count} </Text>
                            <Text style={styles.heading}>Start Date : {item.start_date} </Text>
                            <Text style={styles.heading}>End Date : {item.end_date} </Text>
                            <Text style={styles.heading}>Status : {item.status} </Text> */}


                                    {/* <Text style={styles.heading}>Salesman Name : {item?.ordo_user_name}</Text>
                                    <Text style={styles.heading}>Plan Name : {item?.name}</Text>
                                    <Text style={styles.heading}>No. of Visit : {item?.dealer_count}</Text>
                                    <Text style={styles.heading}>Start Date : {item?.start_date}</Text>
                                    <Text style={styles.heading}>End Date : {item.end_date}</Text> */}
                                    <View style={{ flexDirection: 'row', }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={styles.heading}>Name:</Text>
                                            <Text style={{ ...styles.heading, fontFamily: 'Poppins-SemiBold' }}>{item.ordo_user_name}</Text>
                                        </View>
                                        <View style={{ flex: 1.6, marginLeft: 5 }}>
                                            <Text style={styles.heading}>Plan Name :</Text>
                                            <Text style={{ ...styles.heading, color: Colors.primary, fontFamily: 'Poppins-SemiBold' }}>{item.name} </Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                        <View style={{ flex: 1 }}>
                                            {/* <Text style={{ ...styles.heading, fontFamily: 'Poppins-Regular' }}>Status :</Text>
                                            <Text style={{ ...styles.heading, color: color, fontFamily: 'Poppins-SemiBold' }}>{item.status == 'PendingApproval' ? 'In Progress' : item.status} </Text> */}
                                            <Text style={{ ...styles.heading, color: 'black' }}>No. of Visits :</Text>
                                            <Text style={{ ...styles.heading, color: 'black' }}>#{item?.dealer_count}</Text>
                                        </View>
                                        <View style={{ flex: 1.6, marginLeft: 5 }}>
                                            <Text style={styles.heading}>Plan Duration :</Text>
                                            <Text style={{ ...styles.heading, color: 'grey' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}  </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    );
}

function TourPlans({ navigation }) {

    const {
        tourPlanArray,
    } = useContext(AuthContext)
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <FlatList
                // data={dealerArray}
                data={tourPlanArray}
                renderItem={({ item }) => {
                    let color = item.status == 'Approved' ? 'green' : item.status == 'PendingApproval' ? 'orange' : 'red';
                    return (

                        <TouchableOpacity
                            style={styles.itemContainer}
                            onPress={() => navigation.navigate('SMTourPlanDetails', { item: item })}
                            // key={item.id}
                            activeOpacity={0.5}
                        >
                            <View style={{ paddingHorizontal: 15, }}>
                                <View style={{ marginVertical: 3 }}>
                                    {/* <Text style={styles.heading}>Plan Name : {item.name} </Text>
                        <Text style={styles.value}>Approved By : {item.approved_by}</Text>
                        <Text style={styles.heading}>No. of Visit : {item?.dealer_count} </Text>
                        <Text style={styles.heading}>Start Date : {item.start_date} </Text>
                        <Text style={styles.heading}>End Date : {item.end_date} </Text>
                        <Text style={styles.heading}>Status : {item.status} </Text> */}


                                    {/* <Text style={styles.heading}>Salesman Name : {item?.ordo_user_name}</Text>
                                <Text style={styles.heading}>Plan Name : {item?.name}</Text>
                                <Text style={styles.heading}>No. of Visit : {item?.dealer_count}</Text>
                                <Text style={styles.heading}>Start Date : {item?.start_date}</Text>
                                <Text style={styles.heading}>End Date : {item.end_date}</Text> */}
                                    <View style={{ flexDirection: 'row', }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={styles.heading}>Name:</Text>
                                            <Text style={{ ...styles.heading, fontFamily: 'Poppins-SemiBold' }}>{item.ordo_user_name}</Text>
                                        </View>
                                        <View style={{ flex: 1.6, marginLeft: 5 }}>
                                            <Text style={styles.heading}>Plan Name :</Text>
                                            <Text style={{ ...styles.heading, color: Colors.primary, fontFamily: 'Poppins-SemiBold' }}>{item.name} </Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                                        <View style={{ flex: 1 }}>
                                            {/* <Text style={{ ...styles.heading, fontFamily: 'Poppins-Regular' }}>Status :</Text>
                                        <Text style={{ ...styles.heading, color: color, fontFamily: 'Poppins-SemiBold' }}>{item.status == 'PendingApproval' ? 'In Progress' : item.status} </Text> */}
                                            <Text style={{ ...styles.heading, color: 'black' }}>No. of Visits :</Text>
                                            <Text style={{ ...styles.heading, color: 'black' }}>#{item?.dealer_count}</Text>
                                        </View>
                                        <View style={{ flex: 1.6, marginLeft: 5 }}>
                                            <Text style={styles.heading}>Plan Duration :</Text>
                                            <Text style={{ ...styles.heading, color: 'grey' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}   </Text>
                                        </View>
                                    </View>
                                </View>
                            </View>

                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    );
}


const Tab = createMaterialTopTabNavigator();

function MyTabs() {
    return (

        <Tab.Navigator

            screenOptions={{
                tabBarActiveTintColor: 'black',
                tabBarLabelStyle: { fontSize: 12 },
                tabBarStyle: { backgroundColor: 'white' },
            }}
        >
            <Tab.Screen
                name="TourPlans"
                component={TourPlans}
                options={{ title: 'Pending Approval', tabBarLabelStyle: { fontFamily: 'Poppins-SemiBold', textTransform: 'capitalize' } }}
            />
            <Tab.Screen
                name="ApprovedPlans"
                component={ApprovedPlans}
                options={{ title: 'Approved', tabBarLabelStyle: { fontFamily: 'Poppins-SemiBold', textTransform: 'capitalize' } }}
            />

        </Tab.Navigator>

    );
}

const SalesManagerHome = ({ navigation }) => {

    const {
        token,
        approvedArray,
        setApprovedArray,
        tourPlanArray,
        setTourPlanArray,
        logout } = useContext(AuthContext)
    const getPlans = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__user_id__": token
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_tour_plan.php", requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log('get plans data \n', result?.tour_plan)
                let approved = [];  //approved plans
                let tourPlans = [];  //reject,pending plans  all are other plans
                result?.tour_plan.forEach((item) => {
                    //approved plans 
                    console.log("item", item)
                    if (item?.status == 'Approved') {
                        approved.push(item);
                    }
                    //tour plans
                    else if (item?.status == 'PendingApproval') {
                        tourPlans.push(item)
                    }

                })
                //sorting approved plans array
                let sortedarray = approved.sort((a, b) => (a.start_date < b.start_date) ? 1 : -1)
                setApprovedArray(sortedarray)
                //sorting other  plans array
                let sortedarray2 = tourPlans.sort((a, b) => (a.start_date < b.start_date) ? 1 : -1)
                setTourPlanArray(sortedarray2);
                console.log("approved plan", sortedarray);
                console.log("tour plan", sortedarray2);
            })
            .catch(error => console.log('error', error));



    }

    useFocusEffect(
        React.useCallback(() => {
            getPlans();

        }, []))

    const logoutAlert = () => {
        Alert.alert('Confirmation', 'Are you sure, You want to logout?', [
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            { text: 'OK', onPress: () => { logout() } },
        ]);
    }



    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginLeft: 15, marginTop: 10, alignItems: 'center' }}>

                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity style={{ marginBottom: 7 }} onPress={() => navigation.goBack()}>
                        <AntDesign name='arrowleft' size={25} color={Colors.black} />
                    </TouchableOpacity>

                    <Image source={require('../../assets/images/ordotext.png')} style={{ height: 40, width: 100 }} />
                </View>
                {/* <View style={{ alignItems: 'center', flexDirection: 'row', paddingBottom: 10, flex: 1, justifyContent: 'flex-end',marginRight:10 }}>
                    <TouchableOpacity onPress={logoutAlert}>
                        <Image source={require('../../assets/images/power-off.png')} style={{ height: 20, width: 20, tintColor: Colors.primary, marginLeft: 20, marginRight: 10 }} />
                    </TouchableOpacity>
                </View> */}
            </View>
            {/* <View style={{ paddingHorizontal: 17, marginTop: 10 }}>
                <Text style={styles.buttonTextStyle}>Welcome Sales Manager</Text>
            </View> */}
            <View style={{ flex: 1 }} >
                <MyTabs />
            </View>
        </View>
    )
}

export default SalesManagerHome

const styles = StyleSheet.create({
    itemContainer: {
        marginTop: 10,
        marginLeft: 16,
        marginRight: 16,
        borderRadius: 8,
        backgroundColor: Colors.white,
        borderWidth: 1,
        padding: 10,
        borderColor: Colors.white,
        elevation: 3,
        marginBottom: 5,
    },
    heading: {
        color: '#000',
        fontFamily: 'Poppins-Regular',
        fontSize: 14,
    },
    value: {
        color: 'black',
        fontFamily: 'Poppins-Regular'
    },
    buttonTextStyle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
    },
})