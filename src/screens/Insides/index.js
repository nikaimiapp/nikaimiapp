import React, { useContext, useEffect, useState } from 'react'

import { AuthContext } from '../../Context/AuthContext';
import { View, Dimensions, Text, ScrollView, StyleSheet, TouchableOpacity } from 'react-native'
import { BarChart } from "react-native-gifted-charts";
import Colors from '../../constants/Colors';
import uniqolor from 'uniqolor';
import { useFocusEffect } from '@react-navigation/native';
import { Image } from 'react-native-animatable';
import { MapView } from '@rnmapbox/maps';
import PercentageCircle from 'react-native-percentage-circle';
import { PieChart } from "react-native-gifted-charts";
const screenWidth = Dimensions.get('window').width;
const Insights = ({ navigation }) => {

  // const pieData = [
  //   {
  //     name: 'Walmart', population: 500, color: 'blue', legendFontFamily: 'Poppins-Regular',
  //     legendFontSize: 12,legendFontColor:Colors.primary,
  //   },
  //   { name: 'Fantasy Light Bookstore', population: 700, color: '#36A2EB' },
  //   { name: 'E G India', population: 1000, color: '#FFCE56' },
  //   { name: 'Silicon Electronics', population: 300, color: '#4BC0C0' },
  //   { name: 'Section 5', population: 900, color: '#9966FF' },

  // ];

  // const barData =
  //   [
  //     { value: 15, label: 'Walmart', frontColor: '#FF6384' },
  //     { value: 30, label: 'Fantasy Light Bookstore', frontColor: '#36A2EB' },
  //     { value: 26, label: 'E G India', frontColor: '#FFCE56' },
  //     { value: 40, label: 'Silicon Electronics', frontColor: '#4BC0C0' },
  //     { value: 30, label: 'Amul', frontColor: '#9966FF' },
  //     { value: 26, label: 'Dairy Milk', frontColor: '#9988FF' },
  //     { value: 40, label: 'abc', frontColor: '#FF1256' },

  //   ]

  // //chart stylev file
  // const chartConfig = {
  //   backgroundColor: '#ffffff',
  //   backgroundGradientFrom: '#ffffff',
  //   backgroundGradientTo: '#ffffff',
  //   decimalPlaces: 0,
  //   color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  //   legendConfig: {
  //     width: 80,
  //     height: 20,
  //     labelFontSize: 100,
  //     //color:Colors.primary
  //   }
  // };

  // //returns random color
  // const getRandomColor = () => {
  //   const letters = '0123456789ABCDEF';
  //   let color = '#';

  //   for (let i = 0; i < 6; i++) {
  //     color += letters[Math.floor(Math.random() * 16)];
  //   }

  //   return color;
  // };

  //auth token value
  const { token, aprrovedPlans } = useContext(AuthContext);
  const [totalRevenue, setTotalRevenue] = useState(0);
  //tour plan target completed hooks
  const [assignedVisits, setAssignedVisits] = useState(0);
  const [cmpltdVisits, setCmpltdVisits] = useState(0);
  const [targetPercent, setTargetPercent] = useState(0);

  const [salesPerformance, setSalesPerformance] = useState('');
  const [salesPercent, setSalesPercent] = useState(0);


  //Bar Graph data logic
  const [weeklyData, setWeeklyData] = useState([]);
  const [monthData, setMonthData] = useState([]);
  const [mTotalsales, setMTotalSales] = useState(0);
  const [wTotalsales, setWTotalSales] = useState(0);
  const [wSelected, setWSelected] = useState(true);
  const [mSelected, setMSelected] = useState(false);



  // const [pieChartData, sertPieChartData] = useState([]);
  console.log("approved plans 123 ", aprrovedPlans[0])

  //getting dealer array
  const getInsightData = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "__user_id__": token,
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://dev.ordo.primesophic.com/get_insights_report.php", requestOptions)
      .then(response => response.json())
      .then(async result => {
        console.log("insight result", result);
        console.log("sales", result?.sales);
        console.log("total sales", result?.sales.quarterly_sales_report[0].total_sales);
        //setting total revenue
        if (result?.sales.quarterly_sales_report[0].total_sales > 0) {
          setTotalRevenue(result?.sales.quarterly_sales_report[0].total_sales)
        }
        else {
          setTotalRevenue(0)
        }
        console.log("sales perfomarnce", result.sales_performa);
        let salesPerformance = result.sales_performa
        console.log("anual target", salesPerformance.quarterly_sales_performa_report.sales_performa_quarterly[0]);
        setSalesPerformance(salesPerformance.quarterly_sales_performa_report.sales_performa_quarterly[0])
        //calculating sales performance percentage
        let val = salesPerformance.quarterly_sales_performa_report.sales_performa_quarterly[0];
        if (Number(val.Annual_Target) > 0 && Number(val.Annual_Achieved_Target) > 0) {
          let percentage = (Number(val.Annual_Achieved_Target) / Number(val.Annual_Target)) * 100;
          console.log("annual sales percentage  is", percentage);
          setSalesPercent(percentage);
        }
        else {
          setSalesPercent(0);
        }

        //bar graph logic
        //month
        let monthArrray = result?.sales.quarterly_sales_report[1];
        let m_total = result?.sales.quarterly_sales_report[0].total_sales;
        setMTotalSales(m_total > 0 ? m_total : 0)
        console.log("monthy total sales", m_total);
        console.log("month array", monthArrray);

        let tmpmonthdata = await monthArrray.sales_report_monthly.map((item) => {
          return {
            value: parseInt(item?.lastpaid),
            label: item.month,
          };
        });
        console.log("monthgraph  data", tmpmonthdata);
        setMonthData(tmpmonthdata);


        //week
        let weekArrray = result?.sales.quarterly_sales_report[3];
        let w_total = result?.sales.quarterly_sales_report[2].total_weekly_sales;
        setWTotalSales(w_total > 0 ? w_total : 0)
        console.log("weekly total sales", w_total);
        console.log("week array", weekArrray);

        let tmpweekdata = await weekArrray.current_month_weekly_sales.map((item) => {
          return {
            value: parseInt(item?.lastpaid),
            label: item?.week,
          };
        });
        console.log("weekgraph  data", tmpweekdata);
        setWeeklyData(tmpweekdata);

      })
      .catch(error => console.log('error', error));
  }

  const [pieChartData, sertPieChartData] = useState([]);
  const getActivePlanDetails = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "__user_id__": token,
      "__id__": aprrovedPlans[0]?.id
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://dev.ordo.primesophic.com/get_tour_plan_detail.php", requestOptions)
      .then(response => response.json())
      .then(async result => {
        console.log('active plans data ', result)
        let tmpData = await result?.status[0].dealer_array.map(item => {
          return {
            //pie chart data
            name: item?.name,
            value: Number(item?.no_of_visit),
            color: item?.bg_color_c
          }
        });
        console.log("data", tmpData)
        sertPieChartData(tmpData)
      })
      .catch(error => console.log('api error', error));
  }


  useFocusEffect(
    React.useCallback(() => {
      //getting insights data
      getInsightData()
      getActivePlanDetails();
      //Target acommplished graph logic
      //checking he has active plans
      if (aprrovedPlans.length > 0) {
        //cacluating completed percentage
        let percentage = 0;
        console.log("assinged ", aprrovedPlans[0].total_visits)
        console.log("comleted ", aprrovedPlans[0].completed_visits)
        if (Number(aprrovedPlans[0].total_visits) > 0 && Number(aprrovedPlans[0].completed_visits) > 0) {
          percentage = (Number(aprrovedPlans[0].completed_visits) / Number(aprrovedPlans[0].total_visits)) * 100;
          console.log("completed percentage  is", percentage);
          setTargetPercent(percentage);
        }
        else {
          console.log("completed percentage  is", 0);
          setTargetPercent(0);
        }
        setAssignedVisits(aprrovedPlans[0].total_visits);
        setCmpltdVisits(aprrovedPlans[0].completed_visits)
      }
      //dont have active plan
      else {
        setAssignedVisits(0);
        setCmpltdVisits(0);
        setTargetPercent(0)
      }
    }, [])
  );

  //pie chart custom legend
  const renderLegend = (text, color) => {
    return (
      <View style={{ flexDirection: 'row', marginBottom: 5 }}>
        <View
          style={{
            height: 18,
            width: 18,
            marginRight: 10,
            borderRadius: 4,
            backgroundColor: color || 'white',
          }}
        />
        <Text style={{ color: 'black', fontSize: 14, fontFamily: 'Poppins-Regular' }}>{text || ''}</Text>
      </View>
    );
  };




  return (
    <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
      <View style={styles.salesContainer}>
        <Image
          style={{ height: 40, width: 40, tintColor: Colors.primary }}
          source={require('../../assets/images/trend.png')}
        />
        <View style={{ marginLeft: 10 }}>
          <Text style={styles.total}>{totalRevenue} AED</Text>
          <Text style={styles.label}>Total Sales Revenue</Text>
        </View>

      </View>


      {pieChartData.length > 0 && <View style={{
        backgroundColor: '#fff',
        marginVertical: 8,
        borderRadius: 8,
        marginHorizontal: 16,
        padding: 10,
        elevation: 5,
        //backgroundColor:'red'
      }}>
        {/* Custom Pie chart */}

        <View
          style={{
            //marginVertical: 100,
            //marginHorizontal: 30,
            //marginBottom: 20,
            borderRadius: 10,
            //padding:16,
            //paddingVertical: 50,
            backgroundColor: Colors.white,
            justifyContent: 'center',
            alignItems: 'center',
          }}>


          {/*********************    Custom Header component      ********************/}
          <Text
            style={{
              color: 'black',
              fontSize: 20,
              fontFamily: 'Poppins-SemiBold',
              //marginBottom: 12,
            }}>
            Active Plan Status
          </Text>
          {/****************************************************************************/}


          <PieChart
            radius={100}
            strokeColor="white"
            strokeWidth={4}
            donut
            data={pieChartData}
            // innerCircleColor="#414141"
            innerCircleBorderWidth={4}

            //innerCircleBorderColor={'white'}


            showValuesAsLabels={true}
            showText
            textSize={16}
            textColor='#000'

            font='Poppins-Regular'
            showTextBackground={true}
            textBackgroundRadius={16}
          // //showTextBackground={true}  
          // centerLabelComponent={() => {
          //   return (
          //     <View>
          //       <Text style={{ color: Colors.primary, fontSize: 30,fontFamily:'Poppins-SemiBold' }}>90</Text>
          //       <Text style={{ color:Colors.primary, fontSize: 16,fontFamily:'Poppins-Regular' }}>Vists</Text>
          //     </View>
          //   );
          // }}
          />


          {/*********************    Custom Legend component      ********************/}
          <View
            style={{
              width: '100%',
              //flexDirection: 'row',
              //justifyContent: 'space-evenly',
              //marginTop: 20,
            }}>
            {
              pieChartData.map((item) => {
                return (renderLegend(item.name, item.color))
              })
            }
            {/* {renderLegend('Jan', 'rgb(84,219,234)')}
            {renderLegend('Feb', 'lightgreen')}
            {renderLegend('Mar', 'orange')} */}
          </View>
          {/****************************************************************************/}


        </View>
      </View>}


      <View style={styles.performanceContainer}>
        {/* <Text style={styles.label}>Sales Performace</Text> */}
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 1 }}>
            <Text style={{ ...styles.label, alignSelf: 'center', fontSize: 14, color: Colors.primary, marginVertical: 5 }}>Target Accomplished</Text>
            <PercentageCircle
              radius={60}
              percent={targetPercent}
              color={Colors.primary}
              borderWidth={4}
            >
              <Text style={styles.value}>{targetPercent.toFixed(0)}%</Text>
            </PercentageCircle>
            <View style={{ marginTop: 10 }}>
              <Text style={styles.subHeading}>Assigned Visits : {assignedVisits} </Text>
              <Text style={styles.subHeading}>Completed Visits : {cmpltdVisits} </Text>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={{ ...styles.label, alignSelf: 'center', fontSize: 14, color: Colors.primary, marginVertical: 5 }}>Annual Sales</Text>
            <PercentageCircle
              radius={60}
              percent={salesPercent}
              color={Colors.primary}
              borderWidth={4}
            >
              <Text style={styles.value}>{salesPercent.toFixed(2)}%</Text>
            </PercentageCircle>
            <View style={{ marginTop: 10, marginLeft: 20 }}>
              <Text style={styles.subHeading}>Target : {salesPerformance.Annual_Target > 0 ? Number(salesPerformance.Annual_Target) : 0} </Text>
              <Text style={styles.subHeading}>Achieved : {salesPerformance.Annual_Achieved_Target > 0 ? Number(salesPerformance.Annual_Achieved_Target) : 0} </Text>
            </View>
          </View>

        </View>

      </View>



      <View style={{
        backgroundColor: '#fff',
        marginVertical: 8,
        borderRadius: 8,
        marginHorizontal: 16,
        padding: 10,
        elevation: 5,
        //backgroundColor:'red'
      }}>


        <Text style={styles.label}>Sales Performance</Text>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>

          <Text style={{ ...styles.subHeading, color: '#000' }}>Total Sales : <Text style={{ fontFamily: 'Poppins-SemiBold' }}>{wSelected ? wTotalsales : mTotalsales} AED</Text> </Text>
          <View style={{ alignSelf: 'flex-end', flexDirection: 'row', alignItems: 'center', padding: 4, backgroundColor: Colors.grey, borderRadius: 5, elevation: 3 }}>
            <Text></Text>
            <TouchableOpacity style={{
              backgroundColor: wSelected ? 'white' : Colors.grey,
              elevation: wSelected ? 5 : 0,
              paddingHorizontal: 16,
              borderRadius: 2,
              alignItems: 'center',
              justifyContent: 'center'
            }}
              onPress={() => {
                setWSelected(true);
                setMSelected(false);
              }}
            >
              <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Poppins-SemiBold' }}>W</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{
              backgroundColor: mSelected ? 'white' : Colors.grey,
              elevation: mSelected ? 5 : 0,
              paddingHorizontal: 16,
              borderRadius: 2,
              alignItems: 'center',
              justifyContent: 'center'
            }}
              onPress={() => {
                setMSelected(true);
                setWSelected(false);
              }}
            >
              <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Poppins-SemiBold' }}>M</Text>
            </TouchableOpacity>



          </View>

        </View>


        {weeklyData.length > 0 && monthData.length > 0 && <BarChart
          height={150}
          frontColor={Colors.primary}
          noOfSections={4}
          showFractionalValue
          showYAxisIndices
          data={mSelected ? monthData : weeklyData}

          xAxisLabelTextStyle={{ fontSize: 7, textAlign: 'center', fontFamily: 'Poppins-Regular' }}
          yAxisTextStyle={{ fontSize: 8, fontFamily: 'Poppins-Regular' }}
          isAnimated
          barWidth={25}
          disablePress
          spacing={25}
          hideRules={true}
        //yAxisThickness={0}
        //xAxisThickness={0}






        />}


      </View>

      {/* <View style={{ flex: 1, backgroundColor: '#fff' }}>
        {pieChartData.length > 0 && <View>
          <Text style={{ ...styles.labelText }}>Active Plans</Text>
          <ScrollView horizontal >
            <PieChart
              data={pieChartData}
              width={450}
              height={240}
              chartConfig={chartConfig}
              accessor="value"
              backgroundColor="transparent"
            />
          </ScrollView>
        </View>}
        {pieChartData.length > 0 && <View>
          <Text style={{ ...styles.labelText, marginBottom: 16, marginTop: 30 }}>Customer Visit Graph</Text>
          <BarChart
            height={230}
            noOfSections={4}
            frontColor={Colors.primary}
            disablePress={true}
            initialSpacing={20}
            xAxisLabelTextStyle={{ fontSize: 8,color:Colors.primary }}
            yAxisTextStyle={{ fontSize: 8, fontFamily: 'Poppins-Regular',color:Colors.primary }}
            spacing={40}
            //rotateLabel
            barWidth={20}
            labelWidth={40}
            //xAxisIndicesWidth={30}
            data={pieChartData}

          />

        </View>}
        <TouchableOpacity style={styles.createPlanBtn}
          onPress={() => { navigation.navigate('MyPerformance') }}
        >
          <Text style={styles.buttonTextStyle}>My Performace</Text>
        </TouchableOpacity>
      </View> */}
    </ScrollView>
  )
}

export default Insights;
const styles = StyleSheet.create({
  salesContainer: {
    marginHorizontal: 16,
    padding: 10,
    backgroundColor: Colors.white,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 16,
    marginVertical: 10,
    elevation: 5,
    borderRadius: 5,
    marginTop: 20,



  },
  total: {
    fontSize: 18,
    color: Colors.primary,
    fontFamily: 'Poppins-Bold',

  },
  label: {
    fontSize: 16,
    color: Colors.black,
    fontFamily: 'Poppins-SemiBold',
  },
  performanceContainer: {
    marginHorizontal: 16,
    padding: 10,
    backgroundColor: Colors.white,
    paddingHorizontal: 16,
    marginVertical: 10,
    elevation: 5,
    borderRadius: 5,
  },
  value: {
    fontSize: 20,
    color: Colors.primary,
    fontFamily: 'Poppins-SemiBold',
  },
  subHeading: {
    fontSize: 13,
    color: 'grey',
    fontFamily: 'Poppins-Regular',
  }
})