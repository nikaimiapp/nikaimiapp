import { StyleSheet, Text, View, Image, FlatList, ActivityIndicator, TouchableOpacity, Keyboard, TextInput, Modal, Pressable } from 'react-native'
import React, { useState, useEffect } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../constants/Colors';
import { useFocusEffect } from "@react-navigation/native";


const Inventory = ({ navigation }) => {
    const [masterData, setMasterData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [loading, setLoading] = useState(false)
    const [search, setSearch] = useState('');
    useEffect(() => {
        loadAllProduct()

    }, [])

    // useFocusEffect(
    //     React.useCallback(() => {
    //         loadAllProduct()


    //     }, [])
    // );


    const [cartData, setCartData] = useState([]);

    const loadAllProduct = async () => {
        setLoading(true);
        console.log("loading all product");
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__module_code__": "PO_20",
            "__query__": "",
            "__orderby__": "",
            "__offset__": 0,
            "__select _fields__": [
                "id",
                "name"
            ],
            "__max_result__": 500,
            "__delete__": 0
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_data_s.php", requestOptions)
            .then(response => response.json())
            .then(async result => {
                //console.log('all product data fetched');

                tempArray = await result?.entry_list.map(object => {
                    console.log("og values", object.name_value_list)
                    return {
                        'itemid': object.name_value_list.part_number.value,
                        'description': object.name_value_list.name.value,
                        'ldescription': object.name_value_list.description.value,
                        'price': object.name_value_list.price.value,
                        'qty': 0, 'upc': object.name_value_list.upc_c.value,
                        'category': object.name_value_list.category.value,
                        'subcategory': object.name_value_list.subcategory_c.value,
                        'unitofmeasure': object.name_value_list.unitofmeasure_c.value,
                        'manufacturer': object.name_value_list.manufacturer_c.value,
                        'class': object.name_value_list.class_c.value,
                        'pack': object.name_value_list.pack_c.value,
                        'size': object.name_value_list.size_c.value,
                        "tax": object.name_value_list.tax.value,
                        "hsn": object.name_value_list.hsn.value,
                        'weight': object.name_value_list.weight_c.value,
                        'extrainfo1': object.name_value_list.extrainfo1_c.value,
                        'extrainfo2': object.name_value_list.extrainfo2_c.value,
                        'extrainfo3': object.name_value_list.extrainfo3_c.value,
                        'extrainfo4': object.name_value_list.extrainfo4_c.value,
                        'extrainfo5': object.name_value_list.extrainfo5_c.value,
                        'imgsrc': object.name_value_list.product_image.value,
                        'manufactured_date': object.name_value_list.manufactured_date_c.value,
                        "stock": object.name_value_list.stock_c.value,
                        "id": object.name_value_list.id.value,
                        "noofdays": object.name_value_list.no_of_days.value,
                    }


                });
                console.log("product data", tempArray);
                setMasterData(tempArray)
                setFilteredData(tempArray);
                setLoading(false);


            })
            .catch(error => {
                setLoading(false);
                console.log('error', error)
            });
    }

    const searchProduct = (text) => {
        // Check if searched text is not blank
        if (text) {
            // Inserted text is not blank
            // Filter the masterDataSource
            // Update FilteredDataSource
            const newData = masterData.filter(
                function (item) {
                    const itemData = item.description
                        ? item.description.toUpperCase() + item.itemid.toUpperCase()
                        : ''.toUpperCase();
                    const textData = text.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                });
            setFilteredData(newData);
            setSearch(text);
        } else {
            // Inserted text is blank
            // Update FilteredDataSource with masterDataSource
            setFilteredData(masterData);
            setSearch(text);
        }
    };



    return (
        <View style={styles.container}>

            <Text style={{ alignSelf: 'center', fontSize: 20, color: Colors.primary, fontFamily: 'Poppins-SemiBold', marginVertical: 5 }}>Inventory</Text>
            {/* <View style={{

                height: 40

            }} /> */}

            <ActivityIndicator
                animating={loading}
                color={Colors.primary}
                size="large"
                style={styles.activityIndicator}

            />


            <View style={{ flexDirection: 'row' }}>
                <View style={styles.modalSearchContainer}>
                    <TextInput
                        style={styles.input}
                        value={search}
                        placeholder="Search product"
                        placeholderTextColor="gray"
                        onChangeText={(val) => searchProduct(val)}

                    />
                    <TouchableOpacity style={styles.searchButton} >
                        <AntDesign name="search1" size={20} color="black" />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={{ height: 45, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderRadius: 8, paddingHorizontal: 16, elevation: 5, flex: 0.2 }}
                    onPress={() => {
                        setSearch('');
                        setFilteredData(masterData)
                        Keyboard.dismiss();
                    }
                    }
                >
                    <Text style={{ color: 'blue', fontFamily: 'Poppins-Regular', fontSize: 14 }}>Clear</Text>

                </TouchableOpacity>
            </View>

            <FlatList
                showsVerticalScrollIndicator={false}
                data={filteredData}
                keyboardShouldPersistTaps='handled'
                renderItem={({ item }) =>

                    <Pressable style={styles.elementsView} onPress={() => {
                        navigation.navigate('ProductDetails', { item: item })
                    }
                    }
                    >
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Image

                                source={{ uri: item.imgsrc }}
                                style={{
                                    ...styles.imageView,
                                }}
                            />
                            <View style={{
                                flex: 1,
                                borderLeftWidth: 1.5,
                                paddingLeft: 10,
                                marginLeft: 10,
                                borderStyle: 'dotted',
                                borderColor: 'grey',
                            }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ color: 'grey', fontSize: 12, fontFamily: 'Poppins-Regular', borderBottomColor: 'grey', borderBottomWidth: 0.5, }}>Net wt: {item.weight}</Text>
                                    <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-SemiBold' }}>AED {Number(item.price)}</Text>

                                </View>
                                <Text style={{ color: Colors.primary, fontSize: 12, fontFamily: 'Poppins-SemiBold', marginTop: 5, }}>{item.description}</Text>
                                <Text style={{ color: 'green', fontSize: 12, fontFamily: 'Poppins-Regular' }}>{Number(item.stock) > 0 ? `Current Stock - ${item.stock}` : <Text style={{ color: 'red', fontSize: 12, fontFamily: 'Poppins-Regular' }} >Out of stock</Text>}</Text>
                                <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-Regular' }}>{item.noofdays} days older</Text>


                            </View>







                        </View>
                        <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Poppins-Regular', paddingLeft: 16 }}>{item.itemid}</Text>
                    </Pressable>


                }
                keyExtractor={(item) => item.id.toString()}
            />

        </View>



    )
}

export default Inventory

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        paddingBottom: 0,
        backgroundColor: 'white'
    },
    activityIndicator: {
        flex: 1,
        alignSelf: 'center',
        height: 100,
        position: 'absolute',
        top: '30%',

    },
    elementsView: {
        backgroundColor: "white",
        margin: 5,
        //borderColor: 'black',
        //flexDirection: 'row',
        //justifyContent: 'space-between',
        //alignItems: 'center',
        marginBottom: 16,
        borderRadius: 8,
        elevation: 5,
        padding: 16
        //borderColor: '#fff',
        //borderWidth: 0.5
    },
    imageView: {
        width: 80,
        height: 80,
        // borderRadius: 40,
        // marginTop: 20,
        // marginBottom: 10
    },
    elementText: {
        fontSize: 15,
        fontFamily: 'Poppins-SemiBold',
        color: 'black'
    },
    minusButton: {
        width: 45,
        height: 30,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 30,
        marginLeft: 10
    },
    modalMinusButton: {
        width: 35,
        height: 20,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 40,
        marginLeft: 10
    },
    quantityCount: {
        width: 45,
        height: 30,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 30,
        marginLeft: 1
    },
    modalQuantityCount: {
        width: 35,
        height: 20,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 40,
        marginLeft: 1
    },
    orderCloseView: {
        height: 15,
        width: 15,
        //marginTop: 30
    },
    imageText: {
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'black',
    },
    searchContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 5,

        //paddingVertical: 5,
        paddingHorizontal: 10,
        marginLeft: 10
    },
    input: {
        flex: 1,
        fontSize: 16,
        marginRight: 10,
    },
    searchButton: {
        padding: 5,
    },
    sendButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'green',
        paddingVertical: 10,
        paddingHorizontal: 20,

        height: 40,
        marginLeft: 10
    },
    saveButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: Colors.primary,
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10
    },
    deleteButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'red',
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10
    },
    addButtonView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10,
        alignSelf: 'center'
    },
    modalAddButtonView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        paddingVertical: 5,
        paddingHorizontal: 15,
        height: 35,
        //alignSelf: 'flex-end',
        //marginLeft: 30,
        //marginTop: 60
    },
    buttonText: {
        color: 'blue',
        fontFamily: 'Poppins-Regular'
    },
    sendButton: {
        color: 'white',
        fontFamily: 'Poppins-Regular'
    },
    deleteButton: {
        color: 'red'
    },
    saveButton: {
        color: 'purple'
    },
    textColor: {
        color: 'black',
        fontFamily: 'Poppins-Regular',

    },
    searchModal: {
        backgroundColor: 'white',
        padding: 20,
        width: '90%',
        marginHorizontal: 10,
        borderRadius: 10,
        elevation: 5,
        //borderColor: 'black',
        //borderWidth: 1,
        marginVertical: 100
        // flexDirection:'row'
    },
    modalSearchContainer: {
        flex: 0.8,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 5,
        paddingHorizontal: 10,
        marginBottom: 10,
        marginRight: 10
    },
    modalTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'Poppins-SemiBold'
    },
})