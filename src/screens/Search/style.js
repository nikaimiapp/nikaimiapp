import { StyleSheet } from 'react-native'
export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header: {
        paddingHorizontal: 16,
        paddingVertical: 10,
        elevation: 5,
        paddingHorizontal: 16,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0.5 },
        shadowOpacity: 0.2,
        shadowRadius: 1,
        flexDirection: 'row',
        alignItems: 'center',

    },
    input: {
        padding: 5,
        flex: 1,
        marginLeft: 5,
        fontSize: 16,
        color: '#000',
        fontFamily:'Poppins-Regular',

    },
    flatViewcontainer:{
        flexDirection:'row',
        //alignItems:'center',
        borderBottomColor:'#ccc',
        borderBottomWidth:1,
        paddingVertical:12,
        paddingHorizontal:16,
        flex:1
        
    
    },
    imgStyle:{
        height:50,
        width:50,
        borderRadius:25
    },
    name:{
        fontSize:16,
        color:'#000',
        fontFamily:'Poppins-Regular',
        textTransform:'capitalize',
       

    },
    msg:{
        fontSize:15,
        

    }

})
