import React, { useEffect, useRef, useState, useContext } from "react";
import { View, Text, StyleSheet, Modal, TouchableOpacity, Image, Alert, ScrollView, Dimensions, FlatList, PermissionsAndroid } from 'react-native';
import MapboxGL from '@rnmapbox/maps';
import { lineString as makeLineString } from '@turf/helpers';
import MapboxDirectionsFactory from '@mapbox/mapbox-sdk/services/directions';
import Geolocation from 'react-native-geolocation-service';
const accessToken = 'sk.eyJ1IjoibmlzaGFudGh1amlyZSIsImEiOiJjbGliY3dxN2MwOG9qM2N1azg2dTBsMHQ1In0.ROqFtNqa1Qecr4ZpmT0b2Q';
import { AuthContext } from '../../Context/AuthContext';
MapboxGL.setWellKnownTileServer('Mapbox');
MapboxGL.setAccessToken(accessToken);
import moment from "moment";
import { useFocusEffect } from "@react-navigation/native";
import Colors from "../../constants/Colors";
import { Callout } from '@rnmapbox/maps';
const directionsClient = MapboxDirectionsFactory({ accessToken });


const Map = ({ navigation, route }) => {



    const [routes, setRoute] = useState(null);
    const dealerArray = useRef([]);

    //const bestPathCords = useRef([]);


    //getting todays plan
    const getTodayPlan = async (latitude, longitude) => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "text/plain");

        var raw = JSON.stringify(
            {
                "__user_id__": token,
                "__latitude__": latitude,
                "__longitude__": longitude
            }
        )

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_today_visit.php", requestOptions)
            .then(response => response.json())
            .then(async result => {
                console.log("GET TODAY PLaN API RESPONSE", result)
                dealerArray.current = result?.accounts;
                //checking response contain array
                if (!Array.isArray(dealerArray.current)) {
                    Alert.alert('Sorry', 'No dealer to visit today', [
                        { text: 'Ok', onPress: () => navigation.navigate('BottomTab') },
                    ]);
                    return;
                }
                //if response contains array
                else {
                    console.log("intial dealer Array", dealerArray.current);
                    setCords(result?.accounts);
                    let tmpPathArray = [];
                    result?.path.forEach((item, index) => {
                        //not including lat element
                        //not drawing line last line to current location
                        if (index != result?.path.length - 1) {
                            tmpPathArray.push([Number(item?.longitude), Number(item?.latitude)]);
                        }

                    })
                    console.log("path array", tmpPathArray);
                    //bestPathCords.current = tmpPathArray;
                    //setting route
                    setRoute({
                        type: "FeatureCollection",
                        features: [
                            {
                                type: "Feature",
                                properties: {},
                                geometry: {
                                    type: "LineString",
                                    coordinates: tmpPathArray,
                                },
                            },
                        ],
                    })
                }


            })
            .catch(error => console.log('get today plaN  api error', error));
    }



    //source and destination place cords hooks
    const [userCordinates, setUserCordinates] = useState(null);
    const [isModalVisible, setModalVisible] = useState(false);
    const [modalTitle, setModalTitle] = useState('');
    const [nearByDealer, setNearByDealer] = useState('');

    const { token, changeDocId, changeDealerData, dealerData } = useContext(AuthContext);
    const activeDealer = useRef([])
    const [visitDateKey, setVisitDateKey] = useState('');




    //const { dealerArray } = route.params;
    const dealerCords = useRef([]);


    //console.log("dealerArray", dealerArray);

    //calling active pls dealer data for the particular sales man

    const setCords = async (dealer_array) => {
        console.log('inside')
        let tempCords = []
        dealer_array.forEach(item => {
            //not pushing user current location element
            if (item?.name !== 'Current') {
                tempCords.push({ title: item?.name, cords: [Number(item?.longitude), Number(item?.latitude)] })
            }
        })
        //dealerCords.current = tempCords;
        //checking salesman have dealer to visit today
        if (tempCords.length > 0) {
            dealerCords.current = tempCords
            console.log("dealer cords array", dealerCords.current);
        }
        //user dont have visit 
        else {
            Alert.alert('Sorry', 'No dealer to visit today', [
                { text: 'Ok', onPress: () => navigation.navigate('BottomTab') },
            ]);
        }

    }

    const getActiveDealer = async () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "text/plain");

        var raw = JSON.stringify(
            {
                "__user_id__": token
            }
        )

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_accounts_for_dealer.php", requestOptions)
            .then(response => response.json())
            .then(async result => {
                console.log("active daeler list in map", result)
                activeDealer.current = result;

            })
            .catch(error => console.log('get active dealer api error', error));
    }

    useFocusEffect(

        React.useCallback(() => {
            //getting location
            Geolocation.getCurrentPosition(
                (res) => {
                    getTodayPlan(res.coords.latitude.toString(), res.coords.longitude.toString())
                    //getting user location
                    setUserCordinates([res.coords.longitude, res.coords.latitude]);
                    console.log("lattitude", res.coords.latitude);
                    console.log("longitude", res.coords.longitude);
                },
                (error) => {
                    console.log("get location error", error);
                    console.log("please enable location ")
                    setLocationEnabled(false);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }

            );

            getActiveDealer();
            //setCords();
            checkPermission();



        }, [])
    );

    const checkPermission = async () => {
        const granted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

        if (granted) {
            console.log("location permission granted");
            getLocation();
        }
        else {
            console.log("asking permission");
            requestLocationPermission();
        }
    }

    //Location Permission
    const requestLocationPermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    title: "App Location Permission",
                    message:
                        "App needs access location",
                    buttonNeutral: "Ask Me Later",
                    buttonNegative: "Cancel",
                    buttonPositive: "OK"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use location ride");
                getLocation();
                //setEnableLocationModalOpen(true);
            } else {
                console.log("location permission denied");
            }
        } catch (err) {
            console.warn("location permission error", err);
        }
    };

    const getLocation = async () => {

        Geolocation.getCurrentPosition(
            (res) => {
                console.log("route called")
                console.log(res);
                //getting user location
                console.log("lattitude", res.coords.latitude);
                console.log("longitude", res.coords.longitude);
                setUserCordinates([res.coords.longitude, res.coords.latitude]);
                calculateDistances([res.coords.longitude, res.coords.latitude]);
            },
            (error) => {
                console.log("get location error", error);
                console.log("please enable location ")
                setLocationEnabled(false);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }

        );

    }

    const calculateDistances = async (currentPosition) => {
        const startingPoint = [...currentPosition];
        // const destinations = [
        //     [74.84101, 12.88641],
        //     [74.84644, 12.88455],
        //     [74.83484, 12.89132],
        //     // Add more destinations as needed
        // ];

        console.log("cal distance destination cords", dealerCords.current)
        const requests = dealerCords.current.map((item) => {
            let destination = item.cords
            console.log("destination cords", destination);
            const url = `https://api.mapbox.com/directions/v5/mapbox/driving/${startingPoint[0]},${startingPoint[1]};${destination[0]},${destination[1]}?access_token=${accessToken}`;
            return fetch(url)
                .then((response) => response.json())
                .then((data) => {
                    console.log("cords", destination)
                    const distance = data.routes[0].distance; // Extract the distance from the API response
                    return {
                        cords: destination,
                        distance: distance
                    };
                })
                .catch(error => console.log('distance error', error));
        });
        const distances = await Promise.all(requests);
        console.log("distance obj", distances);
        let minDistance = 15000000; //minimum distance 50m  //for testing value has been changed to 150 km
        //setModalVisible(true);

        distances.map((item) => {

            //modal visible if dealer is near to store (min distance 50m)
            if (item.distance < minDistance) {
                console.log("dealerArray ", dealerArray.current);
                console.log("store cords ", item.cords);

                dealerArray.current.forEach((element) => {
                    //getting dealer info based on near by dealer cords
                    if (element.longitude == item.cords[0] && element.latitude == item.cords[1]) {
                        // //mappping thorugh active plans dealter array to get near by dealer details
                        console.log("inside log");
                        console.log("store id", element.id);
                        console.log("activeDealer Array", activeDealer.current);
                        activeDealer.current.forEach(itm => {
                            if (itm.id == element.id) {
                                console.log("inside log 2")
                                changeDealerData(itm);
                                setNearByDealer(element);
                                setModalTitle(element?.name);
                                setVisitDateKey(element?.visit_date_key);
                                if (navigation.isFocused()) {
                                    //checking user in map screen
                                    setModalVisible(true);
                                }
                                console.log("dealer data", itm);
                            }

                        });
                    }

                })

            }
        })



    };




    useEffect(() => {

        const interval = setInterval(() => {
            if (!navigation.isFocused()) {
                //screen is not focused
                console.log("clearing get location")
                clearInterval(interval);
                return false;
            }
            //checkPermission called after every 10 seconds
            checkPermission();
        }, 5000);
        return () => clearInterval(interval)
    }, [])


    const loc1 = [74.84101, 12.88641] // bharat mall
    const loc2 = [74.84644, 12.88455]  //bejai ch hll
    const loc3 = [74.83484, 12.89132]  //lady hill

    const { tour_plan_id, planName } = route.params;

    const saveCheckIn = () => {
        console.log("active dealer", dealerData);
        console.log("visit date key", visitDateKey);
        console.log("tour plan id", tour_plan_id);
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({
            "__name__": planName,
            "__po_ordousers_id_c__": token,
            "__account_id_c__": nearByDealer?.id,
            "__check_in__": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
            "__tour_plan_id__": tour_plan_id,
            "__visit_date_key__": visitDateKey
            // "name": , //nearByDealer?.name, //tour plan name
            // "po_ordousers_id_c": token,
            // "account_id_c": nearByDealer?.id,
            // "dealer_id": nearByDealer?.name,
            // "check_in": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
            // "tour_plan_id": tour_plan_id,
            // "visit_date_key": visitDateKey
        });
        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };
        fetch("https://dev.ordo.primesophic.com/set_check_in.php", requestOptions)
            .then(response => response.json())
            .then(result => {
                setModalVisible(false);
                console.log('checkin save res', result);
                console.log('inserted id', result.id);
                changeDocId(result.id);
                navigation.navigate('CheckIn', { visitDateKey: visitDateKey, tour_plan_id: tour_plan_id })
            })
            .catch(error => console.log('api error', error));

    }








    return (
        <View style={{ flex: 1 }}>
            <Modal
                visible={isModalVisible}
                animationType="slide"
                transparent={true}

            >
                {/* Modal content */}
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                    <View style={{ backgroundColor: 'white', padding: 20, width: '90%', marginHorizontal: 10, borderRadius: 10 }}>
                        <Text style={styles.modalTitle}>{modalTitle}</Text>
                        <View style={styles.buttonview}>
                            <TouchableOpacity style={styles.buttonContainer} onPress={saveCheckIn}>
                                <Text style={styles.buttonText}>Check in</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonContainer} onPress={() => setModalVisible(false)}>
                                <Text style={styles.buttonText}>Cancel</Text>
                            </TouchableOpacity>

                        </View>

                    </View>
                </View>
            </Modal>

            {userCordinates && <MapboxGL.MapView
                styleURL={MapboxGL.StyleURL.Street}
                zoomLevel={12}

                style={{ width: '100%', height: '100%' }}>
                <MapboxGL.Camera
                    zoomLevel={12}
                    centerCoordinate={userCordinates}
                    animationMode={'flyTo'}
                    animationDuration={0}
                >
                </MapboxGL.Camera>
                {/* <MapboxGL.PointAnnotation id='1' coordinate={loc1} />
                {/* <MapboxGL.PointAnnotation id='2' coordinate={loc2} /> */}

                {userCordinates && <MapboxGL.PointAnnotation
                    id="pointAnnotation"
                    key={'UserLocation'}

                    coordinate={userCordinates}>
                    <View style={{
                        height: 30,
                        width: 30,
                        backgroundColor: '#3f6de7',
                        borderRadius: 50,
                        borderColor: '#fff',
                        borderWidth: 3
                    }}
                    />
                </MapboxGL.PointAnnotation>}

                {dealerCords.current.map((item, index) => {
                    return (
                        <MapboxGL.PointAnnotation id={index.toString()} coordinate={item?.cords} >
                            <Callout
                                title={item?.title}
                                textStyle={{ fontFamily: 'Poppins-Regular', fontSize: 12 }}
                                selected={true}

                            >
                            </Callout>
                        </MapboxGL.PointAnnotation>)
                })}










                {/* <MapboxGL.PointAnnotation id={'1'} coordinate={[74.8428, 12.8713]} /> */}


                {routes && <MapboxGL.ShapeSource id="line1" shape={routes}>
                    <MapboxGL.LineLayer
                        id="linelayer1"
                        style={{ lineColor: "red", lineWidth: 3 }}
                    />
                </MapboxGL.ShapeSource>}






                {/* <MapboxGL.PointAnnotation coordinate={userCordinates} /> */}
                {/* <MapboxGL.Callout title="Bejai" /> */}



            </MapboxGL.MapView>}
        </View >


    )
}

export default Map

const styles = StyleSheet.create({
    buttonview: {
        flexDirection: 'row'
    },
    buttonContainer: {
        heigh: 40,
        padding: 10,
        borderRadius: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        marginVertical: 10,
        backgroundColor: Colors.primary
    },
    buttonText: {
        fontFamily: 'Poppins-Regular',
        color: 'white'
    },
    modalTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'Poppins-SemiBold'

    },
    markerContainer: {
        alignItems: "center",
        width: 60,
        backgroundColor: "transparent",
        height: 70,
    },
    textContainer: {
        backgroundColor: "white",
        borderRadius: 10,
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
    },
    text: {
        textAlign: "center",
        paddingHorizontal: 5,
        flex: 1,
    },

})

