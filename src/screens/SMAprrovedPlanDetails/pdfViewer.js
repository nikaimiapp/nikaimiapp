import React from 'react';
import { View, Text, TouchableOpacity, Image, Alert } from 'react-native';
import Pdf from 'react-native-pdf';
import RNFS, { DownloadDirectoryPath } from 'react-native-fs';
import Colors from '../../constants/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
const PDFViewer = ({ navigation, route }) => {
    const { title } = route.params;
    const pdfUrl = 'https://www.africau.edu/images/default/sample.pdf'; // Replace with your PDF URL


    const savePDFToLocal = async () => {
        try {
            const downloadDest = `${DownloadDirectoryPath}/sample.pdf`;

            const options = {
                fromUrl: pdfUrl,
                toFile: downloadDest,
            };

            const downloadResult = await RNFS.downloadFile(options).promise;

            if (downloadResult.statusCode === 200) {
                console.log('PDF saved to Download folder:', downloadDest);
                Alert.alert('Download', 'PDF file downloaded sucessfully')
                // Show a success message to the user if needed
            } else {
                console.log('Failed to save PDF:', downloadResult);
                // Handle the error or show an error message to the user
            }
        } catch (error) {
            console.error('Error saving PDF:', error);
            // Handle the error or show an error message to the user
        }
    };

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingVertical: 5, paddingHorizontal: 10, alignItems: 'center', backgroundColor: '#fff' }}>

                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                    >
                        <AntDesign name='arrowleft' size={25} color={Colors.black} />
                    </TouchableOpacity>
                    <Text style={{
                        fontSize: 18,
                        fontFamily: 'Poppins-SemiBold',
                        color: Colors.black,
                        marginLeft: 10,
                        marginTop: 3
                    }}>{title} Report</Text>
                </View>
                <TouchableOpacity style={{ paddingBottom: 10, marginRight: 10 }} onPress={savePDFToLocal}>
                    <Image source={require('../../assets/images/download.png')} style={{ height: 30, width: 30, tintColor: Colors.primary, marginRight: 10 }} />
                </TouchableOpacity>
            </View>
            <Pdf
                trustAllCerts={false}
                source={{ uri: pdfUrl, cache: true, }}
                style={{ flex: 1 }}
            />

        </View>
    );
};

export default PDFViewer;
