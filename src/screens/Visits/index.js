
import React, { useState, useEffect, useContext, useRef } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Alert, Image, FlatList, LogBox, ScrollView, Button, Settings } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useFocusEffect } from '@react-navigation/native';
import Geolocation from 'react-native-geolocation-service';
import firestore from '@react-native-firebase/firestore';
import { Pressable } from 'react-native';
import { AuthContext } from '../../Context/AuthContext';
import Colors from '../../constants/Colors';
import FaceSDK, { Enum, FaceCaptureResponse, MatchFacesResponse, MatchFacesRequest, MatchFacesImage, MatchFacesSimilarityThresholdSplit, RNFaceApi } from '@regulaforensics/react-native-face-api'
import { locationPermission, } from '../../utils/Helper'
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import ActionButton from 'react-native-action-button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Entypo from 'react-native-vector-icons/Entypo';



import ProgressCircle from 'react-native-progress-circle'

import Feather from 'react-native-vector-icons/Feather';
import { Dimensions } from "react-native";
import ClockIn from './clockIn';

import {
  LineChart,

  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from "react-native-chart-kit";
const screenWidth = Dimensions.get("window").width;
import { BarChart } from "react-native-gifted-charts";
LogBox.ignoreAllLogs();
const Visits = ({ navigation }) => {


  //plans hooks
  //const [aprrovedPlans, setApprovedPlans] = useState('');
  const [pendingPlans, setPendingPlans] = useState('');
  const [otherPlans, setOtherPlans] = useState('');
  const [completedPlans, setCompletedPlans] = useState('');

  const [clockedIn, setClockedIn] = useState(null);
  const profile = useRef('');
  const [userCordinates, setUserCordinates] = useState([]);


  const { token, logout, aprrovedPlans, setTourPlanName, changeApprovedPlans, changeTourPlanId, salesManager, userData } = useContext(AuthContext);

  const getLocation = async () => {
    let locPermissionDenied = await locationPermission();
    if (locPermissionDenied) {
      Geolocation.getCurrentPosition(
        async (res) => {
          console.log('GET LOCATION CALLED');
          console.log(res);
          //getting user location
          console.log("lattitude", res.coords.latitude);
          console.log("longitude", res.coords.longitude);
          setUserCordinates([res.coords.latitude, res.coords.longitude])

        },
        (error) => {
          console.log("get location error", error);
          console.log("please enable location ")
        },
        {
          enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, accuracy: {
            android: 'high',
            ios: 'bestForNavigation',
          }
        }

      );

    }
    else {
      console.log("location permssion denied")
    }
  }




  const getPlans = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    if (salesManager) {
      var raw = JSON.stringify({
        "__user_id__": token,
        "__own__": 'Yes' //extra parameter
      });
    }
    else {
      var raw = JSON.stringify({
        "__user_id__": token,
      });
    }

    console.log(JSON.stringify({
      "__user_id__": token
    }))

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://dev.ordo.primesophic.com/get_tour_plan.php", requestOptions)
      .then(response => response.json())
      .then(result => {
        console.log('get plans data \n', result?.tour_plan)
        let approved = [];  //approved plans
        let completed = []
        let other = [];  //reject,pending plans  all are other plans

        result?.tour_plan.forEach((item) => {
          console.log("item", item)
          //approved
          if (item?.status == 'Approved') {
            changeTourPlanId(item?.id)
            setTourPlanName(item?.name)
            approved.push(item);
          }
          //completed
          else if (item?.status == 'Completed') {
            completed.push(item)
          }
          //other
          else {
            other.push(item)
          }

        })
        //sorting approved plans array
        let sortedarray = approved.sort((a, b) => (a.start_date < b.start_date) ? 1 : -1)
        console.log("approvedsorted array", sortedarray)
        changeApprovedPlans(sortedarray)


        //sorting completed  plans array
        let sortedarray2 = completed.sort((a, b) => (a.start_date < b.start_date) ? 1 : -1)
        setCompletedPlans(sortedarray2)

        //sorting other  plans array
        let sortedarray3 = other.sort((a, b) => (a.start_date < b.start_date) ? 1 : -1)
        setOtherPlans(sortedarray3)
      })
      .catch(error => console.log('get tour plan error', error));
    // var myHeaders = new Headers();
    // myHeaders.append("Content-Type", "application/json");

    // var raw = JSON.stringify({
    //   "__user_id__": "1878e60a-fe1c-0d00-e8b6-63119c1882aa",
    //   "__id__": "87cc5a45-c1fe-858c-1bca-647441f70493"
    // });

    // var requestOptions = {
    //   method: 'GET',
    //   headers: myHeaders,
    //   body: raw,
    //   redirect: 'follow'
    // };

    // fetch("https://dev.ordo.primesophic.com/get_tour_plan_detail.php", requestOptions)
    //   .then(response => response.json())
    //   .then(result => console.log(result))
    //   .catch(error => console.log('error', error));


  }

  const clockIncheck = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "__user_id__": token,
      "__type__": "clockin"
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://dev.ordo.primesophic.com/get_check_user_clockin.php", requestOptions)
      .then(response => response.json())
      .then(result => {
        console.log('clockin check data', result);
        if (result.status == 200) {
          setClockedIn(true)
        }
        else if (result.status == 201) {
          setClockedIn(false);
          getProfile();
        }
      })
      .catch(error => console.log('error', error));
  }
  const getProfile = () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");



    var raw = JSON.stringify({
      "__user_id__": token,
      "__own__": salesManager ? 'Yes' : 'No'
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://dev.ordo.primesophic.com/get_ordouser_image.php", requestOptions)
      .then(response => response.json())
      .then(result => {
        //console.log('profile', result);
        let data = result.image_base64;
        let base64 = data.split(",");
        //console.log('profile url', base64[1]);
        profile.current = base64[1];
        //faceRecognise();

      })
      .catch(error => console.log('error', error));
  }

  const logoutAlert = () => {
    Alert.alert('Confirmation', 'Are you sure, You want to logout?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      { text: 'OK', onPress: () => { logout() } },
    ]);
  }









  useFocusEffect(
    React.useCallback(() => {
      getTourPlanGrahpData();
      getPlans();
      clockIncheck();
      getLocation();
      FaceSDK.init(json => {
        response = JSON.parse(json)
        console.log(response);
        if (!response["success"]) {
          console.log("Init failed: ");
          console.log(json);
        }
      }, e => { })

    }, [])
  );

  const saveClockIn = () => {
    console.log("location", userCordinates);
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var raw = JSON.stringify({
      "__user_id__": token,
      "__type__": "clockin",
      "__location__": "abc",
      "__latitude__": userCordinates[0],
      "__longitude__": userCordinates[1],
    });

    console.log(JSON.stringify({
      "__user_id__": token,
      "__type__": "clockin",
      "__location__": "abc",
      "__latitude__": userCordinates[0],
      "__longitude__": userCordinates[1],
    }))



    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };

    fetch("https://dev.ordo.primesophic.com/set_user_attendance.php", requestOptions)
      .then(response => response.json())
      .then(async result => {
        console.log('clock in save data', result);
        if (result?.status == "200") {
          Alert.alert('Clock in ', `Clocked in successfully`);
          setClockedIn(true);
        }
      })
      .catch(error => console.log('set attendance error', error));
  }


  //user face recognisation
  const faceRecognise = async () => {
    console.log("inside");
    FaceSDK.presentFaceCaptureActivity(result => {
      let res = JSON.parse(result)
      //checking user cancle image picker
      if (res.exception) {
        console.log("User Canceled Face capture option");
        return;
      }
      //console.log("image uri", FaceCaptureResponse.fromJson(JSON.parse(result)).image.bitmap);
      let base64Img = FaceCaptureResponse.fromJson(JSON.parse(result)).image.bitmap;
      const firstImage = new MatchFacesImage();
      firstImage.imageType = Enum.ImageType.PRINTED; //captured image
      firstImage.bitmap = profile.current
      const secondImage = new MatchFacesImage();
      secondImage.imageType = Enum.ImageType.LIVE; //live image
      secondImage.bitmap = base64Img;
      request = new MatchFacesRequest()
      request.images = [firstImage, secondImage]
      console.log("start compare", profile);
      //comparing two images
      FaceSDK.matchFaces(JSON.stringify(request), response => {
        response = MatchFacesResponse.fromJson(JSON.parse(response))
        console.log("ggg", response);
        FaceSDK.matchFacesSimilarityThresholdSplit(JSON.stringify(response.results), 0.75, str => {
          var split = MatchFacesSimilarityThresholdSplit.fromJson(JSON.parse(str))
          console.log("res", split.length);
          if (split?.matchedFaces.length > 0) {
            //face matched
            let faceMatchPercentage = split.matchedFaces[0].similarity * 100
            console.log("match percentage", faceMatchPercentage.toFixed(2));
            saveClockIn();
          }
          else {
            //face doe not match
            alert('Face not recognised please try again.')
          }
        }, e => { console.log("error") })
      }, e => { console.log("error") })


    }, e => { console.log("error", e) })



  }






  const handlePlan = (item) => {
    navigation.navigate('PlanDetails', { item: item })
  }

  const [weeklyData, setWeeklyData] = useState([]);
  const [monthData, setMonthData] = useState([]);
  //graph options
  const [wSelected, setWSelected] = useState(true);
  const [mSelected, setMSelected] = useState(false);



  // Function to convert monthly data to the desired format
  const convertMonthlyData = (data) => {
    return Object.keys(data).map((month) => {
      return {
        value: parseInt(data[month]),
        label: month,
      };
    });
  };

  // Function to convert weekly data to the desired format
  const convertWeeklyData = (data) => {
    const weeks = Object.keys(data);
    return weeks.map((week) => {
      return {
        value: parseInt(data[week]),
        label: week,
      };
    });
  };



  //getting tour pland grahh data
  const getTourPlanGrahpData = async () => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      "__user_id__": token,
    });

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    };


    fetch("https://dev.ordo.primesophic.com/get_tp_completed_visits_quarterly_weekly.php", requestOptions)
      .then(response => response.json())
      .then(async res => {
        console.log("monthly", res?.monthly_completed_visits);
        console.log("weekly", res?.monthly_completed_visits_with_weekly);

        // Extracting and converting monthly data
        const monthlyData = await res.monthly_completed_visits.map((item) =>
          convertMonthlyData(item)
        )[0];

        // Extracting and converting weekly data
        const weeklyData = await res.monthly_completed_visits_with_weekly.map(
          (item) => convertWeeklyData(item[Object.keys(item)[0]])
        )[0];

        console.log("Monthly Data:", monthlyData);
        console.log("Weekly Data:", weeklyData);
        setMonthData(monthlyData)
        setWeeklyData(weeklyData);
      })
      .catch(error => console.log('error', error));



  }


  //line chart data
  // const data = {
  //   labels: labels,
  //   datasets: [
  //     {
  //       data: graphVal,
  //       color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
  //       strokeWidth: 2, // optional

  //     }
  //   ],
  //   //legend: ["visits"] // optional
  // };
  //
  //
  //-------------------------------------------
  //progress ring data logic
  // const [progressData, setProgressData] = useState('');
  // const getProgressData = () => {
  //   var myHeaders = new Headers();
  //   myHeaders.append("Content-Type", "application/json");

  //   var raw = JSON.stringify({
  //     "__user_id__": token,
  //     "__id__": aprrovedPlans[0]?.id
  //   });

  //   var requestOptions = {
  //     method: 'POST',
  //     headers: myHeaders,
  //     body: raw,
  //     redirect: 'follow'
  //   };

  //   fetch("https://dev.ordo.primesophic.com/get_tour_plan_detail.php", requestOptions)
  //     .then(response => response.json())
  //     .then(async result => {
  //       console.log('plan details', result?.status[0]?.dealer_array);
  //       if (result?.status[0]?.dealer_array.length > 0) {
  //         let label = [];
  //         let value = [];
  //         let totalVisits = 4;
  //         result?.status[0]?.dealer_array.forEach((item) => {
  //           label.push(item?.name);
  //           value.push((Number(item?.no_of_visit) / totalVisits) * 100)
  //         })


  //       }
  //     })
  //     .catch(error => console.log('error', error));
  // }

  // const data = {
  //   labels: ["Walmart", "E G india"], // optional
  //   data: [0.25, 0.75]
  // };










  return (
    <View style={styles.container}>
      {clockedIn == false && !salesManager ? (
        <ClockIn
          faceRecognise={faceRecognise}
          logoutAlert={logoutAlert}

        />
      ) : (
        <View style={{ flex: 1 }} >
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 10, marginTop: 1, alignItems: 'center', marginTop: 10 }}>

            <Image source={require('../../assets/images/ordotext.png')} style={{ height: 50, width: 100, }} />

            <View style={{ flexDirection: 'row' }}>
              <View style={{ paddingBottom: 10 }}>
                {/* <Image source={require('../../assets/images/reload.png')} style={{ height: 25, width: 25, tintColor: Colors.primary }} /> */}
                {salesManager && <TouchableOpacity onPress={() => navigation.navigate('SalesManagerHome')} >
                  <Image source={require('../../assets/images/approved.png')} style={{ height: 20, width: 20, tintColor: Colors.primary, marginRight: 16 }} />
                </TouchableOpacity>}

              </View>

              {/* <Image source={require('../../assets/images/reload.png')} style={{ height: 25, width: 25, tintColor: Colors.primary }} /> */}
              <TouchableOpacity onPress={logoutAlert} style={{ paddingBottom: 10, marginRight: 10 }}>
                <Image source={require('../../assets/images/power-off.png')} style={{ height: 20, width: 20, tintColor: Colors.primary, marginRight: 10 }} />
              </TouchableOpacity>


            </View>
          </View>
          <View style={{ paddingHorizontal: 16 }}>
            <Text style={{
              fontSize: 18,
              fontFamily: 'Poppins-SemiBold',
            }}>Welcome {userData?.name}</Text>
          </View>
          {/* <View style={styles.headerView}>
            <TouchableOpacity style={styles.createPlanBtn}
              onPress={() => { navigation.navigate('CreatePlan') }}
            >
              <Text style={styles.buttonTextStyle}>Create Plan</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={logoutAlert}>
              <Feather name='power' style={styles.logout} size={25} color={Colors.primary} />
            </TouchableOpacity>
          </View> */}
          <View style={{
            backgroundColor: '#fff',
            marginVertical: 8,
            borderRadius: 8,
            marginHorizontal: 16,
            padding: 10,
            elevation: 5,
            //backgroundColor:'red'
          }}>
            <View style={{ alignSelf: 'flex-end', flexDirection: 'row', alignItems: 'center', padding: 4, backgroundColor: Colors.grey, borderRadius: 5, elevation: 3 }}>
              <TouchableOpacity style={{
                backgroundColor: wSelected ? 'white' : Colors.grey,
                elevation: wSelected ? 5 : 0,
                paddingHorizontal: 16,
                borderRadius: 2,
                alignItems: 'center',
                justifyContent: 'center'
              }}
                onPress={() => {
                  setWSelected(true);
                  setMSelected(false);
                }}
              >
                <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Poppins-SemiBold' }}>W</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{
                backgroundColor: mSelected ? 'white' : Colors.grey,
                elevation: mSelected ? 5 : 0,
                paddingHorizontal: 16,
                borderRadius: 2,
                alignItems: 'center',
                justifyContent: 'center'
              }}
                onPress={() => {
                  setMSelected(true);
                  setWSelected(false);
                }}
              >
                <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Poppins-SemiBold' }}>M</Text>
              </TouchableOpacity>



            </View>

            {weeklyData.length > 0 && monthData.length > 0 && <BarChart
              height={150}
              frontColor={Colors.primary}
              noOfSections={4}
              showFractionalValue
              showYAxisIndices
              data={mSelected ? monthData : weeklyData}

              xAxisLabelTextStyle={{ fontSize: 7, textAlign: 'center', fontFamily: 'Poppins-Regular' }}
              yAxisTextStyle={{ fontSize: 8, fontFamily: 'Poppins-Regular' }}
              isAnimated
              barWidth={25}
              disablePress
              spacing={25}






            />}


            {/* <ProgressChart
              data={{
                labels: ["Walmart", "Eg India", "Fantasy"],
                data: [0.4, 0.6, 0.8]

              }}
              width={350}
              height={220}
              strokeWidth={16}
              radius={32}
              chartConfig={{
                backgroundGradientFrom: '#ffffff',
                backgroundGradientTo: '#ffffff',
                decimalPlaces: 0,
                color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
              }}
              hideLegend={false}
            /> */}

            {/* {graphVal.length > 0 && <LineChart
              data={data}
              width={350} // Adjust the width of the chart
              height={220} // Adjust the height of the chart
              chartConfig={{
                backgroundGradientFrom: '#ffffff',
                backgroundGradientTo: '#ffffff',
                decimalPlaces: 0,
                color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,

              }}
              bezier
              style={{
                // marginVertical: 8,
                //borderRadius: 8,
                //backgroundColor: 'red',
                //marginHorizontal: 16,
                //marginTop: 20,
                //elevation:5,
                marginLeft: 6





              }}
              getDotColor={() => Colors.primary}

              withInnerLines={false}
            //withOuterLines={false}
            //withShadow={false}



            />} */}
          </View>
          <ScrollView style={{ flex: 1 }}>
            <View style={{ ...styles.planView, flexDirection: 'row', justifyContent: 'space-between', marginRight: 20 }}>

              {
                aprrovedPlans.length > 0 && <View style={{ justifyContent: 'center' }}>
                  <Text style={styles.planText}>Active Tour Plans</Text>
                </View>}
              <View>
                <TouchableOpacity style={styles.createPlanBtn}
                  onPress={() => {
                    { salesManager ? navigation.navigate('SMCreatePlan') : navigation.navigate('CreatePlan') }
                  }}
                >
                  <Text style={styles.buttonTextStyle}>Create Tour Plan</Text>
                </TouchableOpacity>
              </View>
            </View>
            {
              aprrovedPlans.length > 0 && aprrovedPlans.map(item => {
                let color = item.status == 'Approved' ? 'green' : item.status == 'PendingApproval' ? 'orange' : 'red';
                console.log("total", Number(item?.total_visits))
                console.log("completed", Number(item?.completed_visits))
                let percentage = 0;
                if (Number(item?.completed_visits) > 0 && Number(item?.total_visits) > 0) {
                  percentage = (Number(item?.completed_visits) / Number(item?.total_visits)) * 100;
                  console.log("percentage  is", percentage)
                }

                return (

                  <View
                    style={styles.itemContainer}

                    key={item.id}
                  //activeOpacity={0.5}
                  >
                    <View style={styles.orderDataContainer}>
                      {/* <View style={styles.rowContainer}>
                        <View style={{ flexDirection: 'row', }}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.heading}>Plan Name :</Text>
                            <Text style={{ ...styles.heading, color: Colors.primary, fontFamily: 'Poppins-SemiBold' }}>{item.name} </Text>
                          </View>
                          <View style={{ flex: 1.5, marginLeft: 5 }}>
                            <Text style={styles.heading}>Plan Duration :</Text>
                            <Text style={{ ...styles.heading, color: 'grey' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')} </Text>
                          </View>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                          <View style={{ flex: 1 }}>
                            <Text style={{ ...styles.heading, fontFamily: 'Poppins-Regular' }}>Status :</Text>
                            <Text style={{ ...styles.heading, color: color, fontFamily: 'Poppins-SemiBold' }}>{item.status} </Text>
                          </View>
                          <View style={{ flex: 1.5, marginLeft: 5 }}>
                            <Text style={{ ...styles.heading, color: 'black' }}>No. of Dealers :</Text>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                              <Text style={{ ...styles.heading, color: 'black' }}>#{item?.dealer_count}</Text>
                              <ProgressCircle
                                percent={60}
                                radius={14}
                                borderWidth={4}
                                color={'green'}
                                shadowColor="#999"
                                bgColor="#fff"

                              >
                                <Text style={{ fontSize: 6, fontFamily: 'Poppins-Regular', color: '#000' }}>90%</Text>
                              </ProgressCircle>

                            </View>
                          </View>
                        </View>

                      </View> */}
                      <View>
                        {/* <Text style={styles.planHeading}>Active Tour Plans</Text> */}
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center'
                        }}>


                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/document2.png')} />
                          <Text style={styles.title}>{item?.name}</Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/tick.png')} />
                          <Text style={{ ...styles.text, color: 'green', fontFamily: 'Poppins-SemiBold' }}>{item?.status}</Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/duration.png')} />
                          <Text style={{ ...styles.text, fontWeight: '500' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}</Text>
                        </View>





                        <View style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center'
                        }}>
                          <TouchableOpacity style={{
                            height: 35,
                            width: 120,
                            backgroundColor: Colors.primary,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 5,
                            padding: 5,
                            marginVertical: 10
                          }}
                            onPress={() => handlePlan(item)}
                          >
                            <Text style={{ fontSize: 14, color: Colors.white, fontFamily: 'Poppins-Regular', color: '#fff' }}>Details</Text>
                          </TouchableOpacity>

                          <ProgressCircle
                            percent={percentage}
                            radius={20}
                            borderWidth={4}
                            color={'green'}
                            shadowColor="#999"
                            bgColor="#fff"

                          >
                            <Text style={{ fontSize: 10, fontFamily: 'Poppins-Regular', color: '#000' }}>{percentage.toFixed(0)}%</Text>
                          </ProgressCircle>
                        </View>

                      </View>



                    </View>

                  </View>
                )
              })
            }
            {/* {
            pendingPlans.length > 0 && <View style={styles.planView}>
              <Text style={styles.planText}>Pending Plans</Text>
            </View>}
          {
            pendingPlans.length > 0 && pendingPlans.map(item => {
              return (
                <TouchableOpacity onPress={() => handlePlan(item)} key={item.id} activeOpacity={0.5}>
                  <View style={styles.itemContainer}>
                    <View style={styles.orderDataContainer}>
                      <View style={styles.rowContainer}>
                        <Text style={styles.heading}>Plan Name : {item.name} </Text>
                        {/* <Text style={styles.value}>Approved by : {item.approved_by}</Text> 
                        <Text style={styles.heading}>No of Visit : {item?.dealer_count} </Text>
                        <Text style={styles.heading}>Start Date : {item.start_date} </Text>
                        <Text style={styles.heading}>End Date : {item.end_date} </Text>
                        <Text style={styles.heading}>Status : {item.status} </Text>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )
            })
          } 
          */}

            {
              completedPlans.length > 0 && <View style={styles.planView} activeOpacity={0.5}>
                <Text style={styles.planText}>Completed Tour Plans</Text>
              </View>}
            {
              completedPlans.length > 0 && completedPlans.map(item => {
                return (
                  <View
                    style={styles.itemContainer}
                    // onPress={() => handlePlan(item)}
                    key={item.id}
                  // activeOpacity={0.5}
                  >
                    <View style={styles.orderDataContainer}>
                      {/* <View style={styles.rowContainer}>
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.heading}>Plan Name : </Text>
                            <Text style={{ ...styles.heading, color: Colors.primary, fontFamily: 'Poppins-SemiBold' }}>{item.name} </Text>
                          </View>
                          <View style={{ flex: 1.5, }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                              <Text style={styles.heading}>Plan Duration : </Text>
                              {item.status == 'PendingApproval' &&
                                <TouchableOpacity
                                  onPress={() => navigation.navigate('EditPlan', { id: item.id })}
                                >
                                  <Feather name="edit" size={15} color='black' />
                                </TouchableOpacity>}


                            </View>
                            <Text style={{ ...styles.heading, color: 'grey' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}  </Text>
                          </View>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                          <View style={{ flex: 1 }}>
                            <Text style={{ ...styles.heading, fontFamily: 'Poppins-Regular' }}>Status :</Text>
                            <Text style={{ ...styles.heading, color: color, fontFamily: 'Poppins-SemiBold' }}>{item.status == 'PendingApproval' ? 'In Progress' : item.status} </Text>
                          </View>
                          <View style={{ flex: 1.5, marginLeft: 5 }}>
                            <Text style={styles.heading}>No. of Dealers :</Text>
                            <Text style={styles.heading}>#{item?.dealer_count}</Text>
                          </View>
                        </View>
                        {/* <Text style={styles.value}>Approved by : {item.approved_by}</Text> */}
                      {/* <Text style={styles.heading}>No. of Visit : {item?.dealer_count} </Text>
                      <Text style={styles.heading}>Status : {item.status} </Text> 
                      </View>  */}
                      <View>
                        {/* <Text style={styles.planHeading}>Active Tour Plans</Text> */}


                        {/* <View style={{
                          flexDirection: 'row',
                          alignItems: 'center'
                        }}>

                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/document2.png')} />
                          <Text style={styles.title}>{item?.name}</Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/duration.png')} />
                          <Text style={{ ...styles.text, fontWeight: '500' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}</Text>
                        </View>

                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/tick.png')} />
                          <Text style={{ ...styles.text, color: 'green', fontFamily: 'Poppins-SemiBold' }}>{item.status == 'PendingApproval' ? 'Pending Approval' : item.status} </Text>
                        </View> */}

                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center'
                        }}>


                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/document2.png')} />
                          <Text style={styles.title}>{item?.name}</Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/tick.png')} />
                          <Text style={{ ...styles.text, color: 'green', fontFamily: 'Poppins-SemiBold' }}>{item?.status}</Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/duration.png')} />
                          <Text style={{ ...styles.text, fontWeight: '500' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}</Text>
                        </View>



                        <View style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center'
                        }}>
                          <TouchableOpacity style={{
                            height: 35,
                            width: 120,
                            backgroundColor: Colors.primary,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 5,
                            padding: 5,
                            marginVertical: 10
                          }}
                            onPress={() => handlePlan(item)}
                          >
                            <Text style={{ fontSize: 14, color: Colors.white, fontFamily: 'Poppins-Regular', color: '#fff' }}>Details</Text>
                          </TouchableOpacity>
                          <Image style={{ height: 30, width: 30, resizeMode: 'contain' }} source={require('../../assets/images/thumbup.png')} />

                        </View>

                      </View>
                    </View>

                  </View>
                )
              })
            }

            {
              otherPlans.length > 0 && <View style={styles.planView} activeOpacity={0.5}>
                <Text style={styles.planText}>Other Plans</Text>
              </View>}
            {
              otherPlans.length > 0 && otherPlans.map(item => {
                let color = item.status == 'Approved' ? 'green' : item.status == 'PendingApproval' ? 'orange' : 'red';
                return (
                  <View
                    style={styles.itemContainer}
                    // onPress={() => handlePlan(item)}
                    key={item.id}
                  // activeOpacity={0.5}
                  >
                    <View style={styles.orderDataContainer}>
                      {/* <View style={styles.rowContainer}>
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{ flex: 1 }}>
                            <Text style={styles.heading}>Plan Name : </Text>
                            <Text style={{ ...styles.heading, color: Colors.primary, fontFamily: 'Poppins-SemiBold' }}>{item.name} </Text>
                          </View>
                          <View style={{ flex: 1.5, }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                              <Text style={styles.heading}>Plan Duration : </Text>
                              {item.status == 'PendingApproval' &&
                                <TouchableOpacity
                                  onPress={() => navigation.navigate('EditPlan', { id: item.id })}
                                >
                                  <Feather name="edit" size={15} color='black' />
                                </TouchableOpacity>}


                            </View>
                            <Text style={{ ...styles.heading, color: 'grey' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}  </Text>
                          </View>
                        </View>

                        <View style={{ flexDirection: 'row', marginTop: 5 }}>
                          <View style={{ flex: 1 }}>
                            <Text style={{ ...styles.heading, fontFamily: 'Poppins-Regular' }}>Status :</Text>
                            <Text style={{ ...styles.heading, color: color, fontFamily: 'Poppins-SemiBold' }}>{item.status == 'PendingApproval' ? 'In Progress' : item.status} </Text>
                          </View>
                          <View style={{ flex: 1.5, marginLeft: 5 }}>
                            <Text style={styles.heading}>No. of Dealers :</Text>
                            <Text style={styles.heading}>#{item?.dealer_count}</Text>
                          </View>
                        </View>
                        {/* <Text style={styles.value}>Approved by : {item.approved_by}</Text> */}
                      {/* <Text style={styles.heading}>No. of Visit : {item?.dealer_count} </Text>
                      <Text style={styles.heading}>Status : {item.status} </Text> 
                      </View>  */}
                      <View>
                        {/* <Text style={styles.planHeading}>Active Tour Plans</Text> */}

                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          justifyContent: 'space-between'
                        }}>
                          <View style={{
                            flexDirection: 'row',
                            alignItems: 'center'
                          }}>
                            <Image
                              style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                              source={require('../../assets/images/document2.png')} />
                            <Text style={styles.title}>{item?.name}</Text>
                          </View>
                          {item.status == 'PendingApproval' &&
                            <TouchableOpacity
                              onPress={() => navigation.navigate('EditPlan', { id: item.id })}
                            >
                              <Feather name="edit" size={15} color='black' />
                            </TouchableOpacity>}

                        </View>


                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/tick.png')} />
                          <Text style={{ ...styles.text, color: color, fontFamily: 'Poppins-SemiBold' }}>{item.status == 'PendingApproval' ? 'Pending Approval' : item.status} </Text>
                        </View>
                        <View style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          marginTop: 3,
                        }}>
                          <Image
                            style={{ marginRight: 10, height: 15, width: 15, resizeMode: 'contain' }}
                            source={require('../../assets/images/duration.png')} />
                          <Text style={{ ...styles.text, fontWeight: '500' }}>{moment(item.start_date).format('DD-MM-YYYY')} To {moment(item.end_date).format('DD-MM-YYYY')}</Text>
                        </View>

                        <View style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center'
                        }}>
                          <TouchableOpacity style={{
                            height: 35,
                            width: 120,
                            backgroundColor: Colors.primary,
                            justifyContent: 'center',
                            alignItems: 'center',
                            borderRadius: 5,
                            padding: 5,
                            marginVertical: 10
                          }}
                            onPress={() => handlePlan(item)}
                          >
                            <Text style={{ fontSize: 14, color: Colors.white, fontFamily: 'Poppins-Regular', color: '#fff' }}>Details</Text>
                          </TouchableOpacity>
                        </View>

                      </View>
                    </View>

                  </View>
                )
              })
            }
          </ScrollView>
        </View>)
      }

      <ActionButton
        buttonColor={Colors.primary}
        renderIcon={() => <Ionicons name='chatbox' color='#fff' size={20} />}
        onPress={() => { navigation.navigate('UserList') }}
      />




    </View >
  )
}

export default Visits

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  itemContainer: {
    marginTop: 10,
    marginLeft: 16,
    marginRight: 16,
    borderRadius: 8,
    backgroundColor: Colors.white,
    borderWidth: 1,
    padding: 10,
    borderColor: Colors.white,
    elevation: 5,
    marginBottom: 5,

  },
  orderDataContainer: {
    paddingHorizontal: 10
  },
  rowContainer: {
    //flexDirection: 'row',
    marginVertical: 3
  },
  heading: {
    color: '#000',
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
  },
  value: {
    color: 'black',
    fontFamily: 'Poppins-Regular'
  },
  activityIndicator: {
    flex: 1,
    alignSelf: 'center',
    height: 100,
    position: 'absolute',
    top: '30%',
  },
  headerView: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 16,
    marginTop: 10,
    marginRight: 5
  },
  planView: {
    marginLeft: 20,
    marginTop: 10
  },
  planText: {
    color: 'black',
    fontSize: 16,
    fontFamily: 'Poppins-SemiBold'
  },

  createPlanBtn: {
    height: 40,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
    borderRadius: 8,
    //marginRight: 10
  },
  buttonTextStyle: {
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
  },
  button: {
    height: 40,
    backgroundColor: Colors.primary,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    padding: 10

  },
  logout: {
    marginRight: 10
  },
  title: {
    fontSize: 14,
    color: Colors.primary,
    fontFamily: 'Poppins-SemiBold',
    textTransform: 'capitalize'

  },
  text: {
    fontSize: 14,
    color: Colors.black,
    fontFamily: 'Poppins-Regular',
  },
  planHeading: {
    color: 'black',
    fontSize: 16,
    fontFamily: 'Poppins-SemiBold',
    marginVertical: 3
  }

})