import React, { useContext, useState, useRef } from 'react'
import { AuthContext } from '../../Context/AuthContext'
import { StyleSheet, Text, View, TouchableOpacity, Alert, Image, FlatList, LogBox, ScrollView, Button } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useFocusEffect } from '@react-navigation/native';
import Geolocation from 'react-native-geolocation-service';
import firestore from '@react-native-firebase/firestore';
import { Pressable } from 'react-native';
import Colors from '../../constants/Colors';
import FaceSDK, { Enum, FaceCaptureResponse, MatchFacesResponse, MatchFacesRequest, MatchFacesImage, MatchFacesSimilarityThresholdSplit, RNFaceApi } from '@regulaforensics/react-native-face-api'
import { locationPermission, } from '../../utils/Helper'
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
import ActionButton from 'react-native-action-button';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Dimensions } from "react-native";
import ClockIn from './clockIn';
import {
    LineChart
} from "react-native-chart-kit";
const screenWidth = Dimensions.get("window").width;
LogBox.ignoreAllLogs();

const MerchHome = ({ navigation }) => {

    const { logout, dealerData, userData, token } = useContext(AuthContext);
    console.log('Dealer data existssss : ', dealerData);
    console.log('user data exists : ', userData);

    const [clockedIn, setClockedIn] = useState(null);
    const profile = useRef('');
    const [userCordinates, setUserCordinates] = useState([]);


    const getLocation = async () => {
        let locPermissionDenied = await locationPermission();
        if (locPermissionDenied) {
            Geolocation.getCurrentPosition(
                async (res) => {
                    console.log('GET LOCATION CALLED');
                    console.log(res);
                    //getting user location
                    console.log("lattitude", res.coords.latitude);
                    console.log("longitude", res.coords.longitude);
                    setUserCordinates([res.coords.latitude, res.coords.longitude])

                },
                (error) => {
                    console.log("get location error", error);
                    console.log("please enable location ")
                },
                {
                    enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, accuracy: {
                        android: 'high',
                        ios: 'bestForNavigation',
                    }
                }

            );

        }
        else {
            console.log("location permssion denied")
        }
    }


    const logoutAlert = () => {
        Alert.alert('Confirmation', 'Are you sure, You want to logout?', [
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            { text: 'OK', onPress: () => { logout() } },
        ]);
    }


    const clockIncheck = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__user_id__": token,
            "__type__": "clockin"
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_check_user_clockin.php", requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log('clockin check data', result);
                if (result.status == 200) {
                    setClockedIn(true)
                }
                else if (result.status == 201) {
                    setClockedIn(false);
                    getProfile();
                }
            })
            .catch(error => console.log('error', error));
    }
    const getProfile = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__user_id__": token
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_ordouser_image.php", requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log('profile', result);
                let data = result.image_base64;
                let base64 = data.split(",");
                console.log('profile url', base64[1]);
                profile.current = base64[1];
                //faceRecognise();

            })
            .catch(error => console.log('error', error));
    }











    useFocusEffect(
        React.useCallback(() => {
            //getTourPlanGrahpData();
            //getPlans();
            clockIncheck();
            getLocation();
            FaceSDK.init(json => {
                response = JSON.parse(json)
                console.log(response);
                if (!response["success"]) {
                    console.log("Init failed: ");
                    console.log(json);
                }
            }, e => { })

        }, [])
    );

    const saveClockIn = () => {
        console.log("location", userCordinates);
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        var raw = JSON.stringify({
            "__user_id__": token,
            "__type__": "clockin",
            "__location__": "abc",
            "__latitude__": userCordinates[0],
            "__longitude__": userCordinates[1],
        });

        console.log(JSON.stringify({
            "__user_id__": token,
            "__type__": "clockin",
            "__location__": "abc",
            "__latitude__": userCordinates[0],
            "__longitude__": userCordinates[1],
        }))



        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/set_user_attendance.php", requestOptions)
            .then(response => response.json())
            .then(async result => {
                console.log('clock in save data', result);
                if (result?.status == "200") {
                    Alert.alert('Clock in ', `Clocked in successfully`);
                    setClockedIn(true);
                }
            })
            .catch(error => console.log('set attendance error', error));
    }


    //user face recognisation
    const faceRecognise = async () => {
        console.log("inside");
        FaceSDK.presentFaceCaptureActivity(result => {
            let res = JSON.parse(result)
            //checking user cancle image picker
            if (res.exception) {
                console.log("User Canceled Face capture option");
                return;
            }
            //console.log("image uri", FaceCaptureResponse.fromJson(JSON.parse(result)).image.bitmap);
            let base64Img = FaceCaptureResponse.fromJson(JSON.parse(result)).image.bitmap;
            const firstImage = new MatchFacesImage();
            firstImage.imageType = Enum.ImageType.PRINTED; //captured image
            firstImage.bitmap = profile.current
            const secondImage = new MatchFacesImage();
            secondImage.imageType = Enum.ImageType.LIVE; //live image
            secondImage.bitmap = base64Img;
            request = new MatchFacesRequest()
            request.images = [firstImage, secondImage]
            console.log("start compare", profile);
            //comparing two images
            FaceSDK.matchFaces(JSON.stringify(request), response => {
                response = MatchFacesResponse.fromJson(JSON.parse(response))
                console.log("ggg", response);
                FaceSDK.matchFacesSimilarityThresholdSplit(JSON.stringify(response.results), 0.75, str => {
                    var split = MatchFacesSimilarityThresholdSplit.fromJson(JSON.parse(str))
                    console.log("res", split.length);
                    if (split?.matchedFaces.length > 0) {
                        //face matched
                        let faceMatchPercentage = split.matchedFaces[0].similarity * 100
                        console.log("match percentage", faceMatchPercentage.toFixed(2));
                        saveClockIn();
                    }
                    else {
                        //face doe not match
                        alert('Face not recognised please try again.')
                    }
                }, e => { console.log("error") })
            }, e => { console.log("error") })


        }, e => { console.log("error", e) })



    }



    return (
        <View style={styles.container}>
            {
                clockedIn == false ? (
                    <ClockIn
                        faceRecognise={faceRecognise}
                        logoutAlert={logoutAlert}

                    />
                ) : (
                    <View style={{ flex: 1, backgroundColor: 'white' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginTop: 10 }}>
                            <View style={{ flex: 1 }}>
                                <Image source={require('../../assets/images/ordotext.png')} style={{ height: 50, width: 100 }} />
                            </View>
                            <View style={{ alignItems: 'center', flexDirection: 'row', paddingBottom: 10, flex: 1, justifyContent: 'flex-end' }}>
                                {/* <Image source={require('../../assets/images/reload.png')} style={{ height: 25, width: 25, tintColor: Colors.primary }} /> */}
                                <TouchableOpacity onPress={logoutAlert}>
                                    <Image source={require('../../assets/images/power-off.png')} style={{ height: 20, width: 20, tintColor: Colors.primary, marginLeft: 20, marginRight: 10 }} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ paddingHorizontal: 37, marginTop: 10 }}>
                            <Text style={styles.buttonTextStyle}>Welcome Merchandiser</Text>
                        </View>

                        {/* <View style={{backgroundColor:'red'}}> */}
                        <TouchableOpacity style={styles.elementsView} activeOpacity={0.8} onPress={() => {
                            // checkActivePlan(item, item.id);
                            //changeDealerData(item);
                            //navigation.navigate('CheckIn', { hideCheckOut: true })
                        }
                        }
                        >
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <View style={{ justifyContent: 'center' }}>
                                    <Image
                                        //source={require('../../assets/images/46.png')}
                                        source={{ uri: userData?.account_profile_pic }}
                                        style={{ width: 80, height: 80 }}
                                    />
                                </View>
                                <View style={{
                                    flex: 1,
                                    marginLeft: 8,
                                    borderLeftWidth: 1.5,
                                    paddingLeft: 10,
                                    marginLeft: 20,
                                    borderStyle: 'dotted',
                                    borderColor: 'grey',
                                    justifyContent: 'space-around'
                                }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        {/* <Text style={{ color: Colors.primary, fontSize: 12, fontFamily: 'Poppins-SemiBold', borderBottomColor: 'grey', borderBottomWidth: 0.5 }}>{item?.name}</Text> */}
                                        <Text style={{ color: Colors.primary, fontSize: 12, fontFamily: 'Poppins-SemiBold', borderBottomColor: 'grey', borderBottomWidth: 0.5 }}>{userData?.name}</Text>

                                    </View>
                                    {/* <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-Regular', marginTop: 5 }}>{item?.shipping_address_street} {item?.billing_address_city} {item?.shipping_address_state}</Text> */}
                                    <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-Regular', marginTop: 5 }}>{userData?.shipping_address_street} {userData?.shipping_address_city} {userData?.shipping_address_state}</Text>

                                    {/* <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-Regular' }}>{item?.shipping_address_country} - {item?.shipping_address_postalcode}</Text> */}
                                    <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-Regular' }}>{userData?.shipping_address_country} - {userData?.shipping_address_postalcode}</Text>
                                </View>
                            </View>
                            {/* <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Poppins-Regular', marginTop: 5, paddingLeft: 16 }}>{item?.storeid_c}</Text> */}
                            <Text style={{ fontSize: 12, color: 'black', fontFamily: 'Poppins-Regular', marginTop: 5, paddingLeft: 16 }}>{userData?.storeid_c}</Text>
                        </TouchableOpacity>
                        {/* </View> */}

                        <View style={styles.row1View}>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={styles.recoredbuttonStyle}
                                onPress={() => navigation.navigate('MerchAddProduct')}

                            >
                                <Image transition={false} source={require('../../assets/images/add.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: Colors.primary }} >
                                </Image>
                                <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Add Inventory</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={styles.recoredbuttonStyle}
                                onPress={() => navigation.navigate('MerchInventory', { screen: 'MIListDetail', action: true })}
                            //onPress={() => { navigation.navigate('AdminStores', { newReturn: true }) }}
                            >
                                <Image transition={false} source={require('../../assets/images/order.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: Colors.primary }} >
                                </Image>
                                <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Secondary Offtake</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={styles.recoredbuttonStyle}
                                onPress={() => { navigation.navigate('MerchInventory', { screen: 'CompetitorIntelligence', action: true }) }}
                            >
                                <Image transition={false} source={require('../../assets/images/market.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: Colors.primary }} >
                                </Image>
                                <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Competitor Analysis</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ ...styles.row1View, justifyContent: 'flex-start', marginLeft: 30 }}>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={styles.recoredbuttonStyle}
                                onPress={() => { navigation.navigate('MerchInventory') }}
                            >
                                <MaterialIcons name={"inventory"} size={25} color={Colors.primary} />
                                {/* <Image transition={false} source={require('../../assets/images/market.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: Colors.primary }} >
                                </Image> */}
                                <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Inventory</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={styles.recoredbuttonStyle}
                                onPress={() => navigation.navigate('SKUHistoryList')}

                            >
                                <Image transition={false} source={require('../../assets/images/history.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: Colors.primary }} >
                                </Image>
                                <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>SKU History</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                activeOpacity={0.6}
                                style={styles.recoredbuttonStyle}
                                onPress={() => navigation.navigate('MerchAttendance')}
                            //</View>onPress={() => navigation.navigate('AdminOrders')}
                            >
                                <Image transition={false} source={require('../../assets/images/person.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: Colors.primary }} >
                                </Image>
                                <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Attendance</Text>
                            </TouchableOpacity>

                        </View>
                        {/* <View style={styles.row1View}>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/specialOrders.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Special Orders</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/specialOrders.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Bundled Orders</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/promotions.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Promotions/Offer</Text>
                </TouchableOpacity>
            </View>
            <View style={{ ...styles.activeOrder, marginBottom: tabBarHeight }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'Poppins-SemiBold' }}>No Active Order</Text>
                </View>
                <View style={styles.orderContent}>
                    <View>
                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-SemiBold', fontSize: 17 }}>0</Text>
                        <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey' }}>Amount</Text>
                    </View>
                    <View>
                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-SemiBold', fontSize: 17 }}>0</Text>
                        <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey' }}>Total Items</Text>
                    </View>
                    <View>
                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-SemiBold', fontSize: 17 }}>0</Text>
                        <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey' }}>Total Qty</Text>
                    </View>
                </View>

                {/* <TouchableOpacity style={styles.createOrder}>

                    <Text style={{ color: Colors.primary, fontFamily: 'Poppins-SemiBold', fontSize: 12 }}>Create an Order</Text>

                </TouchableOpacity> 
            </View> */}
                    </View>
                )}
        </View>
    )
}

export default MerchHome

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    recoredbuttonStyle: {
        borderRadius: 4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        marginHorizontal: 5,
        shadowRadius: 2,
        elevation: 5,
        height: 100,
        width: 100,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    row1View: {
        //marginHorizontal: 50,
        // paddingHorizontal: 30,
        // marginLeft:30,
        marginTop: 10,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
    },
    buttonTextStyle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
    },
    activeOrder: {
        marginTop: 25,
        // backgroundColor: 'grey',
        marginHorizontal: 15,
        // alignItems:'center',
        paddingTop: 10,
        borderRadius: 20,
        // flex:1,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        backgroundColor: 'white',

    },
    orderContent: {
        flexDirection: 'row',
        // backgroundColor: 'red',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        marginTop: 10,
        margin: 10,
    },
    createOrder: {
        borderRadius: 0,
        marginBottom: 20,
        elevation: 5,
        paddingVertical: 10,
        marginHorizontal: 30,
        backgroundColor: 'white',
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    elementsView: {
        backgroundColor: "white",
        marginHorizontal: 37,
        //borderColor: 'black',
        //flexDirection: 'row',
        //justifyContent: 'space-between',
        //alignItems: 'center',
        marginBottom: 16,
        borderRadius: 8,
        elevation: 5,
        padding: 8
        //borderColor: '#fff',
        //borderWidth: 0.5
    },
})