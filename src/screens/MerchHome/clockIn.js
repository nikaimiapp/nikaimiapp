import React, { useCallback, useContext } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { AuthContext } from '../../Context/AuthContext';
const ClockIn = ({ faceRecognise, logoutAlert }) => {



    return (
        <View style={styles.container}>
            <View style={{ flex: 1 }}>
                <View style={{ alignItems: 'flex-end', marginRight: 10, marginTop: 10 }}>
                    <TouchableOpacity onPress={logoutAlert}>
                        <Image
                            source={require('../../assets/images/clockInPowerOff.png')}
                            style={{ height: 30, width: 30 }}
                        />
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'flex-end' }}>

                    <Image
                        source={require('../../assets/images/clockin.png')}
                        style={styles.icon}
                    />

                </View>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
                <TouchableOpacity style={styles.button} onPress={faceRecognise}>
                    <Text style={styles.buttonText}>Clock In</Text>
                </TouchableOpacity>
                <Text style={styles.text}>Clock In to proceed</Text>
            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#08082c',
    },
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        opacity: 0.5,
    },
    button: {
        backgroundColor: '#3F51B5',
        padding: 10,
        borderRadius: 5,
        marginBottom: 10,
    },
    buttonText: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    text: {
        fontSize: 16,
        marginBottom: 10,
        color: 'white',
    },
    icon: {
        width: 150,
        height: 150,
    },
});

export default ClockIn;