import { StyleSheet, Text, View, SafeAreaView, TouchableHighlight, TouchableOpacity, Image, ScrollView, Alert } from 'react-native'
import React, { useState, createRef, useContext, useRef } from 'react';
import SignatureCapture from 'react-native-signature-capture';
import { FlatList } from 'react-native-gesture-handler';
import { Dropdown } from 'react-native-element-dropdown';
import Colors from '../../constants/Colors';
import moment from 'moment';
import uuid from 'react-native-uuid';
import ImageResizer from '@bam.tech/react-native-image-resizer'
import RNFS from 'react-native-fs';
import { cameraPermission } from '../../utils/Helper';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { AuthContext } from '../../Context/AuthContext';
import ViewShot from "react-native-view-shot";
import { ProgressDialog } from 'react-native-simple-dialogs';
const AdminOrderReview = ({ navigation,route }) => {
    const { token } = useContext(AuthContext);


    const { productsArray, dealerInfo, cartData, total } = route.params;
    console.log("productArray", productsArray);
    console.log("cartData", cartData);
    console.log("dealerInfo", dealerInfo);

    const [signBase64, setSignBase64] = useState('')
    const [base64img, setBase64img] = useState('');
    const [loading, setLoading] = useState(false);

    const sign = createRef();

    const ref = useRef();

    const takesnapshot = () => {
        ref.current.capture().then(uri => {
            console.log("screen shot uri ", uri);
            RNFS.readFile(uri, 'base64')
                .then(res => {
                    console.log("screen shot base64", res)
                    createOrder(res);
                });
        });
    }

    const saveSign = () => {
        sign.current.saveImage();
    };
    const resetSign = () => {
        sign.current.resetImage();
        setSignBase64('');
    };
    const _onSaveEvent = (result) => {
        setSignBase64(result.encoded);


    };
    const _onDragEvent = () => {
        sign.current.saveImage()
        console.log('dragged');
    };
    const data = [
        {
            id: '1',
            title: 'Kaju katli',
            quantity: 1,
            cost: 35.7
        },
        {
            id: '2',
            title: 'Gulab Jamun',
            quantity: 1,
            cost: 60.2
        },
        {
            id: '3',
            title: 'Horlicks',
            quantity: 1,
            cost: 270.25
        },
    ];
    const DATA = [
        { label: 'Shipped wrong product or size', value: 'Shipped wrong product or size' },
        { label: 'The product was damaged or defective', value: 'The product was damaged or defective' },
        { label: 'The product arrived too late', value: 'The product arrived too late' },
        { label: 'The product did not match the description', value: 'The product did not match the description' },
        { label: 'Other', value: 'Other' },
    ];
    const [isFocus, setIsFocus] = useState(false);
    const [value, setValue] = useState(null);

    // const checkPermission = async () => {
    //     let PermissionDenied = await cameraPermission();
    //     if (PermissionDenied) {
    //         console.log("camera permssion granted");
    //         handleCamera();
    //     }
    //     else {
    //         console.log("camera permssion denied");
    //     }
    // }

    // const handleCamera = async () => {
    //     const res = await launchCamera({
    //         mediaType: 'photo',
    //     });
    //     console.log("response", res.assets[0].uri);
    //     imageResize(res.assets[0].uri, res.assets[0].type);
    // }
    // const handleGallery = async () => {
    //     const res = await launchImageLibrary({
    //         mediaType: 'photo',

    //     });
    //     console.log("response", res.assets[0].uri);
    //     imageResize(res.assets[0].uri, res.assets[0].type);
    // }

    // const imageResize = async (img, type) => {
    //     ImageResizer.createResizedImage(
    //         img,
    //         300,
    //         300,
    //         'JPEG',
    //         50,
    //     )
    //         .then(async (res) => {
    //             console.log('image resize', res);
    //             RNFS.readFile(res.path, 'base64')
    //                 .then(res => {
    //                     setBase64img(res)
    //                 });
    //         })
    //         .catch((err) => {
    //             console.log(" img resize error", err)
    //         });
    // }

    const uploadSignature = (snap) => {
        console.log("sing base64", snap)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "text/plain");


        var raw = JSON.stringify({
            "__note_file__": snap,
            "__note_filename__": `OrderReciept ${returnId}.png`

        })



        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/uploadFile.php", requestOptions)
            .then(response => response.json())
            .then(result => {

                console.log('Signature uploaded', result)
            })
            .catch(error => console.log('error', error));

    }

    const createOrder = async () => {
        let url = 'https://dev.ordo.primesophic.com/set_data_s.php'
        var type = "New";
        let orderId = uuid.v4();
        console.log("order id", orderId)
        fetch(url, {

            method: 'POST',

            body: JSON.stringify(



                {

                    __module_code__: "PO_17",

                    __query__: "",

                    __session_id__: "",

                    __name_value_list__: {

                        "name": "qname",

                        "date_modified": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),

                        "description": "qname",

                        "approval_issue": "",

                        "expiration": moment(new Date("2099-12-31")).format('YYYY-MM-DD hh:mm:ss'),

                        "orderid": orderId,

                        "billing_account_id": "2713663b-6639-d590-23a2-62fce7997e49",

                        "billing_account": "Walamrt",

                        "created_userval_c": "sales1",

                        "assigned_user_name": "sales1",

                        "initiated_by": "sales1",

                        "lastmodifiedby": "1878e60a-fe1c-0d00-e8b6-63119c1882aa",

                        "billing_address_street_2_c": "a",

                        "billing_address_street": "b",

                        "billing_address_city": "c",

                        "billing_address_state": "D",

                        "billing_address_postalcode": "e",

                        "billing_address_country": "g",

                        "line_items": "10",

                        "tax_amount": "10",

                        "subtotal_amount": "100",

                        "subtotal_amount_usdollar": "100",

                        "discount_amount": "200",

                        "currency_id": "-99",

                        "stage": "Approved",

                        "reason_for_return": "",

                        "term": "Neft15",

                        "terms_c": "",

                        "orderstatus_c": "Approved",

                        "invoice_status": "Not Invoiced",

                        "total_amt": "1000",

                        "total_amount": "1000",

                        "total_amount_usdollar": "1000",

                        "lastmodified": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),

                        "shipping_address_street": "a",

                        "shipping_address_state ": "b",

                        "shipping_address_country": "c",

                        "shipping_address_postalcode": "d",

                        "approval_status": "Approved",

                        "totalitems_c": "1-",

                        "device_id": "1234",

                        "os_name": "abc",

                        "osversion_c": "120",

                        "latitude": "1234",

                        "longitude": "123",

                        "type": "type"

                    }

                })

        }).then(function (response) {
            console.log("order first api response", response.json())
            //return response.json();
        })


    }



    return (
        <ViewShot style={{ flex: 1 }} ref={ref} options={{ fileName: "returnfile", format: "png", quality: 0.5 }}>
            <ScrollView style={styles.container}>
                <ProgressDialog
                    visible={loading}
                    title="Sending return request"
                    message="Please, wait..."
                />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                    <Text style={styles.title}>Order Review</Text>
                    <Image style={styles.subimage} source={require('../../assets/images/ordotext.png')} />
                </View>
                <View style={{ marginLeft: 10 }}>
                    <Text style={styles.content}>{dealerInfo?.name}</Text>
                    <Text style={styles.content}>{dealerInfo?.shipping_address_street} {dealerInfo?.billing_address_city} {dealerInfo?.shipping_address_state}</Text>
                    <Text style={styles.content}>{dealerInfo?.shipping_address_country} - {dealerInfo?.shipping_address_postalcode}</Text>
                    <Text style={styles.content}>GSTIN: {dealerInfo?.gst_number != null ? dealerInfo?.gst_number : '1000786'}</Text>
                    <View style={styles.descriptionView}>
                        <Text style={{ ...styles.content, flex: 1, marginLeft: 10 }}>Description</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1, marginRight: 10 }}>
                            <Text style={styles.content}>Qty</Text>
                            <Text style={styles.content}>Price</Text>
                        </View>
                    </View>

                    <FlatList
                        data={cartData}
                        keyExtractor={({ id }) => id}
                        renderItem={({ item }) =>
                            <View style={styles.descriptionContentView}>
                                <Text style={{ ...styles.content, flex: 1 }}>{item.description}</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1, marginLeft: 10 }}>
                                    <Text style={styles.content}>{Number(item.qty)}</Text>
                                    <Text style={styles.content}>AED {Number(item.price)}</Text>
                                </View>
                            </View>
                        }
                    />

                    <View style={styles.dropDownContainer}>
                        <Text style={styles.content}>Sub-Total : AED {Number(total)}</Text>
                        <Text style={styles.content}>GST : AED {0}</Text>
                        <Text style={styles.content}>Saving : AED {0}</Text>
                        <Text style={{ ...styles.content, fontFamily: 'Poppins-SemiBold' }}>Grand Total : AED {Number(total)}</Text>


                        {/* <Dropdown
                            style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                            placeholderStyle={styles.placeholderStyle}
                            selectedTextStyle={styles.selectedTextStyle}
                            inputSearchStyle={styles.inputSearchStyle}
                            iconStyle={styles.iconStyle}
                            data={DATA}

                            maxHeight={300}
                            labelField="label"
                            valueField="value"
                            placeholder={!isFocus ? 'Reason for Return' : '...'}

                            value={value}
                            onFocus={() => setIsFocus(true)}
                            onBlur={() => setIsFocus(false)}
                            onChange={item => {
                                setValue(item.value);
                                setIsFocus(false);
                            }}
                        /> */}
                    </View>
                    {/* <Text style={styles.modalTitle}>Upload Photo</Text>
                    <View style={styles.buttonview}>
                        <TouchableOpacity style={styles.photosContainer} onPress={checkPermission}>
                            <Text style={styles.buttonText}>Camera</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.photosContainer} onPress={handleGallery}>
                            <Text style={styles.buttonText}>Gallery</Text>
                        </TouchableOpacity>

                    </View> */}
                </View>
                {base64img && <Image source={{ uri: `data:jpeg;base64,${base64img}` }} style={{ width: 90, height: 90, resizeMode: 'cover', borderRadius: 8, marginLeft: 10 }} />}
                <View style={{ height: 110, width: "100%", marginTop: 10 }}>
                    <View style={{ marginLeft: 10 }}>
                        <Text style={styles.modalTitle}>
                            SIGN BELOW
                        </Text>
                        <View style={{ borderColor: 'grey', borderWidth: 1, height: 75, width: '100%' }}>
                            <SignatureCapture
                                style={styles.signature}
                                ref={sign}
                                onSaveEvent={_onSaveEvent}
                                onDragEvent={_onDragEvent}
                                showNativeButtons={false}
                                showTitleLabel={false}
                                viewMode={'portrait'}
                                minStrokeWidth={2}
                                maxStrokeWidth={6}
                            />
                        </View>
                    </View>

                </View>

                <View style={{ flexDirection: 'row', marginLeft: 10, justifyContent: 'space-between' }}>
                    <Text style={{ ...styles.content, }}>Date: {moment(new Date()).format('YYYY-MM-DD hh:mm a')}</Text>

                    <TouchableOpacity>
                        <Text style={{ ...styles.content, color: 'red' }}
                            onPress={() => {
                                resetSign();
                            }}>Clear Signature
                        </Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity style={styles.sendButtonView} onPress={() => Alert.alert('Alert', 'Order created sucessfully', [
                    { text: 'OK', onPress: () => navigation.navigate('BottomTab') }
                ])}>
                    <Text style={styles.sendButton}>Place your Order</Text>
                </TouchableOpacity>

            </ScrollView >
        </ViewShot>
    )
}

export default AdminOrderReview

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        backgroundColor: 'white'
    },
    title: {
        marginVertical: 5,
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: 'black',
        marginLeft: 10
    },
    content: {
        fontSize: 12,
        fontFamily: 'Poppins-Regular',
        color: 'black',
    },
    descriptionView: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: 'grey'
    },
    descriptionContentView: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingHorizontal: 20
    },
    grandTotalView: {
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'black',
        alignSelf: 'flex-end'
    },
    titleStyle: {
        fontSize: 10,
        textAlign: 'left',
        marginTop: 10,
        fontFamily: 'Poppins-SemiBold',
        color: 'black'
    },
    signature: {
        flex: 1,
        borderColor: '#00003',
        borderWidth: 1,

    },
    buttonStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        height: 37,
        backgroundColor: '#eeeeee',
        margin: 20,
    },
    sendButtonView: {
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        paddingVertical: 10,
        height: 40,
        alignItems: "center",
        marginTop: 10,
        marginHorizontal: 10,
        marginBottom: 20
    },
    sendButton: {
        color: 'blue',
        fontFamily: 'Poppins-Regular'
    },
    subimage: {
        height: 80,
        width: 80
    },
    dropDownContainer: {
        backgroundColor: 'white',
        marginBottom: 10,
        marginTop: 10,
        alignItems: 'flex-end',

    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
    },
    placeholderStyle: {
        fontSize: 14,
        fontFamily: 'Poppins-Regular'
    },
    selectedTextStyle: {
        fontSize: 14,
        fontFamily: 'Poppins-Regular'
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    modalTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'Poppins-SemiBold'
    },
    buttonview: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    photosContainer: {
        height: 40,
        padding: 10,
        borderRadius: 10,
        marginVertical: 10,
        backgroundColor: Colors.primary,
        marginRight: 10
    },
    buttonText: {
        fontFamily: 'Poppins-Regular',
        color: 'white'
    },
})