import { StyleSheet, Text, View, TouchableOpacity, Alert } from 'react-native'
import React, { useContext } from 'react'
import { Image } from 'react-native-animatable'
import Colors from '../../constants/Colors'
import { AuthContext } from '../../Context/AuthContext'
import { useBottomTabBarHeight } from '@react-navigation/bottom-tabs';

const AdminHome = ({ navigation }) => {
    const tabBarHeight = useBottomTabBarHeight();
    const { logout } = useContext(AuthContext);

    const logoutAlert = () => {
        Alert.alert('Confirmation', 'Are you sure, You want to logout?', [
            {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
            },
            { text: 'OK', onPress: () => { logout() } },
        ]);
    }

    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 30, marginTop: 10 }}>
                <View style={{ flex: 1 }}>
                    <Image source={require('../../assets/images/ordotext.png')} style={{ height: 50, width: 100 }} />
                </View>
                <View style={{ alignItems: 'center', flexDirection: 'row', paddingBottom: 10, flex: 1, justifyContent: 'flex-end' }}>
                    <Image source={require('../../assets/images/reload.png')} style={{ height: 25, width: 25, tintColor: Colors.primary }} />
                    <TouchableOpacity onPress={logoutAlert}>
                        <Image source={require('../../assets/images/power-off.png')} style={{ height: 20, width: 20, tintColor: Colors.primary, marginLeft: 20, marginRight: 10 }} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ paddingHorizontal: 37, marginTop: 10 }}>
                <Text style={styles.buttonTextStyle}>Welcome Admin</Text>
            </View>
            <View style={styles.row1View}>
                <TouchableOpacity
                    activeOpacity={0.6}
                    style={styles.recoredbuttonStyle}
                    onPress={() => { navigation.navigate('AdminStores',{newOrder: true}) }}
                >
                    <Image transition={false} source={require('../../assets/images/order.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>New Order</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.6}
                    style={styles.recoredbuttonStyle}
                    onPress={() => { navigation.navigate('AdminStores',{newReturn: true}) }}
                >
                    <Image transition={false} source={require('../../assets/images/return.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Sales Return</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/stock.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Stock/Inventory</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.row1View}>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/savedOrders.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Saved Orders</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} onPress={() => navigation.navigate('AdminOrders')}>
                    <Image transition={false} source={require('../../assets/images/orderLog.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Order Log</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/history.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>SKU History</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.row1View}>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/specialOrders.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Special Orders</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/specialOrders.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Bundled Orders</Text>
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
                    <Image transition={false} source={require('../../assets/images/promotions.png')} style={{ width: 25, height: 25, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: 'blue' }} >
                    </Image>
                    <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey', textAlign: 'center', marginTop: 12 }}>Promotions/Offer</Text>
                </TouchableOpacity>
            </View>
            <View style={{ ...styles.activeOrder, marginBottom: tabBarHeight }}>
                <View style={{ alignItems: 'center' }}>
                    <Text style={{ fontFamily: 'Poppins-SemiBold' }}>No Active Order</Text>
                </View>
                <View style={styles.orderContent}>
                    <View>
                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-SemiBold', fontSize: 17 }}>0</Text>
                        <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey' }}>Amount</Text>
                    </View>
                    <View>
                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-SemiBold', fontSize: 17 }}>0</Text>
                        <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey' }}>Total Items</Text>
                    </View>
                    <View>
                        <Text style={{ textAlign: 'center', fontFamily: 'Poppins-SemiBold', fontSize: 17 }}>0</Text>
                        <Text style={{ fontFamily: "Poppins-Regular", fontSize: 12, color: 'grey' }}>Total Qty</Text>
                    </View>
                </View>

                <TouchableOpacity style={styles.createOrder}>

                    <Text style={{ color: Colors.primary, fontFamily: 'Poppins-SemiBold', fontSize: 12 }}>Create an Order</Text>

                </TouchableOpacity>
            </View>
        </View>
    )
}

export default AdminHome

const styles = StyleSheet.create({
    recoredbuttonStyle: {
        borderRadius: 4,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        marginHorizontal: 5,
        shadowRadius: 2,
        elevation: 5,
        height: 100,
        width: 100,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10
    },
    row1View: {
        //marginHorizontal: 50,
        paddingHorizontal: 30,
        marginTop: 10,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'center',
    },
    buttonTextStyle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
    },
    activeOrder: {
        marginTop: 25,
        // backgroundColor: 'grey',
        marginHorizontal: 15,
        // alignItems:'center',
        paddingTop: 10,
        borderRadius: 20,
        // flex:1,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        elevation: 5,
        backgroundColor: 'white',

    },
    orderContent: {
        flexDirection: 'row',
        // backgroundColor: 'red',
        justifyContent: 'space-between',
        paddingHorizontal: 30,
        marginTop: 10,
        margin: 10,
    },
    createOrder: {
        borderRadius: 0,
        marginBottom: 20,
        elevation: 5,
        paddingVertical: 10,
        marginHorizontal: 30,
        backgroundColor: 'white',
        marginTop: 5,
        justifyContent: 'center',
        alignItems: 'center'
    }
})