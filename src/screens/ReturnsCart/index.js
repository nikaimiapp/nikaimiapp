import { StyleSheet, Text, View, Image, FlatList, Keyboard, TouchableOpacity, ToastAndroid, TextInput, Modal, Pressable } from 'react-native'
import React, { useState, useEffect } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../constants/Colors';

const ReturnsCart = ({ navigation, route }) => {
    const { productsArray, returnId } = route.params;
    const dealerInfo = route.params?.dealerInfo;
    console.log("return id", returnId)
    //console.log("prodcuts array", productsArray)
    const [masterData, setMasterData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [search, setSearch] = useState('');
    const [totalAmt, setTotalAmt] = useState(0);
    const [totalQty, setTotalQty] = useState(0);

    //good






    const calOrderStat = (array) => {
        let tempTotalAmt = 0;
        let tempQty = 0;
        //calculating total price and total quantity
        array.forEach(item => {
            let amt = Number(item?.product_total_price) * Number(item?.product_qty);
            tempTotalAmt = tempTotalAmt + amt;
            tempQty = tempQty + Number(item?.product_qty);

        });
        console.log("updated amt ", tempTotalAmt);
        setTotalAmt(tempTotalAmt);
        setTotalQty(tempQty);





    }

    storeQty = async () => {
        let newProductArray = await productsArray.map((item) => {
            return {
                ...item,
                Ordered_Product_Qty: item.product_qty
            }

        })
        setMasterData(newProductArray);
        setFilteredData(newProductArray);

    }


    useEffect(() => {
        storeQty();
        calOrderStat(productsArray);
    }, [])


    const [isModalVisible, setModalVisible] = useState(false)
    // const orders = [
    //     {
    //         id: 1,
    //         netWt: 250,
    //         name: 'Soap',
    //         imageSource: 'https://m.media-amazon.com/images/I/61KBJrvYy3L._AC_UF1000,1000_QL80_.jpg',
    //         quantity: 0,
    //         rupees: '₹76',
    //         imageNumber: 58578
    //     },
    //     {
    //         id: 2,
    //         netWt: 568,
    //         name: 'Pot',
    //         imageSource: 'https://m.media-amazon.com/images/I/81UY6IfHNHL._AC_UF894,1000_QL80_.jpg',
    //         quantity: 0,
    //         rupees: '₹60',
    //         imageNumber: 76467
    //     },
    //     {
    //         id: 3,
    //         netWt: 574,
    //         name: 'Pillow',
    //         imageSource: 'https://m.media-amazon.com/images/I/611LBf4W5TL._AC_UY1100_.jpg',
    //         quantity: 0,
    //         rupees: '₹34',
    //         imageNumber: 74574
    //     },
    //     {
    //         id: 4,
    //         netWt: 765,
    //         name: 'Ring Box',
    //         imageSource: 'https://m.media-amazon.com/images/I/51cmW4qRqdL._AC_SS300_.jpg',
    //         quantity: 0,
    //         rupees: '₹18',
    //         imageNumber: 65685
    //     }
    // ]

    const searchProduct = (text) => {
        // Check if searched text is not blank
        if (text) {
            // Inserted text is not blank
            // Filter the masterDataSource
            // Update FilteredDataSource
            const newData = filteredData.filter(
                function (item) {
                    const itemData = item.name
                        ? item.name.toUpperCase() + item.part_number.toUpperCase()
                        : ''.toUpperCase();
                    const textData = text.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                });
            setFilteredData(newData);
            setSearch(text);
        } else {
            // Inserted text is blank
            // Update FilteredDataSource with masterDataSource
            setFilteredData(masterData);
            setSearch(text);
        }
    };

    const addProduct = (item) => {
        console.log('clicked')
        console.log("quantity ", item);
        let tempData = cartData;
        let productExist = false
        if (tempData.length > 0) {
            cartData.map((itm) => {
                if (itm.id == item.id) {
                    console.log("product already exist in the cart")
                    productExist = true;
                    //inscreasing  quantity of the product
                    ++itm.qty;
                }
            })
            //product not present adding item in cart
            if (!productExist) {
                tempData.push({ ...item, qty: 1 });
            }
            setFilteredData(tempData);
            ToastAndroid.show(
                'Items Added Successfully to your cart',
                ToastAndroid.SHORT
            );

        }
        else {
            //cart is empty adding product
            console.log("cart is  empty")
            tempData.push({ ...item, qty: 1 });
        }

    }

    const updateQuantity = (item, type, index) => {
        let tempCart = [];
        tempCart = [...masterData];
        tempCart.map((itm) => {
            if (itm.id == item.id) {
                if (type == 'add') {
                    //checking sales man cannot ordered quantity valu
                    if (item.product_qty < itm.Ordered_Product_Qty) {
                        ++itm.product_qty;
                        setFilteredData(tempCart);
                        calOrderStat(tempCart);
                    }
                    else {
                        alert('You cannot exceed the ordered quantity value')
                    }
                }
                else {
                    if (itm.product_qty > 1) {
                        --itm.product_qty;
                        setFilteredData(tempCart);
                        calOrderStat(tempCart);
                    }
                    else {
                        deleteProduct(index)
                    }
                }

            }
        })

    }

    const deleteProduct = (index) => {
        console.log('ggg');
        let tempCart = [...masterData];
        tempCart.splice(index, 1);
        setFilteredData(tempCart);
        setMasterData(tempCart);
        calOrderStat(tempCart);
    }

    const returnItems = async () => {
        if (filteredData.length > 0) {
            let returnArray = masterData.map((item) => {
                return {
                    id: item.id,
                    qty: item.product_qty
                }
            })
            console.log("return array", returnArray);
            navigation.navigate('ReturnOrderDetails', { productsArray: masterData, returnId: returnId, returnArray: returnArray,dealerInfo:dealerInfo })
        }
        else {
            alert('Sorry, no items to return')
        }

    }

    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10, alignItems: 'center' }}>
                <TouchableOpacity onPress={() => navigation.goBack()} >
                    <AntDesign name='arrowleft' size={25} color={Colors.black} />
                </TouchableOpacity>
                <Text style={{ alignSelf: 'center', fontSize: 20, color: '#000', fontFamily: 'Poppins-SemiBold', marginVertical: 5 }}>My Cart</Text>
                <TouchableOpacity style={styles.saveButtonView} onPress={returnItems}>
                    <Text style={styles.sendButton}>Return</Text>
                </TouchableOpacity>

            </View>
            <View style={{ flexDirection: 'row' }}>
                <View style={styles.modalSearchContainer}>
                    <TextInput
                        style={styles.input}
                        value={search}
                        placeholder="Search product"
                        placeholderTextColor="gray"
                        onChangeText={(val) => searchProduct(val)}

                    />
                    <TouchableOpacity style={styles.searchButton} >
                        <AntDesign name="search1" size={20} color="black" />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={{ height: 45, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderRadius: 8, paddingHorizontal: 16, elevation: 5, flex: 0.2, marginLeft: 5 }}
                    onPress={() => {
                        setSearch('');
                        setFilteredData(masterData)
                        Keyboard.dismiss();
                    }
                    }
                >
                    <Text style={{ color: 'blue', fontFamily: 'Poppins-Regular', fontSize: 14 }}>Clear</Text>

                </TouchableOpacity>
            </View>
            <View style={{ marginLeft: 10 }}>
                <Text style={styles.textColor}>Order Totals:</Text>
            </View>
            <View style={{ justifyContent: 'space-between', flexDirection: 'row', marginHorizontal: 10 }}>
                <Text style={styles.textColor}>Items: {masterData.length}</Text>
                <Text style={styles.textColor}>Qty: {totalQty}</Text>
                <Text style={styles.textColor}>Price: AED {totalAmt}</Text>
            </View>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={filteredData}
                keyboardShouldPersistTaps='handled'
                // renderItem={({ item, index, array }) =>
                //     <View style={styles.elementsView}>
                //         <View style={{ flexDirection: 'row', }}>
                //             <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                //                 <Image
                //                     source={{ uri: item.imageSource }}
                //                     style={styles.imageView}
                //                 />
                //                 <Text style={styles.imageText} >{item.imageNumber}</Text>
                //             </View>
                //             <View style={{ marginLeft: 10, marginTop: 20 }}>
                //                 <View style={{ borderBottomColor: 'grey', borderBottomWidth: 0.5 }}>
                //                     <Text>Net wt: {item.netWt}</Text>
                //                 </View>
                //                 <TouchableOpacity >
                //                     <Text style={styles.elementText} >{item.name}</Text>
                //                 </TouchableOpacity>
                //             </View>
                //             <View>
                //                 <View style={{ alignSelf: 'flex-end', marginRight: 16 }}>
                //                     <Image source={require('../../assets/images/orderClose.png')} style={styles.orderCloseView} />
                //                     {/* </View>
                //                 <View style={styles.rupeesView}> */}
                //                     <Text style={styles.textColor}>{item.rupees}</Text>
                //                 </View>
                //                 <View style={{ flexDirection: 'row' }}>
                //                     <View>
                //                         <TouchableOpacity
                //                             style={styles.minusButton}
                //                         >
                //                             <Text style={{ color: 'black' }}>-</Text>
                //                         </TouchableOpacity>
                //                     </View>
                //                     <View style={styles.quantityCount}>
                //                         <Text style={{ color: 'black' }}>{item.quantity}</Text>
                //                     </View>
                //                     <View>
                //                         <TouchableOpacity
                //                             style={styles.quantityCount}
                //                         >
                //                             <Text style={{ color: 'black' }}>+</Text>
                //                         </TouchableOpacity>
                //                     </View>
                //                 </View>
                //             </View>
                //         </View>

                //     </View>
                // }
                renderItem={({ item, index }) =>
                    <Pressable style={styles.elementsView} >
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Image
                                source={{ uri: item.product_image }}
                                style={{ width: 60, height: 60 }}
                            />
                            {console.log('imagel ink', item.product_image)}
                            <View style={{ flex: 1, marginLeft: 8 }}>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{ color: 'grey', fontSize: 14, fontFamily: 'Poppins-Regular', borderBottomColor: 'grey', borderBottomWidth: 0.5 }}>Net wt: {item.weight_c}</Text>
                                    <TouchableOpacity onPress={() => deleteProduct(index)}>
                                        <Image source={require('../../assets/images/orderClose.png')} style={styles.orderCloseView} />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5 }}>
                                    <Text style={{ color: 'black', fontSize: 14, fontFamily: 'Poppins-Regular' }}>{item.name.length < 20 ? item.name : item.name.slice(0, 20) + '\n' + item.name.slice(20)}</Text>
                                    <Text style={{ color: 'black', fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>AED {Number(item.product_total_price)}</Text>

                                </View>

                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 5, paddingLeft: 5 }}>
                            <Text style={{ fontSize: 14, color: 'black', fontFamily: 'Poppins-Regular', }}>{item.part_number}</Text>
                            <View style={{ flexDirection: 'row', }}>
                                <TouchableOpacity style={{ width: 40, height: 30, borderRadius: 5, borderColor: 'grey', borderWidth: 1, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}
                                    onPress={() => updateQuantity(item, 'minus', index)} >
                                    <Text style={{ color: 'black', fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>-</Text>
                                </TouchableOpacity>
                                <View style={{ width: 45, height: 30, borderRadius: 5, borderColor: 'grey', borderWidth: 1, justifyContent: 'center', alignItems: 'center', marginRight: 10 }}>
                                    <Text style={{ color: 'black', fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>{Number(item.product_qty)}</Text>
                                </View>
                                <TouchableOpacity style={{ width: 40, height: 30, borderRadius: 5, borderColor: 'grey', borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}
                                    onPress={() => updateQuantity(item, 'add', index)}
                                >
                                    <Text style={{ color: 'black', fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>+</Text>
                                </TouchableOpacity>

                            </View>
                        </View>
                    </Pressable>


                }
                keyExtractor={(item) => item.id.toString()}

            />
        </View >
    )
}

export default ReturnsCart

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        paddingBottom:0,
        backgroundColor: 'white'
    },
    elementsView: {
        backgroundColor: "white",
        margin: 5,
        //borderColor: 'black',
        //flexDirection: 'row',
        //justifyContent: 'space-between',
        //alignItems: 'center',
        marginBottom: 16,
        borderRadius: 8,
        elevation: 5,
        padding: 16
        //borderColor: '#fff',
        //borderWidth: 0.5
    },
    imageView: {
        width: 80,
        height: 80,
        // borderRadius: 40,
        // marginTop: 20,
        // marginBottom: 10
    },
    elementText: {
        fontSize: 15,
        fontFamily: 'Poppins-SemiBold',
        color: 'black'
    },
    minusButton: {
        width: 45,
        height: 30,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 30,
        marginLeft: 10
    },
    modalMinusButton: {
        width: 35,
        height: 20,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 40,
        marginLeft: 10
    },
    quantityCount: {
        width: 45,
        height: 30,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 30,
        marginLeft: 1
    },
    modalQuantityCount: {
        width: 35,
        height: 20,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 40,
        marginLeft: 1
    },
    orderCloseView: {
        height: 15,
        width: 15,
        //marginTop: 30
    },
    imageText: {
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'black',
    },
    searchContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 5,

        //paddingVertical: 5,
        paddingHorizontal: 10,
        marginLeft: 10
    },
    input: {
        flex: 1,
        fontSize: 16,
        marginRight: 10,
        fontFamily: 'Poppins-Regular'
    },
    searchButton: {
        padding: 5,
    },
    sendButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'green',
        paddingVertical: 10,
        paddingHorizontal: 20,

        height: 40,
        marginLeft: 10
    },
    saveButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: Colors.primary,
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10
    },
    deleteButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'red',
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10
    },
    addButtonView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10,
        alignSelf: 'center'
    },
    modalAddButtonView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        paddingVertical: 5,
        paddingHorizontal: 15,
        height: 35,
        //alignSelf: 'flex-end',
        //marginLeft: 30,
        //marginTop: 60
    },
    buttonText: {
        color: 'blue',
        fontFamily: 'Poppins-Regular'
    },
    sendButton: {
        color: 'white',
        fontFamily: 'Poppins-Regular'
    },
    deleteButton: {
        color: 'red'
    },
    saveButton: {
        color: 'purple'
    },
    textColor: {
        color: 'black',
        fontFamily: 'Poppins-Regular',

    },
    searchModal: {
        backgroundColor: 'white',
        padding: 20,
        width: '90%',
        marginHorizontal: 10,
        borderRadius: 10,
        elevation: 5,
        //borderColor: 'black',
        //borderWidth: 1,
        marginVertical: 100
        // flexDirection:'row'
    },
    modalSearchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 5,
        paddingHorizontal: 10,
        marginBottom: 10,
        flex: 1
    },
    modalTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'Poppins-SemiBold'
    },
})