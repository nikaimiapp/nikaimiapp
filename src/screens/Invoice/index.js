import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity, Keyboard, TextInput, Modal, Pressable } from 'react-native'
import React, { useState, useEffect, useContext } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../constants/Colors';
import { AuthContext } from '../../Context/AuthContext';
const Invoice = ({ navigation, route }) => {
    const { dealerData } = useContext(AuthContext);
    //uyvdvg uvdg ihfh foj j 
    const dealerInfo = route.params?.item
    console.log('itemmmmmm',dealerInfo)
    console.log('item mkmkmkm',dealerInfo)
    const [masterData, setMasterData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [search, setSearch] = useState('');
    useEffect(() => {
        loadAllProduct()

    }, [])

    const [cartData, setCartData] = useState([]);

    const loadAllProduct = async () => {
        console.log("loading all product");
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__username__": "sales1",
            "__accountid__": dealerInfo?dealerInfo.id:dealerData?.id //:dealerData?.id,
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_orders.php", requestOptions)
            .then(response => response.json())
            .then(async result => {
                console.log("get order res", result)
                // //console.log('all product data fetched');

                let tempArray = [];
                result.forEach(item => {
                    console.log('order id ', item.id)
                    //checking orders having products or not
                    if (item?.products_array) {
                        console.log("have product array", item)
                        tempArray.push({
                            id: item?.id,
                            name: item?.name,
                            number: item?.number,
                            total_amt: item?.total_amt,
                            shipping_address_street: item?.shipping_address_street,
                            products_array: item?.products_array,
                            sku: item.products_array?.length
                        })
                    }

                });
                console.log("product data", tempArray);
                setMasterData(tempArray)
                setFilteredData(tempArray);



            })
            .catch(error => console.log('error', error));
    }

    const searchProduct = (text) => {
        // Check if searched text is not blank
        if (text) {
            // Inserted text is not blank
            // Filter the masterDataSource
            // Update FilteredDataSource
            const newData = masterData.filter(
                function (item) {
                    const itemData = item.name
                        ? item.name.toUpperCase() + item.number.toUpperCase()
                        : ''.toUpperCase();
                    const textData = text.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                });
            setFilteredData(newData);
            setSearch(text);
        } else {
            // Inserted text is blank
            // Update FilteredDataSource with masterDataSource
            setFilteredData(masterData);
            setSearch(text);
        }
    };



    return (
        <View style={styles.container}>
            <View style={{ ...styles.headercontainer }}>
                <TouchableOpacity onPress={() => navigation.goBack()} >
                    <AntDesign name='arrowleft' size={25} color={Colors.black} />
                </TouchableOpacity>
                <Text style={styles.headerTitle}>Return Order</Text>
            </View>
            {/* <Text style={{ alignSelf: 'center', fontSize: 20, color: Colors.primary, fontFamily: 'Poppins-SemiBold', marginVertical: 5 }}>Inventory</Text> */}
            <View style={{ flexDirection: 'row' }}>
                <View style={styles.modalSearchContainer}>
                    <TextInput
                        style={styles.input}
                        value={search}
                        placeholder="Search order"
                        placeholderTextColor="gray"
                        onChangeText={(val) => searchProduct(val)}

                    />
                    <TouchableOpacity style={styles.searchButton} >
                        <AntDesign name="search1" size={20} color="black" />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity
                    style={{ height: 45, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff', borderRadius: 8, paddingHorizontal: 16, elevation: 5, flex: 0.2 }}
                    onPress={() => {
                        setSearch('');
                        setFilteredData(masterData)
                        Keyboard.dismiss();
                    }
                    }
                >
                    <Text style={{ color: 'blue', fontFamily: 'Poppins-Regular', fontSize: 14 }}>Clear</Text>

                </TouchableOpacity>
            </View>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={filteredData}
                keyboardShouldPersistTaps='handled'
                renderItem={({ item }) =>

                    <Pressable style={styles.elementsView} onPress={() => {
                        navigation.navigate('ReturnsCart', { productsArray: item?.products_array, returnId: item?.id, dealerInfo: dealerInfo })
                    }
                    }
                    >
                        <View style={{ flexDirection: 'row', paddingRight: 5 }}>
                            <Image
                                source={require('../../assets/images/invoice.png')}
                                style={{ height: 50, width: 50, alignSelf: 'center' }}
                            />

                            <View style={{ flex: 1, marginLeft: 10 }}>
                                <Text style={{ color: Colors.primary, fontSize: 12, fontFamily: 'Poppins-SemiBold' }}>{item?.name}</Text>
                                <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-Regular' }}>Invoice# : {item?.number}</Text>
                                <Text style={{ color: 'black', fontSize: 12, fontFamily: 'Poppins-Regular' }}>Amount($) : {Number(item?.total_amt)}      SKU's : {item?.sku}</Text>
                                <Text style={{ color: Colors.primary, fontSize: 10, fontFamily: 'Poppins-Regular' }}>Delivered to : {item?.shipping_address_street}</Text>






                            </View>
                        </View>

                    </Pressable>


                }

            />

        </View>



    )
}

export default Invoice

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 24,
        backgroundColor: 'white'
    },
    elementsView: {
        backgroundColor: "white",
        margin: 5,
        //borderColor: 'black',
        //flexDirection: 'row',
        //justifyContent: 'space-between',
        //alignItems: 'center',
        marginBottom: 16,
        borderRadius: 8,
        elevation: 5,
        padding: 16,
        paddingRight: 5,
        //borderColor: '#fff',
        //borderWidth: 0.5
    },
    imageView: {
        width: 80,
        height: 80,
        // borderRadius: 40,
        // marginTop: 20,
        // marginBottom: 10
    },
    elementText: {
        fontSize: 15,
        fontFamily: 'Poppins-SemiBold',
        color: 'black'
    },
    minusButton: {
        width: 45,
        height: 30,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 30,
        marginLeft: 10
    },
    modalMinusButton: {
        width: 35,
        height: 20,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 40,
        marginLeft: 10
    },
    quantityCount: {
        width: 45,
        height: 30,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 30,
        marginLeft: 1
    },
    modalQuantityCount: {
        width: 35,
        height: 20,
        borderColor: 'grey',
        borderWidth: 0.5,
        borderRadius: 5,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        marginTop: 40,
        marginLeft: 1
    },
    orderCloseView: {
        height: 15,
        width: 15,
        //marginTop: 30
    },
    imageText: {
        fontSize: 15,
        fontFamily: 'Poppins-Regular',
        color: 'black',
    },
    searchContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 5,

        //paddingVertical: 5,
        paddingHorizontal: 10,
        marginLeft: 10
    },
    input: {
        flex: 1,
        fontSize: 16,
        marginRight: 10,
        fontFamily: 'Poppins-Regular'
    },
    searchButton: {
        padding: 5,
    },
    sendButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'green',
        paddingVertical: 10,
        paddingHorizontal: 20,

        height: 40,
        marginLeft: 10
    },
    saveButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: Colors.primary,
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10
    },
    deleteButtonView: {
        borderRadius: 5,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'red',
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10
    },
    addButtonView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 40,
        marginLeft: 10,
        alignSelf: 'center'
    },
    modalAddButtonView: {
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white',
        paddingVertical: 5,
        paddingHorizontal: 15,
        height: 35,
        //alignSelf: 'flex-end',
        //marginLeft: 30,
        //marginTop: 60
    },
    buttonText: {
        color: 'blue',
        fontFamily: 'Poppins-Regular'
    },
    sendButton: {
        color: 'white',
        fontFamily: 'Poppins-Regular'
    },
    deleteButton: {
        color: 'red'
    },
    saveButton: {
        color: 'purple'
    },
    textColor: {
        color: 'black',
        fontFamily: 'Poppins-Regular',

    },
    searchModal: {
        backgroundColor: 'white',
        padding: 20,
        width: '90%',
        marginHorizontal: 10,
        borderRadius: 10,
        elevation: 5,
        //borderColor: 'black',
        //borderWidth: 1,
        marginVertical: 100
        // flexDirection:'row'
    },
    modalSearchContainer: {
        flex: 0.8,
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'lightgray',
        borderRadius: 5,
        paddingHorizontal: 10,
        marginBottom: 10,
        marginRight: 10
    },
    modalTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'Poppins-SemiBold'
    },
    headercontainer: {
        paddingVertical: 5,
        //backgroundColor:'red',
        flexDirection: 'row',
        alignItems: 'center',

    },
    headerTitle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: Colors.black,
        marginLeft: 10,
        marginTop: 3,

    },
    text: {
        fontFamily: 'Poppins-Regular',
    }
})
