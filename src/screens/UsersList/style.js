import { StyleSheet} from 'react-native'
export default StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#fff',
        
    },
    header:{
        paddingVertical:16,
        paddingHorizontal:16,
        flexDirection:'row',
        justifyContent:'space-between',
        elevation:5,
        paddingHorizontal:16,
        backgroundColor:'#fff',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 0.5 },
        shadowOpacity: 0.2,
        shadowRadius: 1, 
       
    },
    HeaderText:{
        fontSize:18,
        color:'#000',
        fontFamily:'Poppins-SemiBold',
       
       
        
    },
    flatViewcontainer:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        borderBottomColor:'#ccc',
        borderBottomWidth:1,
        paddingVertical:12,
        paddingHorizontal:16,
        flex:1
        
    
    },
    imgStyle:{
        height:50,
        width:50,
        borderRadius:25
    },
    name:{
        fontSize:16,
        color:'#000',
        
        textTransform:'capitalize',
        marginLeft:10,
        fontFamily:'Poppins-Regular'

    },
    msg:{
        fontSize:15,
        

    },
    msgContainer:{
        flexDirection:'row',
        alignItems:'center'
    },
    unreadContainer:{
        height:25,
        width:25,
        borderRadius:25/2,
        backgroundColor:'#176ce8',
        justifyContent:'center',
        alignItems:'center'
    },
    unreadText:{
        fontSize:12,
        fontWeight:'400',
        color:'#fff'
    }
})
