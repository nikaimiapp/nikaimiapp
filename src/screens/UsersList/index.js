import React, { useState, useEffect, useContext } from 'react'
import { Text, View, TouchableOpacity, FlatList, Image } from 'react-native'
import styles from './style'
import Icon from 'react-native-vector-icons/AntDesign';

import { useFocusEffect } from '@react-navigation/native';
import { AuthContext } from '../../Context/AuthContext';

const UserList = ({ navigation, route }) => {
    const [data, setData] = useState('');
    const { token } = useContext(AuthContext);
    console.log("token", token)
    //fetching past conversation
    useFocusEffect(
        React.useCallback(() => {
            console.log('inside use effect');
            const ws = new WebSocket('wss://ordo.v2.primesophic.com:8090');
            //open connection
            ws.onopen = (e) => {
                console.log('ordo WebSocket connection opened');
                ws.send(JSON.stringify({ type: 'past_conversation', by: token, device_type: 'mobile' }))
                console.log("past conversation request sent")
            }
            ws.onmessage = (e) => {
                //alert("pass conversation reponse reicved");
                // a message was received     
                console.log("past conversation response\n", e.data);
                let res = JSON.parse(e.data);
                if (res?.api_name == 'past_conversation') {
                    setData(res?.data)
                }

            };
            ws.onerror = (e) => {
                // an error occurred
                console.log("ordo web socket connection failed ", e.message);

            };
            ws.onclose = () => {
                console.log('ordo WebSocket connection closed');

            }
            return () => {
                ws.close();
            }
        }, [])
    );


    const renderItem = ({ item }) => {
        return (
            <TouchableOpacity
                style={styles.flatViewcontainer}
                activeOpacity={0.5}
                onPress={() => navigation.navigate('Chat', { item: item, token: token })}
            >
                <View style={styles.msgContainer}>
                    <Image style={styles.imgStyle} source={{ uri: item?.profile_image }} />
                    <Text style={styles.name}>{item.name}</Text>
                </View>
                {/* showing unread message count */}
                {item.unread_message > 0 && <View style={styles.unreadContainer}>
                    <Text style={styles.unreadText}>{item.unread_message}</Text>
                </View>}

            </TouchableOpacity>
        )

    }
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Icon name='arrowleft' size={25} color='#000' />
                </TouchableOpacity>

                <Text style={styles.HeaderText}>Messages</Text>
                <TouchableOpacity onPress={() => navigation.navigate('Search')}>
                    <Icon name='search1' size={25} color='#000' />
                </TouchableOpacity>

            </View>

            <FlatList
                data={data}
                renderItem={renderItem}
                showsVerticalScrollIndicator={false}


            />

        </View>
    )
}

export default UserList


