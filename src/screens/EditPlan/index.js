import React, { useState, useEffect, useRef, useContext } from 'react';
import { View, TextInput, Button, Text, StyleSheet, Dimensions, ScrollView, FlatList, Pressable, Alert } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import DatePicker from 'react-native-date-picker'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Image } from 'react-native-animatable';

import Colors from '../../constants/Colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import { RadioButton } from 'react-native-paper';
import RadioButton from 'react-native-radio-button'
import moment from 'moment';
import { format, lastDayOfMonth, getDay, addDays } from 'date-fns';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AuthContext } from '../../Context/AuthContext';
const EditPlan = ({ navigation, route }) => {

    const { id } = route.params;
    const manager = route.params?.manager;
    console.log("tour plan id", id);

    const [dateText1, setDateText1] = useState(format(new Date(), 'yyyy-MM-dd'));
    const [dateText2, setDateText2] = useState(format(lastDayOfMonth(new Date()), 'yyyy-MM-dd'));
    const { token } = useContext(AuthContext);
    const [planData, setPlanData] = useState('');
    const [dealerArray, setDealerArray] = useState([]);

    const [mSelected, setMSelected] = useState(false)
    const [wSelected, setWSelected] = useState(false)
    const [planName, setPlanName] = useState('');



    const getPlanDetails = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__user_id__": token,
            "__id__": id
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_tour_plan_detail.php", requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log('plan details data 123 ', result.status[0])
                setPlanData(result.status[0]);
                //setting tour plan
                if (result.status[0].type == "Monthly") {
                    setMSelected(true)
                }
                else {
                    setWSelected(true)
                }
                setDateText1(result.status[0].start_date);
                setDateText2(result.status[0].end_date);
                setPlanName(result.status[0].name)
                //setData(result?.status[0]);
                console.log('gg', data);
                result?.status[0].dealer_array.forEach(item => {
                    console.log("item", item)
                });
                setDealerArray(result?.status[0].dealer_array)
                //getting accounts
                loadAcccounts(result?.status[0].dealer_array)
            })
            .catch(error => console.log('api error', error));
    }
    useFocusEffect(
        React.useCallback(() => {
            getPlanDetails();

        }, [])
    );

    // const handleRadioButtonChange = (value) => {
    //   setRadioValue(value);

    //   if (value === 'monthly') {
    //     setDateText2(format(lastDayOfMonth(new Date()), 'yyyy-MM-dd'));
    //   } else if (value === 'weekly') {
    //     const currentDate = new Date();
    //     let fridayDate = currentDate;

    //     while (getDay(fridayDate) !== 5) {
    //       fridayDate = addDays(fridayDate, 1);
    //     }

    //     setDateText2(format(fridayDate, 'yyyy-MM-dd'));
    //   }
    // };
    // const [rSelected, setRSelected] = useState(false)
    // const handleRadioButtonChange = (value) => {
    //   setRadioValue(value);

    //   if (value === 'monthly') {
    //     setDateText2(format(lastDayOfMonth(new Date()), 'yyyy-MM-dd'));
    //   } else if (value === 'weekly') {
    //     const currentDate = new Date();
    //     let fridayDate = currentDate;

    //     while (getDay(fridayDate) !== 5) {
    //       fridayDate = addDays(fridayDate, 1);
    //     }

    //     setDateText2(format(fridayDate, 'yyyy-MM-dd'));
    //   }
    // };
    // new
    const width = Dimensions.get('window').width

    const data = [
        { label: 'Product presentation', value: '1' },
        { label: 'Product training', value: '2' },
        { label: 'Sales pitch', value: '3' },
        { label: 'Negotiation', value: '4' },
        { label: 'Follow-up', value: '5' },
        { label: 'Market analysis', value: '6' },
        { label: 'Relationship building', value: '7' },
        { label: 'Inventory management', value: '8' },
    ];

    const [value, setValue] = useState(null);
    const [isFocus, setIsFocus] = useState(false);
    const [date, setDate] = useState(new Date())
    const [enddate, setEndDate] = useState(new Date())
    // const [isSelected, setSelection] = useState(false);
    const [accountsarray, setAccountsArray] = useState([]);

    const dealerChoosen = useRef(false);


    //radrio val hooks 





    const loadAcccounts = async (dealer_array) => {
        console.log('ttt', dealer_array)
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "text/plain");

        var raw = JSON.stringify(
            {
                "__user_id__": token
            }
        )

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_accounts_for_dealer.php", requestOptions)
            .then(response => response.json())
            .then(async result => {

                // for (var i = 0; i < result.length; i++) {
                //   result[i].checked = false;
                //   //result.entry_list[i].visitcount = ""
                // }


                let tempArray = await result.map((item) => {
                    console.log("item og", item)
                    if (item.visit > 0) {  //checking if visiit is zero
                        let wVisit = 1;  //default weekly visit value
                        //checking company visit is > 4
                        if (item?.visit > 4) {
                            wVisit = Math.floor(item?.visit / 4) //integer division
                        }
                        let checked = false;
                        //auto ticking  user selected dealer option 
                        dealer_array.map((element) => {
                            if (item.id == element.account_id_c) {
                                console.log("selected account", item.id)
                                checked = true;
                            }
                        })
                        return {
                            ...item,
                            checked: checked,
                            monthlyVisit: item?.visit,
                            weeklyVisit: wVisit.toString()
                        }
                    }
                })
                setAccountsArray(tempArray);


                console.log('tmp Array', tempArray)

            })
            .catch(error => console.log('error', error));
    }



    // const updateVisitCount = (text, index) => {
    //   var myarray = [...accountsarray];
    //   myarray[index].visitcount = ""
    //   myarray[index].visitcount = text
    //   myarray[index].checked = true
    //   setAccountsArray(myarray);

    // }
    const handleCheckboxChange = (index) => {

        var myarray = [...accountsarray];
        myarray[index].checked = !accountsarray[index].checked;
        setAccountsArray(myarray);

    }
    const [startDate, setStartDate] = useState("")
    const [StopDate, setStopDate] = useState("")
    const handleDateChange = (value, index) => {
        if (index == 1)
            setStartDate(value);
        else
            setStopDate(value)
    }

    const [open, setOpen] = useState(false)
    const [stopopen, setStopOpen] = useState(false)
    // {renderLabel()}
    // <Dropdown
    //   style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
    //   placeholderStyle={styles.placeholderStyle}
    //   selectedTextStyle={styles.selectedTextStyle}
    //   inputSearchStyle={styles.inputSearchStyle}
    //   iconStyle={styles.iconStyle}
    //   data={data}
    //   search
    //   maxHeight={300}
    //   labelField="label"
    //   valueField="value"
    //   placeholder={!isFocus ? 'Choose Purpose' : '...'}
    //   searchPlaceholder="Search..."
    //   value={value}
    //   onFocus={() => setIsFocus(true)}
    //   onBlur={() => setIsFocus(false)}
    //   onChange={item => {
    //     setValue(item.value);
    //     setIsFocus(false);
    //   }}
    // />
    const renderLabel = () => {
        if (value || isFocus) {
            return (
                <Text style={[styles.label, isFocus && { color: 'blue' }]}>
                    Purpose of Visit
                </Text>
            );
        }
        return null;
    }

    const EditPlanPressed = () => {
        if (planName) {
            //console.log("dealer array",accountsarray)
            let dealerArray = []
            accountsarray.forEach((item) => {
                //push only if no of visit count > 0 
                if (item?.visit > 0 && item?.checked) {
                    dealerChoosen.current = true;
                    dealerArray.push({
                        __account_id__: item?.id,
                        __no_of_visit__: mSelected ? item?.monthlyVisit : item?.weeklyVisit
                    })
                }
                console.log("id", item.id)
                console.log("count", item.visit)
            })
            console.log("final dealer array", dealerArray)
            if (dealerChoosen.current) {
                var myHeaders = new Headers();
                myHeaders.append("Content-Type", "text/plain");

                let type = mSelected ? 'Monthly' : 'Weekly';

                var raw = JSON.stringify({
                    "__name__": planName,
                    "__start_date__": dateText1,
                    "__end_date__": dateText2,
                    "__user_id__": token,
                    "__dealer_array__": dealerArray,
                    "__type__": type,
                    "__tourplan_id__": id


                });

                console.log(raw)
                var requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: raw,
                    redirect: 'follow'
                };

                fetch("https://dev.ordo.primesophic.com/set_tour_plan.php", requestOptions)
                    .then(response => response.json())
                    .then(result => {
                        console.log(result.status);


                        Alert.alert('Edit Plan', 'Plan edited successfully', [
                            { text: 'OK', onPress: () => manager ? navigation.navigate('SalesManagerHome'):navigation.goBack() }
                        ])

                    })
                    .catch(error => console.log('error', error));

            }
            else {
                Alert.alert('Warning', 'Please choose atleast one dealer or Enter no of visit value')
            }
        }
        else {
            Alert.alert('Warning', 'Please enter all the details')
        }



    }
    const RenderItems = ({ item, index }) => (

        <View style={styles.flatliststyle}>

            <View style={{ flexDirection: "row", width: width - 10, alignSelf: 'center', height: 30, width: width, paddingLeft: 10, }} >
                <Image style={{ width: 20, height: 20, marginTop: 5 }} source={{ uri: `https://dev.ordo.primesophic.com/upload/${item?.id}_img_src_c` }}></Image>
                <Text style={{ paddingLeft: 10, color: '#000', borderBottomColor: '#7A7F85', justifyContent: 'center', textAlign: 'left', fontSize: 12, height: 30, textAlignVertical: 'center', width: (width - 10) / 2, alignSelf: 'center', marginTop: 10, fontFamily: 'Poppins-SemiBold' }}>{item?.name}</Text>

                <TextInput
                    style={{ fontSize: 12, width: 50, color: 'black', padding: 5, fontFamily: 'Poppins-Regular' }}
                    value={mSelected ? item?.monthlyVisit : item?.weeklyVisit}
                    editable={false}
                />
                <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 10 }}>
                    <Pressable
                        style={{
                            color: "#F5904B",
                            width: 24,
                            height: 24,
                            borderRadius: 4,
                            borderWidth: 2,
                            borderColor: 'grey',
                            alignItems: 'center',
                            justifyContent: 'center',
                            marginRight: 8,
                            backgroundColor: item.checked ? 'white' : 'transparent',
                        }}
                        onPress={() => handleCheckboxChange(index)}
                    >
                        {item.checked && <Text style={{ color: '#F5904B', fontSize: 14, fontWeight: 'bold' }}>✓</Text>}
                    </Pressable>
                </View>
            </View>

        </View>

    )

    const InputWithLabel = ({ title, value, onPress }) => {
        return (
            <View>
                <Text style={styles.labelText}>{title}</Text>
                {/* <Pressable style={{ ...styles.inputContainer }} onPress={onPress} >
          <Text style={styles.input2}>{value ? value : 'Select date'}</Text>
          <Image style={{ width: 20, height: 20, marginRight: 15 }} source={require("../../assets/images/calendar.png")}></Image>
        </Pressable> */}
            </View>
        )

    }

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
            {/* <View style={{ flex: 0.1, backgroundColor: 'white', alignContent: 'center', justifyContent: 'center', width: width, flexDirection: 'row', height: 30 }}>
        <TouchableOpacity style={{ width: 30, height: 30, marginTop: 20,marginLeft:10 }} onPress={() => {navigation.goBack() }}>
          <Image source={require('../../assets/images/arrow-left.png')} style={{ width: 30, height: 30, resizeMode: 'contain' }}></Image>
        </TouchableOpacity>
        <Text style={{ alignSelf: 'center', fontSize: 20, fontWeight: "500", width: width - 40, marginHorizontal: 0, height: 30, textAlign: 'center',fontFamily:'Poppins-SemiBold' ,color:'#000'}}>Create Plan</Text>
      </View> */}
            <View style={{ ...styles.headercontainer }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <AntDesign name='arrowleft' size={25} color={Colors.black} />
                </TouchableOpacity>
                <Text style={styles.headerTitle}>Edit Plan</Text>
            </View>
            <View style={{ paddingHorizontal: 16 }}>
                <View>
                    <Text style={styles.labelText}>Start Plan</Text>
                    <View style={styles.inputContainer} >
                        <TextInput
                            style={styles.input2}
                            placeholder={`Enter plan name`}
                            onChangeText={(val) => setPlanName(val)}
                            value={planName}


                        />
                    </View>
                </View>

                <View>
                    <Text style={styles.labelText}>Start Date</Text>
                    <View style={styles.inputContainer} >
                        <Text style={styles.input2}>{moment(dateText1).format('DD-MM-YY')}</Text>
                    </View>
                </View>

                <View>
                    <Text style={styles.labelText}>End Date</Text>
                    <View style={styles.inputContainer} >
                        <Text style={styles.input2}>{moment(dateText2).format('DD-MM-YY')}</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                    {/* <RadioButton.Group
            onValueChange={handleRadioButtonChange}
            value={radioValue}
          >
            <View style={{flexDirection:'row'}}>
              <RadioButton.Item label="Monthly" value="monthly" />
              <RadioButton.Item label="Weekly" value="weekly" />
            </View>
          </RadioButton.Group> */}
                    <View style={{ flexDirection: 'row', }}>
                        <RadioButton
                            animation={'bounceIn'}
                            isSelected={mSelected}
                            size={14}

                            innerColor={Colors.primary}
                            outerColor={Colors.primary}
                            onPress={() => {
                                setMSelected(true)
                                setWSelected(false)
                                setDateText2(format(lastDayOfMonth(new Date()), 'yyyy-MM-dd'));
                            }}
                        />
                        <Text style={{ ...styles.labelText, marginLeft: 5, fontSize: 14 }}>Monthly</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginLeft: 10 }}>
                        <RadioButton
                            editable={false}

                            animation={'bounceIn'}
                            size={14}
                            isSelected={wSelected}
                            innerColor={Colors.primary}
                            outerColor={Colors.primary}
                            onPress={() => {
                                setMSelected(false)
                                setWSelected(true)
                                const currentDate = new Date();
                                let fridayDate = currentDate;

                                while (getDay(fridayDate) !== 5) {
                                    fridayDate = addDays(fridayDate, 1);
                                }

                                setDateText2(format(fridayDate, 'yyyy-MM-dd'));
                            }}
                        />
                        <Text style={{ ...styles.labelText, marginLeft: 5, fontSize: 14 }}>Weekly</Text>
                    </View>
                </View>
                {/* <InputWithLabel
          title='Start Date'
          value={currentDate} 
          // value={startDate}
          // onPress={() => setOpen(true)}
        />
        <InputWithLabel
          title='End Date'
          value={lastDateOfMonth} 
          // value={StopDate}
          // onPress={() => setStopOpen(true)}
        /> */}

            </View>
            <View style={styles.container}>


                {/* <FloatingLabelInput
          label="Plan Name"
          value={planName}
          onChangeText={setPlanName}
        /> */}


                {/* <View style={styles.inputView}>
          <Text style={{ width: width - 50, flex: 0.9 }}>{startDate}</Text>
          <TouchableOpacity onPress={() => setOpen(true)}><Image style={{ width: 20, height: 20 }} source={require("../../assets/images/calendar.png")}></Image></TouchableOpacity> */}
                {open == true ?
                    <DatePicker
                        modal
                        mode={'date'}
                        open={open}
                        date={date}
                        format="YYYY-MM-DD"
                        minDate="2022-01-01"
                        maxDate="2200-12-31"
                        onConfirm={(date) => {
                            const dateString = date.toLocaleDateString();
                            console.log(dateString);
                            setOpen(false)
                            setDate(date)
                            handleDateChange(dateString, 1)
                        }}
                        onCancel={() => {
                            setOpen(false)
                        }}
                    /> : null}
                {/* </View>
        <View style={styles.inputView}>
          <Text style={{ width: width - 50, flex: 0.9 }}>{StopDate}</Text>
          <TouchableOpacity onPress={() => setStopOpen(true)}><Image style={{ width: 20, height: 20 }} source={require("../../assets/images/calendar.png")}></Image></TouchableOpacity> */}
                {stopopen == true ?
                    <DatePicker
                        modal
                        mode={'date'}
                        open={stopopen}
                        date={enddate}
                        format="YYYY-MM-DD"
                        minDate="2022-01-01"
                        maxDate="2200-12-31"
                        onConfirm={(date) => {
                            const dateString = date.toLocaleDateString();
                            console.log(dateString);
                            setStopOpen(false)
                            setEndDate(date)
                            handleDateChange(dateString, 2)
                        }}
                        onCancel={() => {
                            setStopOpen(false)
                        }}
                    /> : null}
                {/* </View> */}

                <Text style={{ paddingLeft: 0, fontWeight: '500', marginTop: 10, color: '#000', fontFamily: 'Poppins-SemiBold' }}>Choose your dealer from the list</Text>
                {/* <ScrollView style={{ backgroundColor: '#FFFFFF', marginTop: 10 }}
          scrollEnabled={false}
        > */}
                <View style={{ width: width }}>
                    <View style={{ flexDirection: "row", backgroundColor: '#F1F2F1', height: 30, borderColor: 'grey', borderBottomWidth: 1, width: width - 20, alignItems: 'center' }} >
                        <View style={{ flex: 0.5 }}>
                            <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12, textAlign: 'center' }}>Dealer Name</Text>
                        </View>
                        <View style={{ flex: 0.5 }}>
                            <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12, paddingLeft: 32 }}>Freq.</Text>
                        </View>
                    </View>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={accountsarray}
                        extraData={true}
                        style={{ height: 150 }}
                        renderItem={RenderItems}

                    />
                </View>
                <View style={{ flex: 1, justifyContent: 'flex-end', marginBottom: 20 }}>
                    <TouchableOpacity style={styles.button} onPress={EditPlanPressed} activeOpacity={0.8}>
                        <Text style={styles.btnText}>Edit Plan</Text>
                    </TouchableOpacity>
                </View>


            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
        paddingHorizontal: 16
    },
    dropdown: {
        height: 50,
        borderColor: 'gray',
        borderWidth: 0.5,
        borderRadius: 8,
        paddingHorizontal: 8,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        position: 'absolute',
        backgroundColor: 'white',
        left: 22,
        top: 8,
        zIndex: 999,
        paddingHorizontal: 8,
        fontSize: 14,
        fontFamily: 'Poppins-Regular'
    },
    inputView: {
        width: "100%",
        backgroundColor: "#ffffff",
        height: 40,
        borderWidth: 0.5,
        borderColor: 'grey',
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center',
        marginTop: 5

    },
    textinputView: {
        width: "20%",
        //backgroundColor: "red",
        height: 20,
        borderWidth: 0.5,
        borderColor: 'grey',
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center',
        marginTop: 5

    },
    planinputView: {
        width: "100%",
        backgroundColor: "#ffffff",
        height: 40,
        borderWidth: 0.5,
        borderColor: 'grey',
        flexDirection: 'row',
        justifyContent: "flex-start",
        marginTop: 5

    },
    placeholderStyle: {
        fontSize: 16,
    },
    selectedTextStyle: {
        fontSize: 16,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 16,
    },

    headercontainer: {
        paddingHorizontal: 16,
        paddingVertical: 5,
        //backgroundColor:'red',
        flexDirection: 'row',
        alignItems: 'center',
        //backgroundColor:'red'

    },
    headerTitle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: Colors.black,
        marginLeft: 10,
        marginTop: 3
    },
    labelText: {
        fontFamily: 'Poppins-SemiBold',
        color: Colors.black,
        fontSize: 16,
        marginTop: 5,
    },


    input2: {
        fontFamily: 'Poppins-Regular',
        padding: 8,
        flex: 1,
        color: Colors.black

    },
    inputContainer: {
        marginVertical: 3,
        borderColor: 'grey',
        color: 'black',
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    input: {
        marginVertical: 3,
        padding: 8,
        borderColor: 'grey',
        color: 'black',
        borderWidth: 1,
        fontFamily: 'Poppins-Regular'
    },
    dateContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginVertical: 3,
        padding: 10,
        borderColor: 'grey',
        borderWidth: 1,
    },
    dateText: {
        color: 'black',
        fontFamily: 'Poppins-Regular'
    },
    button: {
        height: 50,
        //marginTop: 20,
        // backgroundColor:'red',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        backgroundColor: Colors.primary,
    },
    btnText: {
        fontFamily: 'Poppins-SemiBold',
        color: '#fff',
        fontSize: 16
    }

});

export default EditPlan;