import { StyleSheet, Text, View, TouchableOpacity, Dimensions, FlatList, Image, TextInput, ScrollView } from 'react-native'
import React, { useContext, useState, useEffect } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../constants/Colors';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useFocusEffect } from '@react-navigation/native';
import { AuthContext } from '../../Context/AuthContext';
import moment from 'moment';
const PlanDetails = ({ navigation, route }) => {
    // new
    const width = Dimensions.get('window').width
    const [dealerArray, setDealerArray] = useState([]);
    const [externalVisit, setExternalVisit] = useState([]);

    const flData = [
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Walmart',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Fantasy Light Bookstore',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        }, {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'E G India',
            frequencyNumber: 1,
            visitedNumber: 1
        },
        {
            image: 'https://images.unsplash.com/photo-1503023345310-bd7c1de61c7d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8aHVtYW58ZW58MHx8MHx8fDA%3D&w=1000&q=80',
            name: 'Silicon Electronics',
            frequencyNumber: 1,
            visitedNumber: 1
        },
    ]
    // new
    const { item } = route.params;
    const [btnTitle, setBtnTitle] = useState('Start Tour')
    useEffect(() => {
        checkPlanStarted();
    })

    const checkPlanStarted = async () => {
        console.log("use effect item id ", item?.id)
        let res = await AsyncStorage.getItem(item?.id)
        console.log("res", res)
        if (res) {
            console.log("resume ")
            setBtnTitle("My Todays")
        }
        else {
            console.log("start")

        }

    }

    const [data, setData] = useState('');
    const { token } = useContext(AuthContext);

    const color = item.status == 'Approved' ? 'green' : item.status == 'PendingApproval' ? 'orange' : 'red';


    console.log("item", item)
    const getPlanDetails = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__user_id__": token,
            "__id__": item?.id
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_tour_plan_detail.php", requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log('plan details data 123 ', result)
                setData(result?.status[0]);
                console.log('gg', data);
                //dealer array
                result?.status[0].dealer_array.forEach(item => {
                    console.log("item", item)
                });
                setDealerArray(result?.status[0].dealer_array);
                //external visit
                result?.status[0].external_visits_array.forEach(item => {
                    console.log("external visit item", item)
                });
                
                if (Array.isArray(result?.status[0].external_visits_array) && result?.status[0].external_visits_array.length > 0) {
                    console.log("setting")
                    setExternalVisit(result?.status[0].external_visits_array)
                }

            })
            .catch(error => console.log('api error', error));
    }
    useFocusEffect(
        React.useCallback(() => {
            getPlanDetails();

        }, [])
    );
    return (

        <View style={styles.container}>
            <View style={styles.rowContainer}>
                <View style={{ ...styles.headercontainer }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <AntDesign name='arrowleft' size={25} color={Colors.black} />
                    </TouchableOpacity>
                    <Text style={styles.headerTitle}>Plan Details</Text>
                </View>
                {(item.status == 'Approved' || item.status == 'Completed') && <TouchableOpacity onPress={() => navigation.navigate('Report', { item: item })}>
                    <Image source={require('../../assets/images/report.png')} style={{ height: 20, width: 20 }} />
                </TouchableOpacity>}
            </View>
            <View style={{ ...styles.contentContainer, flexDirection: 'row' }}>
                <View style={{ flex: 1 }}>
                    <Text style={{ ...styles.planText }}>Plan Name  </Text>
                    <Text style={{ ...styles.value, color: Colors.primary, fontFamily: 'Poppins-SemiBold', textTransform: 'capitalize' }} >{data.name}</Text>
                </View>
                {/* <View style={{ flex: 0.8, marginLeft: 10 }}>
                    <Text style={styles.planText}>No. of Dealers  </Text>
                    <Text style={styles.value}>#{data?.dealer_count}</Text>
                </View> */}
            </View>
            <View style={{ ...styles.contentContainer, flexDirection: 'row' }}>
                <View style={{ flex: 1, }}>
                    <Text style={{ ...styles.planText }}>Created Date  </Text>
                    <Text style={styles.value} >{moment(data?.date_entered).format('DD-MM-YY')}</Text>
                </View>
                <View style={{ flex: 0.8, marginLeft: 10 }}>
                    <Text style={styles.planText}>No. of Dealers  </Text>
                    <Text style={styles.value}>#{data?.dealer_count}</Text>
                </View>
            </View>
            <View style={{ ...styles.contentContainer, flexDirection: 'row' }}>
                <View style={{ flex: 1, }}>
                    <Text style={{ ...styles.planText }}>Region  </Text>
                    <Text style={styles.value} >{data?.region}</Text>
                </View>
                <View style={{ flex: 0.8, marginLeft: 10 }}>
                    <Text style={styles.planText}>Duration  </Text>
                    <Text style={styles.value}>{moment(data.start_date).format('DD-MM-YY')} To {moment(data.end_date).format('DD-MM-YY')}  </Text>
                </View>
            </View>
            {/* <View>
                <Text style={styles.planText}>Duration  </Text>
                <Text style={styles.value}>{moment(item.start_date).format('DD-MM-YY')} To {moment(item.end_date).format('DD-MM-YY')}  </Text>
            </View> */}
            {(item.status == 'Approved' || item.status == 'Completed') && <View style={{ ...styles.contentContainer, flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>


                <View style={{ flex: 1, }}>
                    <Text style={styles.planText}>Approved By</Text>
                    <Text style={styles.value}>{data?.approved_by}</Text>
                </View>
                <View style={{ flex: 0.8, marginLeft: 10 }}>
                    <Text style={styles.planText}>Approved Date  </Text>
                    <Text style={styles.value} >{moment(data?.approved_date).format('DD-MM-YY')}</Text>
                </View>

            </View>}
            {/* {item.status == 'Approved' && <View style={styles.contentContainer}>
                    <Text style={styles.planText}>Approved By : </Text>
                    <Text style={styles.value}>{data.approved_by}</Text>
                </View>}
                <View style={styles.contentContainer}>
                    <Text style={styles.planText}>Number of Visit: </Text>
                    <Text style={styles.value}>{data.dealer_count}</Text>
                </View> */}
            {/* <View style={styles.contentContainer}>
                    <Text style={styles.planText}>Start Date : </Text>
                    <Text style={styles.value}>{data?.start_date}</Text>
                </View>
                <View style={styles.contentContainer}>
                    <Text style={styles.planText}>End Date : </Text>
                    <Text style={styles.value}>{data?.end_date}.</Text>
                </View> */}


            {/* new */}
            {dealerArray.length > 0 && <Text style={styles.planText}>Client List
            </Text>}
            {dealerArray.length > 0 && <View>
                <View style={{ flexDirection: "row", backgroundColor: '#F1F2F1', borderColor: 'grey', borderBottomWidth: 1, alignItems: 'center', justifyContent: 'center' }} >
                    <View style={{ flex: 2.4 }}>
                        <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12, textAlign: 'center' }}>Dealer Name</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12 }}>Freq.</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12 }}>    Visits Completed</Text>
                    </View>
                </View>

                {/* {dealerArray.length > 0 && dealerArray.map(item => {
                        return (
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 2.4 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={{ uri: item?.account_profile_pic }} style={{ height: 20, width: 20, marginVertical: 5, marginLeft: 10 }} />
                                        <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 12, color: '#000', marginLeft: 10 }}> {item.name}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.no_of_visit}</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.completed_visit}</Text>
                                </View>
                            </View>
                        )
                    })
                    } */}
                <View style={{ height: 100 }}>
                    <FlatList
                        data={dealerArray}
                        renderItem={({ item }) => (
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 2.4 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={{ uri: item?.account_profile_pic }} style={{ height: 20, width: 20, marginVertical: 5, marginLeft: 10 }} />
                                        <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 12, color: '#000', marginLeft: 10 }}> {item.name}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.no_of_visit}</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.completed_visit}</Text>
                                </View>
                            </View>
                        )}
                    />
                </View>

            </View>}


            {/* External Visits */}
            {externalVisit.length > 0 && <Text style={styles.planText}>External Visits
            </Text>}
            {externalVisit.length > 0 && <View>
                <View style={{ flexDirection: "row", backgroundColor: '#F1F2F1', borderColor: 'grey', borderBottomWidth: 1, alignItems: 'center', justifyContent: 'center', paddingVertical: 5 }} >
                    <View style={{ flex: 2.4 }}>
                        <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12, textAlign: 'center' }}>Dealer Name</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12 }}>Visits</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        {/* <Text style={{ fontFamily: 'Poppins-SemiBold', color: '#000', fontSize: 12 }}>    Visits Completed</Text> */}
                    </View>
                </View>

                {/* {dealerArray.length > 0 && dealerArray.map(item => {
                        return (
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 2.4 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={{ uri: item?.account_profile_pic }} style={{ height: 20, width: 20, marginVertical: 5, marginLeft: 10 }} />
                                        <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 12, color: '#000', marginLeft: 10 }}> {item.name}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.no_of_visit}</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.completed_visit}</Text>
                                </View>
                            </View>
                        )
                    })
                    } */}
                <View style={{ height: 150 }}>
                    <FlatList
                        data={externalVisit}
                        renderItem={({ item }) => (
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 2.4 }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Image source={{ uri: item?.account_profile_pic }} style={{ height: 20, width: 20, marginVertical: 5, marginLeft: 10 }} />
                                        <Text style={{ fontFamily: 'Poppins-SemiBold', fontSize: 12, color: '#000', marginLeft: 10 }}> {item.name}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.count}</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'center' }}>
                                    {/* <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: '#000' }}>{item.completed_visit}</Text> */}
                                </View>
                            </View>
                        )}
                    />
                </View>

            </View>}


            {/* new */}
            {item.status == 'Approved' && <View style={styles.startPlanView}>
                <TouchableOpacity
                    onPress={async () => {

                        //await AsyncStorage.setItem('activePlan', JSON.stringify(data.dealer_array))
                        console.log("user clicked plan id", item?.id);

                        await AsyncStorage.setItem(item.id, item.id);
                        console.log("stored user pland id complete");
                        let res = await AsyncStorage.getItem(item.id);
                        console.log("get tour plan id #$%", res)



                        navigation.navigate('Map', { tour_plan_id: item?.id, planName: item.name })
                    }}
                    style={styles.loginBtn}>
                    <Text style={styles.loginText}>{btnTitle} Plan</Text>
                </TouchableOpacity>
            </View>}
        </View>

    )
}

export default PlanDetails

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 12,
        backgroundColor: '#fff'
    },
    contentContainer: {
        //marginLeft: 10,
        marginTop: 5
    },
    planText: {
        color: 'black',
        fontSize: 16,
        ////ontWeight: '600',
        fontFamily: 'Poppins-SemiBold',
    },
    loginText: {
        color: 'white',
        fontFamily: 'Poppins-SemiBold',
    },
    loginBtn: {
        width: "80%",
        backgroundColor: "#011A90",
        borderRadius: 30,
        height: 60,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 5,
        marginBottom: 10
    },
    startPlanView: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 20
    },
    headercontainer: {
        //padding: 10,
        paddingTop: 6,
        //backgroundColor:'red',
        flexDirection: 'row',
        alignItems: 'center',


    },
    headerTitle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: Colors.black,
        marginLeft: 10,
        marginTop: 3
    },
    value: {
        fontFamily: 'Poppins-Regular',
        color: 'grey'

    },
    rowContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
})