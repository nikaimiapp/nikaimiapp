import { StyleSheet, Text, View, ScrollView, TouchableOpacity, Alert, Image, Modal, TextInput, PermissionsAndroid, FlatList } from 'react-native'
import React, { useState, useContext, useEffect } from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from '../../constants/Colors';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import { cameraPermission } from '../../utils/Helper';
import { AuthContext } from '../../Context/AuthContext';
import ImageResizer from '@bam.tech/react-native-image-resizer'
import RNFS from 'react-native-fs';
const CompetitorIntelligence = ({ navigation, route }) => {

    const { item } = route.params;
    //console.log("dealer data", dealerData);
    console.log("product data", item);

    const { token, dealerData } = useContext(AuthContext);

    const [isModalVisible, setModalVisible] = useState(false);
    const [cmpdata, setCmpData] = useState([]);
    const [modelNo, setModelNo] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState('');
    const [packs, setPacks] = useState('');
    const [desc, setDesc] = useState('')
    const [base64img, setBase64img] = useState('');

    const clearModalValue = () => {
        setModelNo('');
        setPrice('');
        setQuantity('');
        setPacks('');
        setDesc('');
        setBase64img('');
    }

    // const getProductCmpRecordId = async () => {
    //     console.log("loading cmp product");
    //     var myHeaders = new Headers();
    //     myHeaders.append("Content-Type", "application/json");

    //     var raw = JSON.stringify({
    //         "__userid__": token,
    //         "__account_id__": dealerData?.account_id_c,

    //     });

    //     var requestOptions = {
    //         method: 'POST',
    //         headers: myHeaders,
    //         body: raw,
    //         redirect: 'follow'
    //     };

    //     fetch("https://dev.ordo.primesophic.com/get_competitor_analysis_product_id.php", requestOptions)
    //         .then(response => response.json())
    //         .then(result => {
    //             console.log("get cmp record id res", result);
    //             result.forEach(itm => {
    //                 if (itm.product_id == item?.id) {
    //                     loadCmpProduct(itm.id);
    //                     return;
    //                 }

    //             });

    //         })
    //         .catch(error => console.log('error', error));
    // }

    const loadCmpProduct = async (id) => {

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "__userid__": token,
            "__account_id__": dealerData?.id,
            "__products_id__": item.id,
        });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        fetch("https://dev.ordo.primesophic.com/get_competitor_analysis.php", requestOptions)
            .then(response => response.json())
            .then(result => {
                console.log("product cmp res", result);
                setCmpData(result)
            })
            .catch(error => console.log('error', error));

    }

    useEffect(() => {
        loadCmpProduct();
    }, [])

    const handleAdd = () => {
        if (modelNo && price && quantity && packs && base64img && desc) {
            setModalVisible(false);
            var myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            var raw = JSON.stringify({
                // "__module_code__": "PO_35",
                // "__query__": "",
                "__name__": modelNo,
                "__price__": price,
                "__ordo_user_id__": token,
                "__pack_of__": packs,
                "__quantity_sold__": quantity,
                "__remarks__": desc,
                "__products_id__": item.id,
                "__account_id__": dealerData?.id,
                "__product_image__": base64img,
                "__product_image__": item?.imgsrc,
                "__product_base64__": base64img,
                "__ordousers_id__": token
            });



            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch("https://dev.ordo.primesophic.com/set_competitor_analysis.php", requestOptions)
                .then(response => response.json())
                .then(result => {
                    console.log('add ompetetor item  res', result);
                    const newItem = {
                        id: modelNo,
                        model: modelNo,
                        price: price,
                        quantity_sold: quantity,
                        pack_of: packs,
                        remarks: desc,
                        image_url: base64img
                    };
                    setCmpData(prevData => [...prevData, newItem]);
                    clearModalValue();

                })
                .catch(error => console.log('error', error));


        }
        else {
            Alert.alert('Error', 'Please fill all the details')
        }



    };

    const checkPermission = async () => {
        let PermissionDenied = await cameraPermission();
        if (PermissionDenied) {
            console.log("camera permssion granted");
            handleCamera();
        }
        else {
            console.log("camera permssion denied");
            //requestStoragePermission();
        }
    }

    const handleCamera = async () => {
        // setModalVisible1(false);
        const res = await launchCamera({
            mediaType: 'photo',
        });
        console.log("response", res.assets[0].uri);
        imageResize(res.assets[0].uri, res.assets[0].type);
    }
    const handleGallery = async () => {
        // setModalVisible1(false);
        const res = await launchImageLibrary({
            mediaType: 'photo',

        });
        console.log("response", res.assets[0].uri);
        imageResize(res.assets[0].uri, res.assets[0].type);
    }

    const imageResize = async (img, type) => {
        ImageResizer.createResizedImage(
            img,
            300,
            300,
            'JPEG',
            50,

        )
            .then(async (res) => {
                console.log('image resize', res);
                RNFS.readFile(res.path, 'base64')
                    .then(res => {
                        console.log('base64', res);
                        setBase64img(`data:${type};base64,${res}`);
                    });
            })
            .catch((err) => {
                console.log(" img resize error", err)
            });
    }
    return (
        <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>

            <View style={{ ...styles.headercontainer }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <AntDesign name='arrowleft' size={25} color={Colors.black} />
                </TouchableOpacity>
                <Text style={styles.headerTitle}>Competitor Analysis</Text>
            </View>

            <View style={styles.container}>
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 0.4, flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ flex: 0.4, justifyContent: 'center', alignItems: 'center' }}>
                            {/* <Image source={ {uri:productData[0].product_image}}/> */}
                            <Image source={{ uri: item.imgsrc }} style={{ width: 60, height: 80, resizeMode: 'cover' }} />
                        </View>

                        <View style={{ flex: 0.8 }}>
                            <Text style={{ ...styles.text, fontFamily: 'Poppins-SemiBold', color: Colors.primary }}>{item.description}</Text>
                            <Text style={styles.text}>#{item.itemid}</Text>
                            <Text style={styles.text}>{item.manufacturer}</Text>
                            <Text style={styles.text}>AED  {Number(item.price)}</Text>
                            {/* <Text style={styles.text}>SKU Price : 4567</Text> */}
                        </View>
                    </View>

                    <View style={styles.checkOutView}>
                        <TouchableOpacity style={styles.createPlanBtn}
                            onPress={() => setModalVisible(true)}
                        >
                            <Text style={styles.buttonTextStyle}>Add Competitor</Text>
                        </TouchableOpacity>
                    </View>
                    {cmpdata.length > 0 && <Text style={{
                        fontSize: 16,
                        fontFamily: 'Poppins-SemiBold',
                        color: Colors.black,
                    }}>Competitor Items</Text>}
                    <FlatList
                        data={cmpdata}
                        keyExtractor={item => item.id}
                        renderItem={({ item }) => {
                            //repalcing ' and " values 
                            let sQuote = item.remarks.replaceAll('&#039;', `'`)
                            let dQuote = sQuote.replaceAll('&quot;', `"`)

                            return (
                                // <View>
                                //   <Text>{item.modalNo}</Text>
                                //   <Text>{item.price}</Text>
                                //   <Text>{item.quantity}</Text>
                                // </View>

                                <View style={{
                                    flex: 1, backgroundColor: "white",
                                    margin: 5,
                                    flexDirection: 'row',
                                    marginBottom: 16,
                                    borderRadius: 8,
                                    elevation: 5,
                                    padding: 8
                                }}>

                                    <View style={{ flex: 0.4, justifyContent: 'center', alignItems: 'center' }}>
                                        {/* <Image source={ {uri:productData[0].product_image}}/> */}
                                        <Image source={{ uri: item.image_url }} style={{ width: 60, height: 80, resizeMode: 'cover' }} />
                                    </View>

                                    <View style={{ flex: 0.8 }}>

                                        <Text style={{ ...styles.text, fontFamily: 'Poppins-SemiBold', color: Colors.primary }}>{item.model}</Text>
                                        <Text style={styles.text}>#AED  {Number(item.price)}</Text>
                                        {/* <Text style={styles.text}>Packs : {item.pack_of}</Text> */}
                                        <Text style={styles.text}>Qty Sold : {item.quantity_sold}</Text>
                                        <Text style={{ ...styles.text, color: 'grey', fontFamily: 'Poppins-Italic' }}>{dQuote}</Text>

                                        {/* <Text style={styles.text}>Name : {item.description}</Text>
                                    <Text style={styles.text}>manufacturer : {item.manufacturer}</Text> */}

                                        {/* <Text style={styles.text}>SKU Price : 4567</Text> */}
                                    </View>
                                </View>

                            )
                        }

                        }
                    />


                    <Modal
                        visible={isModalVisible}
                        animationType="slide"
                        transparent={true}
                    >
                        {/* Modal content */}
                        <ScrollView style={{ flex: 1, marginTop: 10 }}>
                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 10, }}>
                                <View style={{ backgroundColor: 'white', padding: 20, width: '90%', marginHorizontal: 10, borderRadius: 10, borderColor: 'black', borderWidth: 1 }}>
                                    {/* new */}
                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
                                        <Text style={{ ...styles.modalTitle, color: Colors.primary }}>Competitor Details</Text>
                                        <TouchableOpacity onPress={() => {
                                            setModalVisible(false);
                                            clearModalValue();
                                        }}>
                                            <AntDesign name='close' size={20} color={`black`} />
                                        </TouchableOpacity>

                                    </View>

                                    <Text style={styles.modalTitle}>Company/Product</Text>
                                    <TextInput style={styles.cNameTextInput} placeholder='Enter Company/Product'
                                    />
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={styles.modalTitle}>Model/SKU#</Text>
                                            <TextInput style={styles.cNameTextInput} placeholder='Enter Model No' value={modelNo}
                                                onChangeText={text => setModelNo(text)} />
                                        </View>

                                        <View style={{ flex: 1, marginLeft: 5 }}>
                                            {/* <Text style={styles.modalTitle}>Price</Text>
                                            <TextInput style={styles.cNameTextInput} placeholder='Enter Price' value={price}
                                                onChangeText={text => setPrice(text)} keyboardType='numeric' /> */}
                                            <Text style={styles.modalTitle}>Pack Size</Text>
                                            <TextInput style={styles.cNameTextInput} placeholder='Enter packs' value={packs}
                                                onChangeText={text => setPacks(text)} keyboardType='numeric' />
                                        </View>
                                    </View>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ flex: 1 }}>
                                            <Text style={styles.modalTitle}>Qty Sold/Month</Text>
                                            <TextInput style={styles.cNameTextInput} placeholder='Enter quantity' value={quantity}
                                                onChangeText={text => setQuantity(text)} keyboardType='numeric' />
                                        </View>
                                        <View style={{ flex: 1, marginLeft: 5 }}>
                                            <Text style={styles.modalTitle}>Price</Text>
                                            <TextInput style={styles.cNameTextInput} placeholder='Enter Price' value={price}
                                                onChangeText={text => setPrice(text)} keyboardType='numeric' />
                                            {/* new */}
                                        </View>
                                    </View>

                                    <Text style={styles.modalTitle}>Remarks</Text>
                                    <TextInput
                                        multiline={true}
                                        numberOfLines={5}
                                        placeholder="Enter Text..."
                                        style={styles.textarea}
                                        onChangeText={(val) => { setDesc(val) }}
                                        //onChangeText={(text) => this.setState({ text })}
                                        value={desc}
                                    />
                                    {/* <Text style={styles.modalTitle}>Remarks</Text>
                <TextInput
                  multiline={true}
                  numberOfLines={8}
                  placeholder="Enter Text..."
                  style={styles.textarea}
                  onChangeText={(val) => { setDesc(val) }}
                  //onChangeText={(text) => this.setState({ text })}
                  value={desc}
                />
                 */}


                                    <Text style={styles.modalTitle}>Upload Photo</Text>
                                    <View style={styles.buttonview}>
                                        <TouchableOpacity style={styles.photosContainer} onPress={checkPermission}>
                                            <Text style={styles.buttonText}>Camera</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity style={styles.photosContainer} onPress={handleGallery}>
                                            <Text style={styles.buttonText}>Gallery</Text>
                                        </TouchableOpacity>
                                        {/* <View style={styles.closeView}>
                        <Button title="Close" onPress={() => setModalVisible1(false)} />
                      </View> */}
                                    </View>
                                    {base64img && <Image source={{ uri: base64img }} style={{ width: 90, height: 90, resizeMode: 'cover', borderRadius: 8 }} />}

                                    <View style={styles.buttonview}>
                                        <TouchableOpacity style={styles.buttonContainer} onPress={handleAdd} >
                                            <Text style={styles.buttonText}>Submit</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </ScrollView>
                    </Modal>
                </View>
            </View>

        </ScrollView>
    )
}

export default CompetitorIntelligence

const styles = StyleSheet.create({
    headercontainer: {
        padding: 10,
        //backgroundColor:'red',
        flexDirection: 'row',
        alignItems: 'center',
    },
    headerTitle: {
        fontSize: 18,
        fontFamily: 'Poppins-SemiBold',
        color: Colors.black,
        marginLeft: 10,
        marginTop: 3
    },
    container: {
        backgroundColor: 'white',
        padding: 16,
        flex: 0.9
    },
    text: {
        fontFamily: 'Poppins-Regular',
    },
    checkOutView: {
        marginTop: 20,
        alignSelf: 'flex-start',
        marginBottom: 20
    },
    createPlanBtn: {
        height: 40,
        //width:40,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.primary,
        borderRadius: 10
    },
    buttonTextStyle: {
        color: '#fff',
        fontFamily: 'Poppins-SemiBold',
    },
    modalTitle: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'Poppins-SemiBold'
    },
    cNameTextInput: {
        height: 40,
        borderWidth: 1,
        borderColor: '#B3B6B7',
        padding: 5,
        fontFamily: 'Poppins-Regular',
        marginBottom: 10
    },
    buttonview: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    buttonContainer: {
        height: 40,
        padding: 10,
        borderRadius: 10,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //marginRight: 10,
        marginVertical: 10,
        backgroundColor: Colors.primary
    },
    photosContainer: {
        height: 40,
        padding: 10,
        borderRadius: 10,
        marginVertical: 10,
        backgroundColor: Colors.primary,
        marginRight: 10
    },
    buttonText: {
        fontFamily: 'Poppins-Regular',
        color: 'white'
    },
    textarea: {
        borderWidth: 0.5,
        borderColor: 'black',
        marginBottom: 10,
        borderRadius: 5,
        padding: 10,
        //fontSize: 13,
        textAlignVertical: 'top',
        color: '#000',
        fontFamily: 'Poppins-Regular',


    },
    text: {
        fontFamily: 'Poppins-Regular',
    },
})