import { StyleSheet, Text, View, BackHandler, Alert, TouchableOpacity, Image, Dimensions, Modal, TextInput } from 'react-native'
import React, { useState, useContext, useEffect } from 'react'
import Colors from '../../constants/Colors'
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Dropdown } from 'react-native-element-dropdown';
import { AuthContext } from '../../Context/AuthContext';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
const CheckIn = ({ navigation, route }) => {



  const backAction = () => {
    //checking screen is focused
    if (!navigation.isFocused()) {
      return false;
    }
    Alert.alert('Hold on!', 'Are you sure you want to go back?\nIf you leave before checkout, your data will be lost.', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      { text: 'YES', onPress: () => navigation.navigate('BottomTab') },
    ]);
    return true;
  };

  //disable back button logic
  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );
    return () => backHandler.remove();
  }, []);

  const { width, height } = Dimensions.get('screen');
  const [isModalVisible, setModalVisible] = useState(false);

  const backIcon = route.params?.backIcon;
  const external = route.params?.backIcon;

  
  const visitDateKey = route.params?.visitDateKey
  const tour_plan_id = route.params?.tour_plan_id




  const { token, checkInDocId, dealerData, changeDocId } = useContext(AuthContext);
  console.log("near by dealer data", dealerData);
  console.log("chakci in id", checkInDocId);
  console.log("visit date key", visitDateKey);


  const [desc, setDesc] = useState('')


  //new
  const [value, setValue] = useState(null);
  const [isFocus, setIsFocus] = useState(false);

  const [price, setPrice] = useState('');
  const [orderBooked, setOrderBooked] = useState('');




  useEffect(() => {
    //getDealerData();

  }, [])


  // const getDealerData = async () => {
  //   var myHeaders = new Headers();
  //   myHeaders.append("Content-Type", "application/json");
  //   var raw = JSON.stringify({
  //     "__module_code__": "PO_19",
  //     "__query__": `accounts.id='${dealerData.account_id_c}'`,
  //     "__orderby__": "",
  //     "__offset__": 0,
  //     "__select _fields__": [
  //       "id",
  //       "name"
  //     ],  
  //     "__max_result__": 500,
  //     "__delete__": 0
  //   });

  //   var requestOptions = {
  //     method: 'POST',
  //     headers: myHeaders,
  //     body: raw,
  //     redirect: 'follow'
  //   };

  //   fetch("https://dev.ordo.primesophic.com/get_data_s.php", requestOptions)
  //     .then(response => response.json())
  //     .then(async result => {
  //       console.log('customer details ', result);
  //       let tempArray = await result?.entry_list.map(object => {
  //         console.log("customer res", object.name_value_list)
  //         return {
  //           'addressline1': object.name_value_list.billing_address_street.value,
  //           'addressline2': object.name_value_list.billing_address_street_2.value,
  //           'country': object.name_value_list.billing_address_country.value,
  //           'state': object.name_value_list.billing_address_state.value,
  //           'name': object.name_value_list.name.value,
  //           'postalcode': object.name_value_list.billing_address_postalcode.value,
  //           'creditlimit': object.name_value_list.creditlimit_c.value,
  //           // "credit_note": object.name_value_list.credit_note.value,
  //           'image': "https://dev.ordo.primesophic.com/upload/" + object.name_value_list.id.value + "_img_src_c",
  //           // 'lastsaleamount': "0",
  //           // 'lastpaymentdate': "",
  //           // 'lastsaledate': "",
  //           //'lastsaledate': object.name_value_list.lastsaledate.value,
  //           'due_amount_c': object.name_value_list.due_amount_c.value,
  //           'ispaymentdue': object.name_value_list.payment_due_c.value,
  //           // 'id': object.name_value_list.id.value,
  //           // 'email': object.name_value_list.email.value,
  //           // 'owner': object.name_value_list.ownership.value,
  //           'storeid': object.name_value_list.storeid_c.value,



  //         }
  //       })

  //       setcustomerData(tempArray[0]);


  //     })
  //     .catch(error => console.log('error', error));
  // }




  //new
  const data = [
    { label: 'Secondary Offtake', value: 'Secondary Offtake' },
    { label: 'Competitor Analysis', value: 'Competitor Analysis' },
    { label: 'Return', value: 'Return' },
    { label: 'Shelf Display', value: 'Shelf Display' },

  ];
  //new






  const saveCheckOut = () => {
    if (value && desc && price ) {
      console.log("price", price);
      console.log("booked", orderBooked);
      console.log("custom date key",moment(new Date()).format('YYYYMMDD'))

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
      var raw = JSON.stringify({
        "__module_code__": "PO_37",
        "__query__": "",
        "__name_value_list__":
        {
          "purpose_of_visit": value,
          "description": desc,
          "id": checkInDocId,
          "check_out": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
          "tour_plan_id": tour_plan_id,
          "visit_date_key": visitDateKey,
          "ordered_product": orderBooked,
          "last_amount_paid": price,
          "order_date": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
          "external_visit":external?"1":null,
          "external_visit_date_key":moment(new Date()).format('YYYYMMDD')


        }
      });

      console.log(JSON.stringify({
        "__module_code__": "PO_37",
        "__query__": "",
        "__name_value_list__":
        {
          "purpose_of_visit": value,
          "description": desc,
          "id": checkInDocId,
          "check_out": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
          "tour_plan_id": tour_plan_id,
          "visit_date_key": visitDateKey,
          "ordered_product": orderBooked,
          "last_amount_paid": price,
          "order_date": moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),

        }
      }))

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
      };

      fetch("https://dev.ordo.primesophic.com/set_data_s.php", requestOptions)
        .then(response => response.json())
        .then(async result => {
          changeDocId('');
          console.log('checkout save res', result);
          setModalVisible(false);
          //await AsyncStorage.removeItem('activePlan')
          navigation.navigate('BottomTab')

        })
        .catch(error => console.log('error', error));
    }
    else {
      alert('Please fill all the details');
    }

  }



  return (
    <View style={styles.container}>
      <View style={{
        ...styles.checkOutView,
        flexDirection: 'row',

      }}>

        {backIcon && <TouchableOpacity style={{ marginHorizontal: 5 }} onPress={backAction}>
          <AntDesign name='arrowleft' size={25} color={Colors.black} />
        </TouchableOpacity>}
        <Image style={{ height: 40, width: 90, marginTop: 5 }} source={require('../../assets/images/ordotext.png')} />
        <Text style={{ fontSize: 18, fontFamily: 'Poppins-SemiBold', marginLeft: 10, marginBottom: 5, marginTop: 5 }}>Customers Details</Text>
        {/* <TouchableOpacity style={styles.createPlanBtn}
          onPress={() => setModalVisible(true)}
        >
          <Text style={styles.buttonTextStyle}>Checkout</Text>
        </TouchableOpacity> */}
      </View>
      {/* <View>
          <Text style={{fontSize:18,fontFamily:'Poppins-SemiBold',marginLeft:10,marginBottom:5}}>Customers Details</Text>
        </View> */}
      {/* <View style={styles.row1View}>
        <View style={styles.orderView}>
          <Button title="Market" color={Colors.primary} />
        </View>
        <Button title="Order" color={Colors.primary} />
      </View>
      <View style={styles.row2View}>
        <View style={styles.orderView}>
          <Button title="Return" color={Colors.primary} />
        </View>
        <Button title="Shelf Display" color={Colors.primary} />
      </View> */}
      {/* <Text style={styles.title}>Select your options</Text> */}
      <View style={{ backgroundColor: 'white', flex: 0.7, marginHorizontal: 10, marginBottom: 16, padding: 8, elevation: 5 }}>
        <View style={{ flexDirection: 'row', justifyContent: 'center', flex: 1 }}>
          <View style={{ flex: 0.5, justifyContent: 'center', alignItems: 'center' }}>
            <Image
              source={{ uri: `https://dev.ordo.primesophic.com/upload/${dealerData?.id}_img_src_c` }}
              style={{ width: 100, height: 100 }}
            />
          </View>
          <View style={{
            flex: 0.5,
            marginLeft: 8,
            borderLeftWidth: 1.5,
            paddingLeft: 10,
            // marginLeft: 20,
            borderStyle: 'dotted',
            borderColor: 'grey',
            // alignItems:'center'
            justifyContent: 'center',
            // backgroundColor:'red'
          }}>

            <Text style={{ ...styles.contentView, color: Colors.primary, fontSize: 14, fontFamily: 'Poppins-SemiBold' }}>{dealerData?.name}</Text>
            <Text style={{ ...styles.contentView, fontFamily: 'Poppins-Regular' }}>#{dealerData?.storeid_c}</Text>
            <Text style={styles.contentView}>{dealerData?.shipping_address_street}</Text>
            {dealerData?.billing_address_city != '' && <Text style={styles.contentView}>{dealerData?.billing_address_city}</Text>}
            <Text style={styles.contentView}>{dealerData?.shipping_address_state}</Text>
            <Text style={styles.contentView}>{dealerData?.shipping_address_country} - {dealerData?.shipping_address_postalcode}</Text>
          </View>
        </View>
      </View>
      <View style={styles.row1View}>

        <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} onPress={() => navigation.navigate('CheckInInventory', { screen: 'MIListDetail' })}>
          <Image transition={false} source={require('../../assets/images/order.png')} style={{ width: 30, height: 30, resizeMode: 'cover', marginTop: -1, alignSelf: 'center', tintColor: Colors.primary }} >
          </Image>
          <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 12, color: 'black', textAlign: 'center', marginTop: 12 }}>Secondary Offtake</Text>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} onPress={() => { navigation.navigate('CheckInInventory', { screen: 'CompetitorIntelligence' }) }}>
          <Image transition={false} source={require('../../assets/images/market.png')} style={{ width: 30, height: 30, resizeMode: 'cover', alignSelf: 'center', marginTop: -1, tintColor: Colors.primary }} >
          </Image>
          <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 12, color: 'black', textAlign: 'center', marginTop: 12 }}>Competitor Analysis</Text>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} onPress={() => { navigation.navigate('Invoice') }}>
          <Image transition={false} source={require('../../assets/images/returnOrder.png')} style={{ width: 30, height: 30, resizeMode: 'cover', alignSelf: 'center', marginTop: -1, tintColor: Colors.primary }}  >
          </Image>
          <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 12, color: 'black', textAlign: 'center', marginTop: 12 }}>Return</Text>
        </TouchableOpacity>

      </View>
      <View style={styles.row1View}>

        {/* <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} onPress={() => { navigation.navigate('MIList', { dealerData: dealerData }) }}>
          <Image transition={false} source={require('../../assets/images/market.png')} style={{ width: 25, height: 25, resizeMode: 'cover', alignSelf: 'center', marginTop: -1,tintColor:'blue' }} >
          </Image>
          <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 12, color: 'black', textAlign: 'center', marginTop: 12 }}>Market Intelligence</Text>
        </TouchableOpacity> */}


        <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} onPress={() => { navigation.navigate('SKUHistoryList') }}>
          <Image transition={false} source={require('../../assets/images/history.png')} style={{ width: 30, height: 30, resizeMode: 'cover', alignSelf: 'center', marginTop: -1, tintColor: Colors.primary }} >
          </Image>
          <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 12, color: 'black', textAlign: 'center', marginTop: 12 }}>Secondary Offtake History</Text>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} onPress={() => { navigation.navigate('PreviousVisits') }} >
          <Image transition={false} source={require('../../assets/images/previousVisits.png')} style={{ width: 30, height: 30, resizeMode: 'cover', alignSelf: 'center', marginTop: -1, tintColor: Colors.primary }} >
          </Image>
          <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 12, color: 'black', textAlign: 'center', marginTop: 12 }}>Previous Visits</Text>
        </TouchableOpacity>

        <TouchableOpacity activeOpacity={0.6} style={styles.recoredbuttonStyle} >
          <Image transition={false} source={require('../../assets/images/shelfDisplay.png')} style={{ width: 30, height: 30, resizeMode: 'cover', alignSelf: 'center', marginTop: -1, tintColor: Colors.primary }} >
          </Image>
          <Text style={{ fontFamily: "Poppins-SemiBold", fontSize: 12, color: 'black', textAlign: 'center', marginTop: 12 }}>Shelf Display</Text>
        </TouchableOpacity>

      </View>
      <View style={{ margin: 10, borderRadius: 5, backgroundColor: 'white', padding: 10, elevation: 5 }}>
        <View style={{ paddingHorizontal: 16 }}>
          <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: 'black' }}>Last Due Amt: <Text style={{ color: 'red' }}>{dealerData?.due_amount_c > 0 ? dealerData?.due_amount_c : ''}</Text></Text>
          <Text style={{ fontFamily: 'Poppins-Regular', fontSize: 12, color: 'black' }}>Credit Limit: {dealerData?.creditlimit_c} </Text>
        </View>
        {dealerData?.payment_due_c == '1' && <View style={{ marginLeft: 8 }}>
          <Text style={{ fontSize: 9, color: 'red' }}>"Alert : You have crossed the credit limit. Please make the payment to continue."</Text>
        </View>}
      </View>

      <TouchableOpacity style={styles.checkOutButton} onPress={() => setModalVisible(true)} activeOpacity={0.8}>
        <Text style={styles.checkOutText}>CheckOut</Text>
      </TouchableOpacity>

      <Modal
        visible={isModalVisible}
        animationType="slide"
        transparent={true}

      >
        {/* Modal content */}
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginBottom: 10, }}>
          <View style={{ backgroundColor: 'white', padding: 20, width: '90%', marginHorizontal: 10, borderRadius: 10, elevation: 5 }}>
            {/* new */}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10 }}>
              <Text style={styles.modalTitle}>Purpose of visit</Text>
              <TouchableOpacity onPress={() => setModalVisible(false)}>
                <AntDesign name='close' size={20} color={`black`} />
              </TouchableOpacity>

            </View>
            <View style={styles.dropDownContainer}>
              <Dropdown
                style={[styles.dropdown, isFocus && { borderColor: 'blue' }]}
                placeholderStyle={styles.placeholderStyle}
                selectedTextStyle={styles.selectedTextStyle}
                itemTextStyle={styles.selectedTextStyle}
                inputSearchStyle={styles.inputSearchStyle}
                iconStyle={styles.iconStyle}
                data={data}
                //search
                maxHeight={300}
                labelField="label"
                valueField="value"
                placeholder={!isFocus ? 'Select item' : '...'}
                //searchPlaceholder="Search..."
                value={value}
                onFocus={() => setIsFocus(true)}
                onBlur={() => setIsFocus(false)}
                onChange={item => {
                  setValue(item.value);
                  setIsFocus(false);
                }}



              // renderLeftIcon={() => (
              //   <AntDesign
              //     style={styles.icon}
              //     color={isFocus ? 'blue' : 'black'}
              //     name="Safety"
              //     size={20}
              //   />
              // )}
              />
            </View>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ flex: 1 }}>
                <Text style={styles.modalTitle}>Ordered Amount (AED)</Text>
                <TextInput style={styles.cNameTextInput} placeholder='Ordered amount'
                  onChangeText={text => setPrice(text)}
                  keyboardType='numeric' />
              </View>
              {/* <View style={{ flex: 1, marginLeft: 5 }}>
                <Text style={styles.modalTitle}>Orders booked</Text>
                <TextInput style={styles.cNameTextInput} placeholder='Ordered booked'
                  onChangeText={text => setOrderBooked(text)}
                  keyboardType='numeric' />
                {/* new 
              </View> */}
            </View>

            <Text style={styles.modalTitle}>Report</Text>
            {/* new */}
            <TextInput
              multiline={true}
              numberOfLines={10}
              placeholder="Enter Text..."
              style={styles.textarea}
              onChangeText={(val) => { setDesc(val) }}
              //onChangeText={(text) => this.setState({ text })}
              value={desc}
            />


            <View style={styles.buttonview}>
              <TouchableOpacity style={styles.buttonContainer} onPress={saveCheckOut}>
                <Text style={styles.buttonText}>Submit</Text>
              </TouchableOpacity>
              {/* <TouchableOpacity style={styles.buttonContainer} onPress={() => setModalVisible(false)}>
                <Text style={styles.buttonText}>Cancel</Text>
              </TouchableOpacity> */}
            </View>
          </View>
        </View>
      </Modal>
    </View >
  )
}

export default CheckIn

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#f1f1f2'
    backgroundColor: 'white'

  },
  row1View: {
    //marginHorizontal: 50,
    paddingHorizontal: 30,
    //marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignContent: 'center',
    justifyContent: 'center',
  },
  row2View: {
    paddingHorizontal: 30,
    // marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  orderView: {
    marginRight: 20
  },
  checkOutView: {
    flexDirection: 'row',
    //justifyContent: 'space-between',
    marginRight: 10,
    //paddingHorizontal: 10,
    alignItems: 'center',
    //backgroundColor:'red',
    marginLeft: 3
  },
  recoredbuttonStyle: {
    borderRadius: 4,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5, marginHorizontal: 5,
    shadowRadius: 2,
    elevation: 5,
    height: 118,
    width: 118,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  },
  createPlanBtn: {
    height: 40,
    //width:40,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.primary,
    borderRadius: 10
  },
  buttonTextStyle: {
    color: '#fff',
    fontFamily: 'Poppins-SemiBold',
  },
  buttonview: {
    flexDirection: 'row'
  },
  buttonContainer: {
    heigh: 40,
    padding: 10,
    borderRadius: 10,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    //marginRight: 10,
    marginVertical: 10,
    backgroundColor: Colors.primary
  },
  buttonText: {
    fontFamily: 'Poppins-Regular',
    color: 'white'
  },
  modalTitle: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'Poppins-SemiBold'
  },
  textarea: {
    borderWidth: 0.5,
    borderColor: 'black',
    //margin: 15,
    marginTop: 8,
    borderRadius: 5,
    padding: 10,
    fontSize: 14,
    fontFamily: 'Poppins-Regular',
    textAlignVertical: 'top',
    color: '#000'
  },
  dropDownContainer: {
    backgroundColor: 'white',
    marginBottom: 10
    //padding: 16,
    //backgroundColor:'red'
  },
  dropdown: {
    height: 50,
    borderColor: 'gray',
    borderWidth: 0.5,
    borderRadius: 8,
    paddingHorizontal: 8,
  },
  icon: {
    marginRight: 5,
  },
  label: {
    position: 'absolute',
    backgroundColor: 'white',
    left: 22,
    top: 8,
    zIndex: 999,
    paddingHorizontal: 8,
    fontSize: 14,
  },
  placeholderStyle: {
    fontSize: 14,
    fontFamily: 'Poppins-Regular'
  },
  selectedTextStyle: {
    fontSize: 16,
    fontFamily: 'Poppins-Regular'
  },
  iconStyle: {
    width: 20,
    height: 20,
  },
  inputSearchStyle: {
    height: 40,
    fontSize: 16,
  },
  title: {
    marginVertical: 10,
    paddingHorizontal: 36,
    fontSize: 18,
    fontFamily: 'Poppins-SemiBold',
    color: 'black'
  },
  contentView: {
    color: 'black',
    fontSize: 12,
    fontFamily: 'Poppins-Italic',
    //fontStyle:'italic'
  },
  checkOutButton: {
    height: 50,
    margin: 10,
    // backgroundColor:'red',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    backgroundColor: Colors.primary,
  },
  checkOutText: {
    fontFamily: 'Poppins-SemiBold',
    color: '#fff',
    fontSize: 16
  },
  modalTitle: {
    fontSize: 16,
    color: 'black',
    fontFamily: 'Poppins-SemiBold'
  },
  cNameTextInput: {
    height: 40,
    borderWidth: 1,
    borderColor: '#B3B6B7',
    padding: 5,
    fontFamily: 'Poppins-Regular',
    marginBottom: 10
  },

})