import { StyleSheet, Text, View } from 'react-native'
import React, { useState, useEffect } from 'react'
import firestore from '@react-native-firebase/firestore';
export const AuthContext = React.createContext();
import AsyncStorage from '@react-native-async-storage/async-storage';

export const AuthProvider = ({ children, navigation }) => {
    //token
    const [token, setToken] = useState(null);
    const [userData, setUserData] = useState('')
    const [checkInDocId, setCheckInDocId] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [dealerData, setDealerData] = useState('');
    const [admin, setAdmin] = useState(false);
    const [salesManager, setSalesManager] = useState(false);
    const [merch, setMerch] = useState(false);

    const [tourPlanId, setTourPlanId] = useState('');
    const [tourPlanName, setTourPlanName] = useState('');



    const changeToken = (val) => {
        setToken(val);
    }
    const changeDocId = (val) => {
        setCheckInDocId(val);
    }
    const changeDealerData = (val) => {
        setDealerData(val);
    }

    //const approvoded plans array hooks
    const [aprrovedPlans, setApprovedPlans] = useState('');

    const changeApprovedPlans = (val) => {
        setApprovedPlans(val);
    }

    const changeAdmin = (val) => {
        setAdmin(val);
    }
    const changeTourPlanId = (val) => {
        setTourPlanId(val);
    }

    const clearToken = async () => {
        if (token) {
            var myHeaders = new Headers();
            var raw = JSON.stringify({
                __id__: token
            });


            var requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: raw,
                redirect: 'follow'
            };

            fetch("https://dev.ordo.primesophic.com/set_firebasetoken_null.php", requestOptions)
                .then(response => response.json())
                .then(async result => {
                    console.log("firebase clear api token response", result)
                    if (result.status == "success") {
                        console.log("firebase token cleared")
                    }
                    else {
                        console.log("firebase token clear failed")
                    }
                    AsyncStorage.clear();
                    setIsLoading(false);
                    //setting admin false
                    if (admin) {
                        setAdmin(false);
                    }
                    //sales manager
                    if (salesManager) {
                        setSalesManager(false);
                    }
                    //merchandiser 
                    if (merch) {
                        setMerch(false);
                    }


                })
                .catch(error => console.log('error', error));
        }
        else {
            alert('Please fill all the details');
        }

    }






    //logout method
    const logout = async () => {
        setIsLoading(true);
        //setting user token as null 
        setToken(null);
        setUserData(null);
        try {
            //removing all items;

            //clearing firebase token
            clearToken();

        } catch (error) {
            // There was an error on the native side
            console.log("Error while removing data", error);
        }
    }
    // checking user is already logged in each time when app starts
    const isLoggenIn = async () => {
        try {
            setIsLoading(true);
            //fetching user data
            let userData = await AsyncStorage.getItem("userData");
            if (userData) {
                console.log("user data  saved", userData)
                setUserData(JSON.parse(userData));
            }
            //sales man 
            let userToken = await AsyncStorage.getItem("token");
            if (userToken) {
                console.log("usertoken saved", userToken)
                setToken(userToken);
            }
            //admin
            let isAdmin = await AsyncStorage.getItem("isAdmin");
            if (isAdmin) {
                console.log("amdin saved", isAdmin)
                setAdmin(isAdmin)
                setToken(isAdmin);
            }
            //sales manager
            let isSalesManager = await AsyncStorage.getItem("isSalesManager");
            if (isSalesManager) {
                console.log("sales manager saved", isSalesManager)
                setSalesManager(isSalesManager);
                setToken(isSalesManager);
            }

            //merchandiser
            let isMerchandiser = await AsyncStorage.getItem("isMerch");
            if (isMerchandiser) {
                console.log("merchandiser saved", isMerchandiser)
                setMerch(isMerchandiser);
                setToken(isMerchandiser);
                
                let tmpData =JSON.parse(userData);
                changeDealerData({ id: tmpData.account_id })
            }



            setIsLoading(false);
        } catch (error) {
            console.log("Error retrieving data", error);

        }
    }

    // AdminOffers Array Hooks

    const [pendingArray, setPendingArray] = useState([])
    const [completedArray, setCompletedArray] = useState([])
    const [returnArray, setReturnArray] = useState([])


    //sales manager plans hooks
    const [approvedArray, setApprovedArray] = useState([]);
    const [tourPlanArray, setTourPlanArray] = useState([]);






    useEffect(() => {
        isLoggenIn();
    }, [])


    return (
        <AuthContext.Provider value={{
            token,
            changeToken,
            logout,
            changeDocId,
            checkInDocId,
            isLoading,
            dealerData,
            changeDealerData,
            aprrovedPlans,
            changeApprovedPlans,
            admin,
            changeAdmin,
            tourPlanId,
            changeTourPlanId,
            pendingArray,
            setPendingArray,
            completedArray,
            setCompletedArray,
            returnArray,
            setReturnArray,
            salesManager,
            setSalesManager,
            approvedArray,
            setApprovedArray,
            tourPlanArray,
            setTourPlanArray,
            userData,
            setUserData,
            merch,
            setMerch,
            tourPlanName,
            setTourPlanName

        }}
        >
            {children}
        </AuthContext.Provider>

    )
}



